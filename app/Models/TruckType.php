<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TruckType extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'name',
        'status_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';
}
