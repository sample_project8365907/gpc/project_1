<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScrapCurrentBatch extends Model
{
    use HasFactory;

    protected $fillable = [
        'scrap_current_batch',
    ];
}
