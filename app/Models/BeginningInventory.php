<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class BeginningInventory extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'locc_baled',
        'locc_loose',
        'mw_baled',
        'mw_loose',
        'date',
        'location_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
