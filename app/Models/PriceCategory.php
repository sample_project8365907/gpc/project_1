<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PriceCategory extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'name',
        'material_type_id',
        'amount',
        'effective_date',
        'location_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? static::query()
            : static::query()
            ->where('name', 'like', '%' . $search . '%');
    }

    public function materialType()
    {
        return $this->belongsTo(MaterialType::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
