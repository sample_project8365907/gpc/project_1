<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Voucher extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'gcv_no',
        'ws_no',
        'supplier_id',
        'supplier_name',
        'driver_name',
        'plate_no',
        'truck_type_id',
        'material_type_id',
        'price_category_id',
        'gross_weight',
        'tare_weight',
        'net_weight',
        'deducted_weight',
        'corrected_weight',
        'mc',
        'ot',
        'pm',
        'unit_price',
        'amount',
        'time_in',
        'time_out',
        'location_id',
        'prepared_by',
        'checked_by',
        'approved_by',
        'status_id',
        'encoded_at',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->where('gcv_no', 'like', '%' . $search . '%')
            ->orWhere('supplier_name', 'like', '%' . $search . '%')
            ->orWhereHas('materialType', function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%');
            });
    }

    // This is for supplier performance report
    public static function searchSupplier($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->whereHas('supplier', function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%');
            });
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function status()
    {
        return $this->belongsTo(OtherStatus::class);
    }

    public function materialType()
    {
        return $this->belongsTo(MaterialType::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
