<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class CogsBeginningInventory extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'location_id',
        'material_type_id',
        'quantity',
        'average_cost',
        'total_cost',
        'effective_date',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function materialType()
    {
        return $this->belongsTo(materialType::class);
    }
}
