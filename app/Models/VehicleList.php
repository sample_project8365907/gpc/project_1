<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class VehicleList extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'hauler_id',
        'truck_type_id',
        'plate_no',
        'tare_weight',
        'capacity',
        'status_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->where('plate_no', 'like', '%' . $search . '%');
    }

    public function vehicleTypes()
    {
        return $this->belongsTo(TruckType::class, 'truck_type_id', 'id');
    }
}
