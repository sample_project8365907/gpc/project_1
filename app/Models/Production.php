<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Production extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'bale_no',
        'material_type_id',
        'weight',
        'mw_converted',
        'location_id',
        'status_id',
        'encoded_at',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->where('bale_no', 'like', '%' . $search . '%');
    }

    public function materialType()
    {
        return $this->belongsTo(MaterialType::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function deliveries()
    {
        return $this->belongsToMany(Delivery::class)->withTimestamps();
    }
}
