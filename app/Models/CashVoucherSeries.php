<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class CashVoucherSeries extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'cv_series',
        'location_id',
        'status_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->where('cv_series', 'like', '%' . $search . '%');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
