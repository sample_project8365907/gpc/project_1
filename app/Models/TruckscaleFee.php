<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TruckscaleFee extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'cv_no',
        'supplier_name',
        'driver_name',
        'plate_no',
        'truck_type_id',
        'unit_price',
        'gross_weight',
        'tare_weight',
        'net_weight',
        'time_in',
        'time_out',
        'amount',
        'location_id',
        'prepared_by',
        'checked_by',
        'approved_by',
        'status_id',
        'encoded_at',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->where('cv_no', 'like', '%' . $search . '%')
            ->orWhere('supplier_name', 'like', '%' . $search . '%');
    }
}
