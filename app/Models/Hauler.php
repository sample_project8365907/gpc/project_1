<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Spatie\Activitylog\Traits\LogsActivity;

class Hauler extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'name',
        'tin',
        'vat',
        'head_office_address',
        'head_office_tel_no',
        'head_office_fax_no',
        'head_office_email',
        'head_office_property',
        'plant_address',
        'plant_tel_no',
        'plant_fax_no',
        'plant_email',
        'plant_property',
        'sec',
        'dti',
        'type_of_organization',
        'line_of_business',
        'no_of_years',
        'documents',
        'status_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->where('name', 'like', '%' . $search . '%');
    }
}
