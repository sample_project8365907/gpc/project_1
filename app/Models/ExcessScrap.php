<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExcessScrap extends Model
{
    use HasFactory;

    protected $fillable = [
        'volunteer_id',
        'material_type_id',
        'excess_weight',
        'scrap_current_batch',
        'status_id',
        'encoded_at',
    ];
}
