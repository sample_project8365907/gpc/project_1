<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Delivery extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'transfer_slip_no',
        'trip_ticket_no',
        'paper_mill_id',
        'plate_no',
        'quantity_bales',
        'gross_weight',
        'tare_weight',
        'net_weight',
        'deducted_weight',
        'mc',
        'ot',
        'pm',
        'description',
        'material_type_id',
        'hauler_id',
        'trucking_rate_category_id',
        'driver',
        'ts_approved_by',
        'ts_checked_by',
        'selected_bales_weight',
        'time_in',
        'time_out',
        'corrected_weight',
        'prepared_by',
        'noted_by',
        'approved_by',
        'status_id',
        'location_id',
        'encoded_at',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->whereHas('paperMill', function (Builder $query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            })
            ->orWhere('transfer_slip_no', 'like', '%' . $search . '%')
            ->orWhere('trip_ticket_no', 'like', '%' . $search . '%');
    }

    public function paperMill()
    {
        return $this->belongsTo(PaperMill::class);
    }

    public function materialType()
    {
        return $this->belongsTo(MaterialType::class);
    }

    public function status()
    {
        return $this->belongsTo(OtherStatus::class);
    }

    public function productions()
    {
        return $this->belongsToMany(Production::class)->withTimestamps();
    }

    public function hauler()
    {
        return $this->belongsTo(Hauler::class);
    }

    public function truckingRateCategory()
    {
        return $this->belongsTo(TruckingRateCategory::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(VehicleList::class, 'plate_no', 'id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function summary()
    {
        return $this->hasOne(DeliverySummary::class);
    }
}
