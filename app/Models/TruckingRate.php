<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TruckingRate extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'hauler_id',
        'trucking_rate_category_id',
        'trucking_destination_id',
        'amount',
        'effective_date',
        'status_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public function destination()
    {
        return $this->belongsTo(TruckingDestination::class, 'trucking_destination_id', 'id');
    }
}
