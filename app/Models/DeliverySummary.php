<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DeliverySummary extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'delivery_id',
        'summary_document_no',
        'summary_date',
        'summary_net_weight',
        'summary_mc',
        'summary_ot',
        'summary_pm',
        'summary_corrected_weight',
        'summary_soa_no',
        'summary_way_bill',
        'summary_status_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';
}
