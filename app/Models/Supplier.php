<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Supplier extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'code',
        'name',
        'address',
        'owner',
        'contact_no',
        'location_id',
        'status_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        $current_location = session()->get('location_id');

        return empty($search)
            ? self::query()
            : self::query()
            ->where('name', 'like', '%' . $search . '%')
            ->where('location_id', $current_location)
            ->orWhere('code', 'like', '%' . $search . '%');
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class);
    }

    public function materialTypes()
    {
        return $this->belongsToMany(MaterialType::class)->withTimestamps();
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
