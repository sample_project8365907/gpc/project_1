<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VolunteerScrap extends Model
{
    use HasFactory;

    protected $fillable = [
        'volunteer_id',
        'material_type_id',
        'weight',
        'scrap_batch_id',
        'status_id',
        'encoded_at',
    ];

    public function materialType()
    {
        return $this->belongsTo(MaterialType::class);
    }
}
