<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MaterialType extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'name',
        'status_id',
    ];

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    // protected $touches = ['suppliers'];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public function suppliers()
    {
        return $this->belongsToMany(Supplier::class)->withTimestamps();
    }

    public function locations()
    {
        return $this->belongsToMany(Location::class)->withTimestamps();
    }
}
