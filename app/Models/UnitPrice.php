<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class UnitPrice extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'price_category_id',
        'amount',
        'effective_date',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';
}
