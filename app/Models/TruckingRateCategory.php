<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TruckingRateCategory extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'location_id',
        'hauler_id',
        'name',
        'status_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? static::query()
            : static::query()
            ->where('name', 'like', '%' . $search . '%');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function hauler()
    {
        return $this->belongsTo(Hauler::class);
    }

    public function truckingRate()
    {
        return $this->hasOne(TruckingRate::class);
    }
}
