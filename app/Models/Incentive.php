<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Incentive extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'financer',
        'locc_incentive',
        'mw_incentive',
        'location_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->where('financer', 'like', '%' . $search . '%')
            ->orWhereHas('location', function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%');
            });
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
