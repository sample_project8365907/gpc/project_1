<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PaperMill extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'name',
        'tin',
        'vat',
        'address',
        'contact_no',
        'fax_no',
        'email',
        'property',
        'plant_address',
        'plant_tel_no',
        'plant_fax_no',
        'plant_email',
        'plant_property',
        'status_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'system';

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->where('name', 'like', '%' . $search . '%');
    }
}
