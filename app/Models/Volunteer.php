<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'name',
        'address',
        'company_name',
        'contact_no',
        'location_id',
        'status_id',
        'encoded_at',
    ];

    public static function search($search)
    {
        return empty($search)
            ? self::query()
            : self::query()
            ->where('status_id', 1)
            ->where('name', 'like', '%' . $search . '%')
            ->orWhere('code', 'like', '%' . $search . '%');
    }

    public function scraps()
    {
        return $this->hasMany(VolunteerScrap::class);
    }
}
