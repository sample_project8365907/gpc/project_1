<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\Voucher;

class ReimbursementController extends Controller
{
    public function index()
    {
        return view('reimbursements.index');
    }

    public function print(Request $request)
    {
        $date_to = $request->date_to;
        $date_from = $request->date_from;
        $prepared_by = $request->prepared_by;
        $checked_by = $request->checked_by;
        $approved_by = $request->approved_by;

        $current_location = session()->get('location_id');
        $daily_reports = Voucher::whereBetween('created_at', [$date_from, Carbon::parse($date_to)->addDays(1)])
            ->where('status_id', 1)
            ->where('location_id', $current_location)
            ->get();

        $pdf = PDF::loadView('livewire.reimbursements.print', compact(
            'date_from',
            'date_to',
            'prepared_by',
            'checked_by',
            'approved_by',
            'daily_reports',
        ))->setOptions([
            'chroot' => realpath(base_path()),
            'dpi' => 300,
            'defaultMediaType' => 'print',
            'isFontSubsettingEnabled' => 'true'
        ])
            ->setPaper('letter', 'portrait')
            ->setWarnings(true);

        return $pdf->stream();
    }
}
