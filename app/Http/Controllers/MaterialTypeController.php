<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MaterialTypeController extends Controller
{
    public function index()
    {
        return view('material-types.index');
    }
}
