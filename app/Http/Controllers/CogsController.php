<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\CogsBeginningInventory;
use App\Models\Location;
use App\Models\MaterialType;
use App\Models\Voucher;
use App\Models\Delivery;
use Barryvdh\DomPDF\Facade\Pdf;

class CogsController extends Controller
{
    public function index()
    {
        return view('cogs.index');
    }

    public function print(Request $request)
    {
        $current_location = session()->get('location_id');
        $month = $request->month;
        $year = $request->year;

        $today = Carbon::createFromDate($year, $month, '01');

        $reports = [];
        $materials = [];

        $mem_btd_quantity[] = 0;
        $mem_btd_average_cost[] = 0;
        $mem_btd_total_cost[] = 0;

        $data_btd_quantity[] = 0;
        $data_btd_average_cost[] = 0;
        $data_btd_total_cost[] = 0;

        $rowspan = 0;

        if ((($year > now()->format('Y')) && ($month < now()->format('n')))) {
            $dates = 0; // No dates will show.
        } elseif (($year < now()->format('Y')) || ($month < now()->format('n'))) {
            $dates = $today->daysInMonth; // Show all the dates in a month.
        } elseif (($year == now()->format('Y')) && ($month == now()->format('n'))) {
            $dates = now()->format('j'); // Show all the previous dates until the current date in a month.
        } else {
            $dates = 0; // No dates will show.
        }

        for ($i = 1; $i < ($dates + 1); $i++) {
            $h = 0;

            $date = Carbon::createFromDate($today->year, $today->month, $i);

            // Get all the material types from the current baling station/location.
            $materials = [];
            $material_types = Location::where('id', $current_location)->first()->materialTypes->whereNotIn('id', [3, 6, 7]);

            foreach ($material_types as $material_type) {
                $h++;

                $beginning_inventory = CogsBeginningInventory::whereDate('effective_date', $date->format('Y-m-d'))
                    ->where('material_type_id', $material_type->id)
                    ->orderBy('effective_date', 'desc')->first();

                // Get the purchases/recieving quantity per material type.
                $vouchers = Voucher::where('material_type_id', $material_type->id)
                    ->where('location_id', $current_location)
                    ->where('status_id', 1)
                    ->whereDate('encoded_at', $date->format('Y-m-d'))
                    ->get();

                // Get the purchases/recieving quantity per material type.
                $deliveries = Delivery::where('material_type_id', $material_type->id)
                    ->where('location_id', $current_location)
                    ->where('status_id', 1)
                    ->whereDate('encoded_at', $date->format('Y-m-d'))
                    ->get();

                if ($beginning_inventory) {
                    $beg_quantity = $beginning_inventory->quantity;
                    $beg_average_cost = $beginning_inventory->average_cost;
                    $beg_total_cost = $beginning_inventory->total_cost;
                } else {
                    // Check if the index is exists.
                    $d_beg_quantity = (isset($data_btd_quantity[$i - 1][$h - 1])) ? $data_btd_quantity[$i - 1][$h - 1] : 0;
                    $d_beg_average_cost = (isset($data_btd_average_cost[$i - 1][$h - 1])) ? $data_btd_average_cost[$i - 1][$h - 1] : 0;
                    $d_beg_total_cost = (isset($data_btd_total_cost[$i - 1][$h - 1])) ? $data_btd_total_cost[$i - 1][$h - 1] : 0;

                    $beg_quantity = $d_beg_quantity;
                    $beg_average_cost = $d_beg_average_cost;
                    $beg_total_cost = $d_beg_total_cost;
                }

                $pur_quantity = $vouchers->pluck('corrected_weight')->sum();
                $pur_total_cost = $vouchers->pluck('amount')->sum();
                $pur_average_cost = ($pur_total_cost != 0 && $pur_quantity != 0) ? ($pur_total_cost / $pur_quantity) : 0; // When pur_average_cost is division by zero, replace it with 0 value;

                $sales_quantity = $deliveries->pluck('corrected_weight')->sum();
                if ($pur_total_cost != 0 && $pur_quantity != 0) {
                    $sales_average_cost = round(($beg_total_cost + $pur_total_cost) / ($beg_quantity + $pur_quantity));
                } else {
                    $sales_average_cost = 0;
                }
                $sales_total_cost = $sales_quantity * $sales_average_cost;

                $btd_quantity = ($beg_quantity + $pur_quantity) - $sales_quantity;
                $btd_total_cost = ($beg_total_cost + $pur_total_cost) - $sales_total_cost;
                if ($btd_total_cost != 0 && $btd_quantity != 0) {
                    $btd_average_cost = $btd_total_cost / $btd_quantity;
                } else {
                    $btd_average_cost = 0;
                }

                $mem_btd_quantity[] = $btd_quantity;
                $mem_btd_average_cost[] = $btd_average_cost;
                $mem_btd_total_cost[] = $btd_total_cost;

                // Casting string to prevent the data corruption when passing it to browser.
                // https://stackoverflow.com/a/71427329
                $materials[] = [
                    'material_id' => $material_type->id,
                    'material_name' => $material_type->name,

                    'beg_quantity' => (string)$beg_quantity,
                    'beg_average_cost' => (string)$beg_average_cost,
                    'beg_total_cost' => (string)$beg_total_cost,

                    'pur_quantity' =>  (string)$pur_quantity,
                    'pur_average_cost' =>  (string)$pur_average_cost,
                    'pur_total_cost' =>  (string)$pur_total_cost,

                    'sales_quantity' => (string)$sales_quantity,
                    'sales_average_cost' => (string)$sales_average_cost,
                    'sales_total_cost' => (string)$sales_total_cost,

                    'btd_quantity' => (string)$btd_quantity,
                    'btd_average_cost' => (string)$btd_average_cost,
                    'btd_total_cost' => (string)$btd_total_cost,
                ];
            }

            // Get only the last 2 value in array.
            $data_btd_quantity[] = array_slice($mem_btd_quantity, -2, 2);
            $data_btd_average_cost[] = array_slice($mem_btd_average_cost, -2, 2);
            $data_btd_total_cost[] = array_slice($mem_btd_total_cost, -2, 2);

            $reports[] = ['date' => $date, 'materials' => $materials];
        }

        $rowspan = count($materials);

        $pdf = PDF::loadView('cogs.print', compact('reports', 'rowspan', 'month', 'year'))
            ->setOptions([
                'chroot' => realpath(base_path()),
                'dpi' => 300,
                'defaultMediaType' => 'print',
                'isFontSubsettingEnabled' => 'true'
            ])
            ->setPaper('letter', 'portrait');

        return $pdf->stream();
    }
}
