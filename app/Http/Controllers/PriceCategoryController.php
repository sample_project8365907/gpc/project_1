<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PriceCategoryController extends Controller
{
    public function index()
    {
        return view('price-categories.index');
    }

    public function edit($id)
    {
        $pcid = $id;

        return view('price-categories.edit', compact('pcid'));
    }
}
