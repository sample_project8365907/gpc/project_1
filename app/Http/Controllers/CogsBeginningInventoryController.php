<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CogsBeginningInventoryController extends Controller
{
    public function index()
    {
        return view('cogs-beginning-inventories.index');
    }
}
