<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Custom\CustomFPDF;
use App\Models\TruckscaleFee;

class TruckscaleFeeController extends Controller
{
    public function index()
    {
        return view('truckscale-fees.index');
    }

    public function singlePrint($id)
    {
        $truckscale_fee = TruckscaleFee::where('id', $id)->orderBy('encoded_at', 'asc')->first();

        $prepared_by = ($truckscale_fee->prepared_by == '') ? ' ' : $truckscale_fee->prepared_by;
        $checked_by = ($truckscale_fee->checked_by == '') ? ' ' : $truckscale_fee->checked_by;
        $approved_by = ($truckscale_fee->approved_by == '') ? ' ' : $truckscale_fee->approved_by;

        $filename = 'Truckscale Fee Voucher (' . date('F j, Y', strtotime($truckscale_fee->encoded_at)) . ')';

        $baling_station = session()->get('location');

        define('FPDF_FONTPATH', public_path('/fonts'));

        // $this->fpdf = New CustomFPDF('L', 'mm', ['210', '165']);
        $this->fpdf = new CustomFPDF('P', 'mm', ['144.78', '208.28']);

        // 1st Page
        $this->fpdf->AddPage();
        $this->fpdf->AddFont('Roboto-Regular', 'R', 'Roboto-Regular.php');
        $this->fpdf->AddFont('Roboto-Bold', 'B', 'Roboto-Bold.php');
        $this->fpdf->SetMargins(0, 0, 0);
        $this->fpdf->SetAutoPageBreak(false);

        // Page Title
        $this->fpdf->SetTitle('Truckscales Fees');

        // Baling Station
        $this->fpdf->SetXY(12.5, 15);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(120, 5, $baling_station . ' Baling Station', 0, 1, 'C');

        // Baling Station Address
        $this->fpdf->SetXY(12.5, 22);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(120, 5, '#10 Isidro Francisco St. Valenzuela City', 0, 1, 'C');

        // Divider
        $this->fpdf->SetXY(12.5, 30);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(120, 5, '______________________________________________________________________', 0, 1, 'C');

        // Supplier Name Value
        $this->fpdf->SetXY(33, 37);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(60, 5, $truckscale_fee->supplier_name, 0, 1, 'C');

        // Supplier Name
        $this->fpdf->SetXY(12.5, 38);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(120, 5, 'SUPPLIER: __________________________________', 0, 1, 'L');

        // Material Type Value
        $this->fpdf->SetXY(95, 38);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(120, 5, $truckscale_fee->material_type, 0, 1, 'L');

        // Material Type
        $this->fpdf->SetXY(95, 38);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(120, 5, 'TYPE: ______________', 0, 1, 'L');

        // $this->fpdf->SetXY(13, 36);
        // $this->fpdf->SetTextColor(0, 0, 0);
        // $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        // $this->fpdf->CellFitScale(120, 5, '_________________', 0, 1, 'L');

        // Divider
        $this->fpdf->SetXY(12.5, 43);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(120, 5, '_____________________________________________________________________', 0, 1, 'C');



        // Output
        $this->fpdf->Output($filename, 'I');
        exit;
    }
}
