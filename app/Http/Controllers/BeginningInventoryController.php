<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BeginningInventoryController extends Controller
{
    public function index()
    {
        return view('beginning-inventories.index');
    }
}
