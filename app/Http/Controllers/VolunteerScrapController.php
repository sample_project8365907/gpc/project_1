<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Models\Volunteer;
use App\Models\ScrapCurrentBatch;

class VolunteerScrapController extends Controller
{
    public function printCoupon(Request $request)
    {
        $current_batch_id = ScrapCurrentBatch::first('scrap_current_batch')->scrap_current_batch;
        $batch_id = $current_batch_id;

        $vid = $request->id;
        $volunteer = Volunteer::find($vid);
        $volunteer_name = $volunteer->name;
        $total_coupons = $request->total_coupons;
        $volunteer_code = $request->volunteer_code;
        $qr = [];

        for ($i = 1; $i <= $total_coupons; $i++) {
            $qr[] = ['volunteer_name' => $volunteer_name, 'volunteer_code' => $volunteer_code, 'coupon_number' => $i, 'batch_id' => $batch_id];
        }

        $blocks = array_chunk($qr, 5);

        $pdf = PDF::loadView('volunteer-scraps.single-print-coupon', compact('blocks'))
            ->setOptions([
                'chroot' => realpath(base_path()),
                'dpi' => 120,
                'defaultMediaType' => 'print',
                'isFontSubsettingEnabled' => 'true'
            ])
            ->setPaper('letter', 'portrait');

        return $pdf->stream();
    }
}
