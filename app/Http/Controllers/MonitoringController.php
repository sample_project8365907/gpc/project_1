<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\Voucher;

use App\Exports\MonitoringsExport;
use Maatwebsite\Excel\Facades\Excel;

class MonitoringController extends Controller
{
    public function index()
    {
        return view('monitorings.index');
    }

    public function print(Request $request)
    {
        $date_to = $request->date_to;
        $date_from = $request->date_from;
        $search = $request->search;

        $current_location = session()->get('location_id');
        $accounting_reports = Voucher::whereBetween('created_at', [$date_from, Carbon::parse($date_to)->addDays(1)])
            ->where('location_id', $current_location)
            ->get();

        $pdf = PDF::loadView('livewire.monitorings.print', compact(
            'date_from',
            'date_to',
            'accounting_reports',
        ))->setOptions([
            'chroot' => realpath(base_path()),
            'dpi' => 300,
            'defaultMediaType' => 'print',
            'isFontSubsettingEnabled' => 'true'
        ])
            ->setPaper('letter', 'portrait')
            ->setWarnings(true);

        return $pdf->stream();
    }

    public function excel(Request $request)
    {
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $search = $request->search;

        $current_location = session()->get('location_id');
        $accounting_reports = Voucher::whereBetween('created_at', [$date_from, Carbon::parse($date_to)->addDays(1)])
            ->where('location_id', $current_location)
            ->get();

        $filename = 'Accounting Reports (' . now()->format('Y-m-d') . ').xlsx';

        return Excel::download(new MonitoringsExport($accounting_reports, $date_from, $date_to), $filename);
    }
}
