<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Codedge\Fpdf\Fpdf\Fpdf;
use App\Http\Custom\CustomFPDF;
use App\Models\Delivery;

class TripTicketPrintController extends Controller
{
    private $fpdf;

    public function normalize($word)
    {
        $word = str_replace("Ñ", "%D1", $word);
        $word = str_replace("ñ", "%F1", $word);

        return urldecode($word);
    }

    public function print(Request $request)
    {
        $trip_ticket = Delivery::find($request->id);

        $material_type_name = ($trip_ticket->materialType->name != '') ? $trip_ticket->materialType->name : ' ';

        $current_location = session()->get('location_id');

        // Add -MB or Machine Baled if the baling station is Valenzuela.
        if ($current_location == 1) {
            $material_type_name = $material_type_name . '-MB';
        }

        $filename = 'Trip Ticket (' . date('F j, Y', strtotime($trip_ticket->encoded_at)) . ').pdf';

        define('FPDF_FONTPATH', public_path('/fonts'));

        // $this->fpdf = New CustomFPDF('L', 'mm', ['213.36', '127']);
        $this->fpdf = new CustomFPDF('P', 'mm', 'letter');

        // 1st Page
        // $this->fpdf->AddPage('P', ['56', '87.7']);
        $this->fpdf->AddPage();
        $this->fpdf->AddFont('Roboto-Regular', 'R', 'Roboto-Regular.php');
        $this->fpdf->AddFont('Roboto-Regular', 'R', 'Roboto-Bold.php');
        $this->fpdf->SetMargins(0, 0, 0);
        $this->fpdf->SetAutoPageBreak(false);

        // Page Title
        $this->fpdf->SetTitle('Trip Ticket');

        // Trip Ticket Layout for reference
        // $this->fpdf->Image(public_path('images/Trip Ticket Layout 8.40(h)x5(w).jpg'), 0, 0, 213.36, 127);

        // Add space if these fields are empty
        $prepared_by = (empty($trip_ticket->prepared_by)) ? ' ' : $trip_ticket->prepared_by;
        $noted_by = (empty($trip_ticket->noted_by)) ? ' ' : $trip_ticket->noted_by;
        $approved_by = (empty($trip_ticket->approved_by)) ? ' ' : $trip_ticket->approved_by;

        // for time in and time out
        $time_in = ($trip_ticket->time_in == null) ? '--' : date('h:i a', strtotime($trip_ticket->time_in));
        $time_out = ($trip_ticket->time_out == null) ? '--' : date('h:i a', strtotime($trip_ticket->time_out));

        // Vehicle No. / Plate No.
        $this->fpdf->SetXY(22, 16);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(40, 5, $trip_ticket->vehicle->plate_no, 0, 1, 'L');

        // Driver
        $this->fpdf->SetXY(14, 22);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(43, 5, $this->normalize($trip_ticket->driver), 0, 1, 'L');

        // Hauler
        $this->fpdf->SetXY(14, 27);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(43, 5, $this->normalize($trip_ticket->hauler->name), 0, 1, 'L');

        // Date Now
        $this->fpdf->SetXY(163, 16);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 11);
        $this->fpdf->CellFitScale(43, 5, date('F j, Y', strtotime($trip_ticket->encoded_at)), 0, 1, 'L');

        // Trip Ticket No.
        $this->fpdf->SetXY(1, 48);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 9);
        $this->fpdf->CellFitScale(25, 5, 'TS # ' . $trip_ticket->transfer_slip_no, 0, 1, 'C');

        // Origin
        $this->fpdf->SetXY(25, 48);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(52, 5, 'Origin: ' . $this->normalize(session()->get('location')), 0, 1, 'L');

        // Destination / Paper Mill
        $this->fpdf->SetXY(25, 53);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(52, 5, 'Destination: ' . $this->normalize($trip_ticket->paperMill->name), 0, 1, 'L');

        // Time In
        $this->fpdf->SetXY(132, 48);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(19, 5, $time_in, 0, 1, 'C');

        // Time Out
        $this->fpdf->SetXY(152.5, 48);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(19, 5, $time_out, 0, 1, 'C');

        // Material Type
        $this->fpdf->SetXY(5, 79);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(52, 5, $material_type_name, 0, 1, 'L');

        // No. of Bales
        $this->fpdf->SetXY(25, 79);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(52, 5, $trip_ticket->quantity_bales . ' BALE(S)', 0, 1, 'L');

        // Corrected Weight
        $this->fpdf->SetXY(45, 79);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(52, 5, $trip_ticket->corrected_weight . ' KGS (CW)', 0, 1, 'L');

        // Rate
        // $this->fpdf->SetXY(21, 87);
        // $this->fpdf->SetTextColor(0, 0, 0);
        // $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        // $this->fpdf->CellFitScale(45, 5, 'Rate', 1, 1, 'C');

        // Prepared By
        $this->fpdf->SetXY(11, 113);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(45, 5, $this->normalize($prepared_by), 0, 1, 'C');

        // Noted By
        $this->fpdf->SetXY(97.5, 113);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(45, 5, $this->normalize($noted_by), 0, 1, 'C');

        // Approved By
        $this->fpdf->SetXY(163, 113);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
        $this->fpdf->CellFitScale(45, 5, $this->normalize($approved_by), 0, 1, 'C');

        // Output
        $this->fpdf->Output($filename, 'I');
        exit;
    }
}
