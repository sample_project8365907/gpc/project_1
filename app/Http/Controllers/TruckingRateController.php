<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TruckingRateController extends Controller
{
    public function index($id, $hauler_id)
    {
        $trcid = $id;
        $hid = $hauler_id;

        return view('trucking-rates.index', compact('trcid', 'hid'));
    }
}
