<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Custom\CustomFPDF;
use App\Models\TruckscaleFee;

class TruckscaleFeeController extends Controller
{
    public function index()
    {
        return view('truckscale-fees.index');
    }

    public function singlePrint($id)
    {
        $truckscale_fee = TruckscaleFee::where('id', $id)->orderBy('encoded_at', 'asc')->first();

        $prepared_by = ($truckscale_fee->prepared_by == '') ? ' ' : $truckscale_fee->prepared_by;
        $checked_by = ($truckscale_fee->checked_by == '') ? ' ' : $truckscale_fee->checked_by;
        $approved_by = ($truckscale_fee->approved_by == '') ? ' ' : $truckscale_fee->approved_by;

        $filename = 'Truckscale Fee Voucher (' . date('F j, Y', strtotime($truckscale_fee->encoded_at)) . ')';

        $baling_station = session()->get('location');

        define('FPDF_FONTPATH', public_path('/fonts'));

        // $this->fpdf = New CustomFPDF('L', 'mm', ['210', '165']);
        $this->fpdf = new CustomFPDF('P', 'mm', 'letter');

        // Page Title
        $this->fpdf->SetTitle('Truckscales Fees');

        // 1st Page
        // $this->fpdf->AddPage('P', ['56', '87.7']);
        $this->fpdf->AddPage();
        $this->fpdf->AddFont('Roboto-Regular', 'R', 'Roboto-Regular.php');
        $this->fpdf->AddFont('Roboto-Bold', 'B', 'Roboto-Bold.php');
        $this->fpdf->SetMargins(0, 0, 0);
        $this->fpdf->SetAutoPageBreak(false);

        // Page Title
        $this->fpdf->SetTitle('Voucher');

        // Voucher Layout for reference
        // $this->fpdf->Image(public_path('images/Voucher 21(w)x16.5(h).jpg'), 0, 0, 210, 165);

        // Supplier
        $this->fpdf->SetXY(26, 33);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 24);
        $this->fpdf->CellFitScale(120, 5, $truckscale_fee->supplier_name, 0, 1, 'C');

        // Supplier Address
        // $this->fpdf->SetXY(26, 47);
        // $this->fpdf->SetTextColor(0, 0, 0);
        // $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        // $this->fpdf->CellFitScale(120, 5, 'Sample Address of Supplier', 0, 1, 'C');

        // Date
        $this->fpdf->SetXY(168, 33);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(38, 5, date('F j, Y', strtotime($truckscale_fee->encoded_at)), 0, 1, 'C');

        // TABLE
        $this->fpdf->SetXY(7, 66);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 14);
        $this->fpdf->ImprovedTable(['PAYMENT FOR', 'WS No.', 'PLATE NO.', 'GROSS (INBOUND)', 'TARE (OUTBOUND)', 'NET WEIGHT'], [], [25, 15, 25, 30, 30, 30]);

        // // PAYMENT FOR:
        // $this->fpdf->SetXY(7, 76);
        // $this->fpdf->SetTextColor(0, 0, 0);
        // $this->fpdf->SetFont('Roboto-Bold', 'B', 14);
        // $this->fpdf->CellFitScale(25, 5, $truckscale_fee->materialType->name, 0, 1, 'C');

        // // CV No:
        // $this->fpdf->SetXY(32, 76);
        // $this->fpdf->SetTextColor(0, 0, 0);
        // $this->fpdf->SetFont('Roboto-Bold', 'B', 14);
        // $this->fpdf->CellFitScale(15, 5, $truckscale_fee->ws_no, 0, 1, 'C');

        // PLATE NO.
        $this->fpdf->SetXY(47, 76);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 14);
        $this->fpdf->CellFitScale(25, 5, $truckscale_fee->plate_no, 0, 1, 'C');

        // GROSS (INBOUND)
        $this->fpdf->SetXY(77, 76);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 14);
        $this->fpdf->CellFitScale(20, 5, number_format($truckscale_fee->gross_weight), 0, 1, 'C');

        // // MC
        // $this->fpdf->SetXY(92, 76);
        // $this->fpdf->SetTextColor(0, 0, 0);
        // $this->fpdf->SetFont('Roboto-Bold', 'B', 14);
        // $this->fpdf->CellFitScale(15, 5, $truckscale_fee->mc, 0, 1, 'C');

        // TARE (OUTBOUND)
        $this->fpdf->SetXY(107, 76);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 14);
        $this->fpdf->CellFitScale(20, 5, number_format($truckscale_fee->tare_weight), 0, 1, 'C');

        // NET WEIGHT
        $this->fpdf->SetXY(137, 76);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 14);
        $this->fpdf->CellFitScale(15, 5, number_format($truckscale_fee->net_weight), 0, 1, 'C');

        // // UNIT PRICE
        // $this->fpdf->SetXY(142, 76);
        // $this->fpdf->SetTextColor(0, 0, 0);
        // $this->fpdf->SetFont('Roboto-Bold', 'B', 14);
        // $this->fpdf->CellFitScale(20, 5, number_format($truckscale_fee->unit_price, 2), 0, 1, 'C');

        // // TOTAL
        // $this->fpdf->SetXY(168, 76);
        // $this->fpdf->SetTextColor(0, 0, 0);
        // $this->fpdf->SetFont('Roboto-Bold', 'B', 20);
        // $this->fpdf->CellFitScale(40, 5, number_format($truckscale_fee->amount, 2), 0, 1, 'C');

        // // TOTAL 2
        // $this->fpdf->SetXY(168, 110);
        // $this->fpdf->SetTextColor(0, 0, 0);
        // $this->fpdf->SetFont('Roboto-Bold', 'B', 20);
        // $this->fpdf->CellFitScale(40, 5, number_format($truckscale_fee->amount, 2), 0, 1, 'C');

        // PREPARED BY:
        $this->fpdf->SetXY(3, 136);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(28, 5, $prepared_by, 0, 1, 'C');

        // CHECKED BY:
        $this->fpdf->SetXY(32, 136);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(28, 5, $checked_by, 0, 1, 'C');

        // APPROVED BY:
        $this->fpdf->SetXY(62, 136);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(28, 5, $approved_by, 0, 1, 'C');


        // Output
        $this->fpdf->Output($filename, 'I');
        exit;
    }
}
