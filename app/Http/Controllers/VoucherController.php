<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;
use Codedge\Fpdf\Fpdf\Fpdf;
use App\Http\Custom\CustomFPDF;
use App\Models\Voucher;

use App\Exports\VouchersExport;
use Maatwebsite\Excel\Facades\Excel;

class VoucherController extends Controller
{
    public function index()
    {
        return view('vouchers.index');
    }

    public function singlePrint($id, $val)
    {
        $voucher = Voucher::where('id', $id)->orderBy('encoded_at', 'asc')->first();

        $prepared_by = ($voucher->prepared_by == '') ? ' ' : $voucher->prepared_by;
        $checked_by = ($voucher->checked_by == '') ? ' ' : $voucher->checked_by;
        $approved_by = ($voucher->approved_by == '') ? ' ' : $voucher->approved_by;

        $supplier_address = ($voucher->supplier->address == '' || $voucher->supplier->address == NULL) ? ' ' : $voucher->supplier->address;
        $ws_no = ($voucher->ws_no == '' || $voucher->ws_no == NULL) ? ' ' : $voucher->ws_no;
        $time_in = ($voucher->time_in == '' || $voucher->time_in == NULL) ? ' ' : date('h:i A', strtotime($voucher->time_in));
        $time_out = ($voucher->time_out == '' || $voucher->time_out == NULL) ? ' ' : date('h:i A', strtotime($voucher->time_out));

        $filename = 'Voucher (' . date('F j, Y', strtotime($voucher->encoded_at)) . ')';

        define('FPDF_FONTPATH', public_path('/fonts'));

        // $this->fpdf = New CustomFPDF('L', 'mm', ['210', '165']);
        $this->fpdf = new CustomFPDF('P', 'mm', 'letter');

        // 1st Page
        // $this->fpdf->AddPage('P', ['56', '87.7']);
        $this->fpdf->AddPage();
        $this->fpdf->AddFont('Roboto-Regular', 'R', 'Roboto-Regular.php');
        $this->fpdf->AddFont('Roboto-Bold', 'B', 'Roboto-Bold.php');
        $this->fpdf->SetMargins(0, 0, 0);
        $this->fpdf->SetAutoPageBreak(false);

        // Page Title
        $this->fpdf->SetTitle('Voucher');

        // Voucher Layout for reference
        // $this->fpdf->Image(public_path('images/Voucher 21(w)x16.5(h).jpg'), 0, 0, 210, 165);

        // For Valenzuela only
        if (session()->get('location_id') == 1) {
            // New Voucher Form
            $this->fpdf->Image(public_path('images/Voucher Form.jpg'), 0, 0, 216, 0);

            // CV No.
            $this->fpdf->SetXY(115, 16.5);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Bold', 'B', 16);
            $this->fpdf->CellFitScale(120, 5, $voucher->gcv_no, 0, 1, 'C');

            // Supplier
            $this->fpdf->SetXY(36, 26);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 20);
            $this->fpdf->CellFitScale(110, 5, $voucher->supplier_name, 0, 1, 'C');

            // Supplier Address
            $this->fpdf->SetXY(40, 35);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(106, 5, $supplier_address, 0, 1, 'C');

            // Date
            $this->fpdf->SetXY(163, 27);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(35, 5, date('F j, Y', strtotime($voucher->encoded_at)), 0, 1, 'C');

            // if (session()->get('location_code') == 'GLP') {
            $this->fpdf->SetXY(28, 50);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 8);

            // Header 1st
            $this->fpdf->Cell(60, 6, 'PLATE NO.:', 1, 0, 'L'); // 1st header column 
            $this->fpdf->Cell(60, 6, 'MATERIAL TYPE:', 1, 1, 'L'); // 2nd header column

            // Header 2nd
            $this->fpdf->SetX(28);
            $this->fpdf->Cell(60, 6, 'TIME IN:', 1, 0, 'L'); // 3rd header column 
            $this->fpdf->Cell(60, 6, 'GROSS WEIGHT:', 1, 1, 'L'); // 4th header column

            // Header 3rd
            $this->fpdf->SetX(28);
            $this->fpdf->Cell(60, 6, 'TIME OUT:', 1, 0, 'L'); // 5th header column 
            $this->fpdf->Cell(60, 6, 'TARE WEIGHT:', 1, 1, 'L'); // 6th header column

            // Header 4th
            $this->fpdf->SetX(28);
            $this->fpdf->Cell(60, 6, 'MC:', 1, 0, 'L'); // 7th header column 
            $this->fpdf->Cell(60, 6, 'DEDUCTED WEIGHT:', 1, 1, 'L'); // 8th header column

            // Header 5th
            $this->fpdf->SetX(28);
            $this->fpdf->Cell(60, 6, 'OT:', 1, 0, 'L'); // 9th header column 
            $this->fpdf->Cell(60, 6, 'CORRECTED WEIGHT:', 1, 1, 'L'); // 10th header column

            // Header 6th
            $this->fpdf->SetX(28);
            $this->fpdf->Cell(60, 6, 'PM:', 1, 0, 'L'); // 11th header column 
            if ($val == 1) {
                $this->fpdf->Cell(60, 6, 'UNIT PRICE:', 1, 1, 'L');
            } // 12th header column

            // 1st Column
            // Plate No.
            $this->fpdf->SetXY(46, 50.5);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(41, 5, $voucher->plate_no, 0, 1, 'L');

            // 1st Column
            // Time In
            $this->fpdf->SetXY(46, 57);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(41, 5, $time_in, 0, 1, 'L');

            // 1st Column
            // Time Out
            $this->fpdf->SetXY(46, 63);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(41, 5, $time_out, 0, 1, 'L');

            // 1st Column
            // MC
            $this->fpdf->SetXY(46, 69);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(41, 5, $voucher->mc . '%', 0, 1, 'L');

            // 1st Column
            // OT
            $this->fpdf->SetXY(46, 75);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(41, 5, $voucher->ot, 0, 1, 'L');

            // 1st Column
            // PM
            $this->fpdf->SetXY(46, 81);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(41, 5, $voucher->pm, 0, 1, 'L');

            // 2nd Column
            // Material Type
            $this->fpdf->SetXY(120, 50.5);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(30, 5, $voucher->materialType->name, 0, 1, 'L');

            // 2nd Column
            // Gross Weight
            $this->fpdf->SetXY(120, 57);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(30, 5, $voucher->gross_weight, 0, 1, 'L');

            // 2nd Column
            // Tare Weight
            $this->fpdf->SetXY(120, 63);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(30, 5, $voucher->tare_weight, 0, 1, 'L');

            // 2nd Column
            // Deducted Weight
            $this->fpdf->SetXY(120, 69);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(30, 5, $voucher->deducted_weight, 0, 1, 'L');

            // 2nd Column
            // Corrected Weight
            $this->fpdf->SetXY(120, 75);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(30, 5, $voucher->corrected_weight, 0, 1, 'L');

            if ($val == 1) {
                // 2nd Column
                // Unit Price
                $this->fpdf->SetXY(120, 81);
                $this->fpdf->SetTextColor(0, 0, 0);
                $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
                $this->fpdf->CellFitScale(30, 5, $voucher->unit_price, 0, 1, 'L');
            }


            if ($val == 1) {
                // TOTAL
                $this->fpdf->SetXY(40, 96);
                $this->fpdf->SetTextColor(0, 0, 0);
                $this->fpdf->SetFont('Roboto-Regular', 'R', 16);
                $this->fpdf->CellFitScale(90, 5, 'Php ' . number_format($voucher->amount, 2), 0, 1, 'C');

                // TOTAL 2
                $this->fpdf->SetXY(159, 96);
                $this->fpdf->SetTextColor(0, 0, 0);
                $this->fpdf->SetFont('Roboto-Regular', 'R', 16);
                $this->fpdf->CellFitScale(40, 5, number_format($voucher->amount, 2), 0, 1, 'C');
            }

            // PREPARED BY:
            $this->fpdf->SetXY(20, 116);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(23, 5, $prepared_by, 0, 1, 'C');

            // CHECKED BY:
            $this->fpdf->SetXY(46, 116);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(24, 5, $checked_by, 0, 1, 'C');

            // APPROVED BY:
            $this->fpdf->SetXY(72.5, 116);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 10);
            $this->fpdf->CellFitScale(25, 5, $approved_by, 0, 1, 'C');
        } else {

            // New Voucher Form
            // $this->fpdf->Image(public_path('images/Voucher Form.jpg'), 0, 0, 216, 0);

            // CV No.
            $this->fpdf->SetXY(120, 23.5);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 16);
            $this->fpdf->CellFitScale(120, 5, $voucher->gcv_no, 0, 1, 'C');

            // Supplier
            $this->fpdf->SetXY(26, 39);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 24);
            $this->fpdf->CellFitScale(120, 5, $voucher->supplier_name, 0, 1, 'C');

            // Supplier Address
            $this->fpdf->SetXY(26, 48);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            // $this->fpdf->CellFitScale(120, 5, $supplier_address, 0, 1, 'C');

            // Date
            $this->fpdf->SetXY(169, 40);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(38, 5, date('F j, Y', strtotime($voucher->encoded_at)), 0, 1, 'C');

            // if (session()->get('location_code') == 'GLP') {
            $this->fpdf->SetXY(10, 66);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);

            // Header 1st
            $this->fpdf->Cell(75, 7, 'PLATE NO.:', 1, 0, 'L'); // 1st header column 
            $this->fpdf->Cell(75, 7, 'MATERIAL TYPE:', 1, 1, 'L'); // 2nd header column

            // Header 2nd
            $this->fpdf->SetX(10);
            $this->fpdf->Cell(75, 7, 'TIME IN:', 1, 0, 'L'); // 3rd header column 
            $this->fpdf->Cell(75, 7, 'GROSS WEIGHT:', 1, 1, 'L'); // 4th header column

            // Header 3rd
            $this->fpdf->SetX(10);
            $this->fpdf->Cell(75, 7, 'TIME OUT:', 1, 0, 'L'); // 5th header column 
            $this->fpdf->Cell(75, 7, 'TARE WEIGHT:', 1, 1, 'L'); // 6th header column

            // Header 4th
            $this->fpdf->SetX(10);
            $this->fpdf->Cell(75, 7, 'MC:', 1, 0, 'L'); // 7th header column 
            $this->fpdf->Cell(75, 7, 'DEDUCTED WEIGHT:', 1, 1, 'L'); // 8th header column

            // Header 5th
            $this->fpdf->SetX(10);
            $this->fpdf->Cell(75, 7, 'OT:', 1, 0, 'L'); // 9th header column 
            $this->fpdf->Cell(75, 7, 'CORRECTED WEIGHT:', 1, 1, 'L'); // 10th header column

            // Header 6th
            $this->fpdf->SetX(10);
            $this->fpdf->Cell(75, 7, 'PM:', 1, 0, 'L'); // 11th header column 
            if ($val == 1) {
                $this->fpdf->Cell(75, 7, 'UNIT PRICE:', 1, 1, 'L');
            } // 12th header column

            // 1st Column
            // Plate No.
            $this->fpdf->SetXY(35, 67);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(45, 5, $voucher->plate_no, 0, 1, 'L');

            // 1st Column
            // Time In
            $this->fpdf->SetXY(35, 74);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(45, 5, $time_in, 0, 1, 'L');

            // 1st Column
            // Time Out
            $this->fpdf->SetXY(35, 81);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(45, 5, $time_out, 0, 1, 'L');

            // 1st Column
            // MC
            $this->fpdf->SetXY(35, 88);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(45, 5, $voucher->mc . '%', 0, 1, 'L');

            // 1st Column
            // OT
            $this->fpdf->SetXY(35, 95);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(45, 5, $voucher->ot, 0, 1, 'L');

            // 1st Column
            // PM
            $this->fpdf->SetXY(35, 102);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(45, 5, $voucher->pm, 0, 1, 'L');

            // 2nd Column
            // Material Type
            $this->fpdf->SetXY(130, 67);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(30, 5, $voucher->materialType->name, 0, 1, 'L');

            // 2nd Column
            // Gross Weight
            $this->fpdf->SetXY(130, 74);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(30, 5, $voucher->gross_weight, 0, 1, 'L');

            // 2nd Column
            // Tare Weight
            $this->fpdf->SetXY(130, 81);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(30, 5, $voucher->tare_weight, 0, 1, 'L');

            // 2nd Column
            // Deducted Weight
            $this->fpdf->SetXY(130, 88);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(30, 5, $voucher->deducted_weight, 0, 1, 'L');

            // 2nd Column
            // Corrected Weight
            $this->fpdf->SetXY(130, 95);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(30, 5, $voucher->corrected_weight, 0, 1, 'L');

            if ($val == 1) {
                // 2nd Column
                // Unit Price
                $this->fpdf->SetXY(130, 102);
                $this->fpdf->SetTextColor(0, 0, 0);
                $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
                $this->fpdf->CellFitScale(30, 5, $voucher->unit_price, 0, 1, 'L');
            }


            if ($val == 1) {
                // TOTAL
                $this->fpdf->SetXY(40, 115);
                $this->fpdf->SetTextColor(0, 0, 0);
                $this->fpdf->SetFont('Roboto-Regular', 'R', 20);
                $this->fpdf->CellFitScale(90, 5, 'Php ' . number_format($voucher->amount, 2), 0, 1, 'C');

                // TOTAL 2
                $this->fpdf->SetXY(166, 115);
                $this->fpdf->SetTextColor(0, 0, 0);
                $this->fpdf->SetFont('Roboto-Regular', 'R', 20);
                $this->fpdf->CellFitScale(40, 5, number_format($voucher->amount, 2), 0, 1, 'C');
            }

            // PREPARED BY:
            $this->fpdf->SetXY(10, 138);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(28, 5, $prepared_by, 0, 1, 'C');

            // CHECKED BY:
            $this->fpdf->SetXY(38, 138);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(29, 5, $checked_by, 0, 1, 'C');

            // APPROVED BY:
            $this->fpdf->SetXY(68, 138);
            $this->fpdf->SetTextColor(0, 0, 0);
            $this->fpdf->SetFont('Roboto-Regular', 'R', 12);
            $this->fpdf->CellFitScale(30, 5, $approved_by, 0, 1, 'C');
        }

        // Output
        $this->fpdf->Output($filename, 'I');
        exit;
    }
}
