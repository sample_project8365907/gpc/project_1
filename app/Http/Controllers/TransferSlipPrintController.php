<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Codedge\Fpdf\Fpdf\Fpdf;
use App\Http\Custom\CustomFPDF;
use App\Models\Delivery;

class TransferSlipPrintController extends Controller
{
    protected $fpdf;

    public function normalize($word)
    {
        $word = str_replace("Ñ", "%D1", $word);
        $word = str_replace("ñ", "%F1", $word);

        return urldecode($word);
    }

    public function print(Request $request)
    {
        $transfer_slip = Delivery::find($request->id);

        $material_type_name = ($transfer_slip->materialType->name != '') ? $transfer_slip->materialType->name : ' ';

        $current_location = session()->get('location_id');

        // Add -MB or Machine Baled if the baling station is Valenzuela.
        if ($current_location == 1) {
            $material_type_name = $material_type_name . '-MB';
        }

        $filename = 'Transfer Slip (' . date('F j, Y', strtotime($transfer_slip->encoded_at)) . ')';

        define('FPDF_FONTPATH', public_path('/fonts'));

        // $this->fpdf = New CustomFPDF('L', 'mm', ['212.09', '171.45']);
        $this->fpdf = new CustomFPDF('P', 'mm', 'letter');

        // 1st Page
        // $this->fpdf->AddPage('P', ['56', '87.7']);
        $this->fpdf->AddPage();
        $this->fpdf->AddFont('Roboto-Regular', 'R', 'Roboto-Regular.php');
        $this->fpdf->AddFont('Roboto-Bold', 'B', 'Roboto-Bold.php');
        $this->fpdf->SetMargins(0, 0, 0);
        $this->fpdf->SetAutoPageBreak(false);

        // Page Title
        $this->fpdf->SetTitle('Transfer Slip');

        // Transfer Slip Layout for reference
        // $this->fpdf->Image(public_path('images/Transfer Slip 8.35(w)x6.75(h).jpg'), 0, 0, 212.09, 171.45);

        // Add space if these fields are empty
        $description = (empty($transfer_slip->description)) ? ' ' : $transfer_slip->description;
        $ts_approved_by = (empty($transfer_slip->ts_approved_by)) ? ' ' : $transfer_slip->ts_approved_by;
        $ts_checked_by = (empty($transfer_slip->ts_checked_by)) ? ' ' : $transfer_slip->ts_checked_by;

        // Paper Mill
        $this->fpdf->SetXY(35, 42);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(98, 5, $this->normalize($transfer_slip->paperMill->name), 0, 1, 'L');

        // Paper Mill Address
        $this->fpdf->SetXY(35, 50);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(98, 5, $this->normalize($transfer_slip->paperMill->address), 0, 1, 'L');

        // Date Now
        $this->fpdf->SetXY(162, 42);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(42, 5, date('F j, Y', strtotime($transfer_slip->encoded_at)), 0, 1, 'L');

        // Truck No./Plate No.
        $this->fpdf->SetXY(170, 50);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(35, 5, $transfer_slip->vehicle->plate_no, 0, 1, 'L');

        // Quantity Bales
        $this->fpdf->SetXY(5, 80);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(30, 5, $transfer_slip->quantity_bales, 0, 1, 'C');

        // Quantity Bales Unit
        $this->fpdf->SetXY(38, 80);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(25, 5, 'BALES', 0, 1, 'C');

        // Net Weight
        $this->fpdf->SetXY(5, 100);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(30, 5, $transfer_slip->corrected_weight, 0, 1, 'C');

        // Quantity Kilos Unit
        $this->fpdf->SetXY(38, 100);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(25, 5, 'KGS', 0, 1, 'C');

        // Material Type / Hauler / Driver / Desicription
        $this->fpdf->SetXY(65, 80);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(140, 5, $material_type_name, 0, 1, 'C');

        $this->fpdf->SetXY(65, 100);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(140, 5, 'Hauler: ' . $this->normalize($transfer_slip->hauler->name), 0, 1, 'C');

        $this->fpdf->SetXY(65, 107);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(140, 5, 'Driver: ' . $this->normalize($transfer_slip->driver), 0, 1, 'C');

        $this->fpdf->SetXY(65, 114);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(140, 5, $this->normalize($description), 0, 1, 'C');

        $this->fpdf->SetXY(5, 152);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(60, 5, $this->normalize($ts_approved_by), 0, 1, 'L');

        $this->fpdf->SetXY(73, 152);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 12);
        $this->fpdf->CellFitScale(58, 5, $this->normalize($ts_checked_by), 0, 1, 'L');

        // Output
        $this->fpdf->Output($filename, 'I');
        exit;
    }
}
