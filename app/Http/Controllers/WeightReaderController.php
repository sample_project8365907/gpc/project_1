<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class WeightReaderController extends Controller
{
    public function store(Request $request)
    {
        Storage::disk('local')->put('public/test-weight/weight-1.txt', $request->weight);

        return $request->weight;
    }
}
