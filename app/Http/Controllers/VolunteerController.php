<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\Volunteer;
use App\Models\ScrapCurrentBatch;

class VolunteerController extends Controller
{
    public function index()
    {
        return view('volunteers.index');
    }

    public function edit($id)
    {
        $vid = $id;

        $volunteer = Volunteer::find($vid);
        $volunteer_code = $volunteer->code;

        return view('volunteers.edit', compact('vid', 'volunteer_code'));
    }

    public function scanQr()
    {
        return view('volunteers.scan-qr');
    }

    public function downloadCoupon(Request $request)
    {
        $current_batch_id = ScrapCurrentBatch::first('scrap_current_batch')->scrap_current_batch;
        $batch_id = $current_batch_id;
        // $current_location = session()->get('location_id');

        $volunteers_data = Volunteer::where('status_id', 1)
            ->orderBy('encoded_at', 'desc')
            ->orderBy('created_at', 'desc');

        $qr = [];
        $coupons = [];
        foreach ($volunteers_data->get() as $volunteer) {
            $store_coupon = 0;
            foreach ($volunteer->scraps()->where('scrap_batch_id', $batch_id)->where('status_id', 1)->get() as $key => $scrap) {
                $store_coupon += $scrap->weight;
            }

            $coupons = floor($store_coupon / 2);

            for ($i = 1; $i <= $coupons; $i++) {
                $qr[] = ['volunteer_name' => $volunteer->name, 'volunteer_code' => $volunteer->code, 'coupon_number' => $i, 'batch_id' => $batch_id];
            }
        }

        $blocks = array_chunk($qr, 5);

        $pdf = PDF::loadView('volunteers.download-coupon', compact('blocks'))
            ->setOptions([
                'chroot' => realpath(base_path()),
                'dpi' => 120,
                'defaultMediaType' => 'print',
                'isFontSubsettingEnabled' => 'true'
            ])
            ->setPaper('letter', 'portrait');

        return $pdf->stream();
    }
}
