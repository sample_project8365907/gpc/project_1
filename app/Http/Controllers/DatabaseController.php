<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\DatabaseHistory;

class DatabaseController extends Controller
{
    public function index()
    {
        return view('database.index');
    }

    public function export()
    {
        $mysqlHostName = env('DB_HOST');
        $mysqlUserName = env('DB_USERNAME');
        $mysqlPassword = env('DB_PASSWORD');
        $DbName        = env('DB_DATABASE');
        $path = 'files/';
        $file_name = $path . 'baling_prod_db_' . date('mdY') . '.sql';
        $file_name2 = 'baling_prod_db_' . date('mdY') . '.sql';
        $file_name_zip = $path . 'baling_prod_db_' . date('mdY') . '.sql.zip';

        $queryTables = \DB::select(\DB::raw('SHOW TABLES'));
        foreach ($queryTables as $table) {
            foreach ($table as $tName) {
                $tables[] = $tName;
            }
        }
        // For exporting specific table.
        // $tables  = array("users","products","categories");

        $connect = new \PDO("mysql:host=$mysqlHostName;dbname=$DbName;charset=utf8", "$mysqlUserName", "$mysqlPassword", array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        $get_all_table_query = "SHOW TABLES";
        $statement = $connect->prepare($get_all_table_query);
        $statement->execute();
        $output = '';
        foreach ($tables as $table) {
            $show_table_query = "SHOW CREATE TABLE " . $table . "";
            $statement = $connect->prepare($show_table_query);
            $statement->execute();
            $show_table_result = $statement->fetchAll();

            foreach ($show_table_result as $show_table_row) {
                $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
            }
            $select_query = "SELECT * FROM " . $table . "";
            $statement = $connect->prepare($select_query);
            $statement->execute();
            $total_row = $statement->rowCount();

            for ($count = 0; $count < $total_row; $count++) {
                $single_result = $statement->fetch(\PDO::FETCH_ASSOC);
                $table_column_array = array_keys($single_result);
                $table_value_array = array_values($single_result);
                $output .= "\nINSERT INTO $table (";
                $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
                $output .= "'" . implode("','", $table_value_array) . "');\n";
            }
        }

        // Create the sql file.
        $file_handle = fopen($file_name, 'w+');
        fwrite($file_handle, $output);
        fclose($file_handle);

        // Add to zip the created sql file.
        $zip = new \ZipArchive();
        $zip->open($file_name_zip, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $zip->addFile($file_name, $file_name2);
        $zip->close();

        unlink($file_name);

        if ($zip) {
            $data = [
                'user_id' => Auth::user()->id,
                'action' => 'export',
            ];

            DatabaseHistory::create($data);
        }

        return response()->download($file_name_zip)->deleteFileAfterSend(true);
    }
}
