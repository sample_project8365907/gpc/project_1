<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Delivery;
use App\Models\TruckingRate;
use App\Models\DeliverySummary;
use App\Models\AllowableMc;
use App\Models\Location;
use App\Exports\DeliveriesSummaryExport;
use Barryvdh\DomPDF\Facade\Pdf;
use Maatwebsite\Excel\Facades\Excel;


class DeliveryController extends Controller
{
    public function index()
    {
        return view('deliveries.index');
    }

    public function print(Request $request)
    {
        $current_location = session()->get('location_id');

        $date_from = $request->date_from_filter;
        $date_to = $request->date_to_filter;

        if ($date_from == '' || $date_to == '') {
            $date_from = now()->format('Y-m-d');
            $date_to = now()->format('Y-m-d');
        }

        if ($date_from == $date_to) {
            $date_title = date('F j, Y', strtotime($date_from));
            $filename = 'Daily Receiving - ' . $date_title . '.pdf';
        } else {
            $date_title = date('F j, Y', strtotime($date_from)) . ' to ' . date('F j, Y', strtotime($date_to));
            $filename = 'Daily Receiving from ' . $date_title . '.pdf';
        }

        $current_location = session()->get('location_id');
        $deliveries = Delivery::where('location_id', $current_location)
            ->when($request->material_type_filter, function ($query, $material_type) {
                return $query->where('material_type_id', $material_type);
            })
            ->when($date_from, function ($query, $date_from_filter) {
                return $query->whereDate('encoded_at', '>=', $date_from_filter);
            })
            ->when($date_to, function ($query, $date_to_filter) {
                return $query->whereDate('encoded_at', '<=', $date_to_filter);
            })
            ->where('status_id', 1)
            ->orderBy('encoded_at', 'desc')
            ->orderBy('id', 'desc')
            ->get();

        // For Total Summary -----------------------------------------------------------------------------
        $distinct_material_types = Delivery::distinct()
            ->where('location_id', $current_location)
            ->where('status_id', 1)
            ->whereBetween('encoded_at', [$date_from, $date_to])
            ->get(['material_type_id']);

        $distinct_paper_mills = Delivery::distinct()
            ->where('location_id', $current_location)
            ->where('status_id', 1)
            ->whereBetween('encoded_at', [$date_from, $date_to])
            ->get(['paper_mill_id']);

        $d_material_types = [];
        $d_paper_mills = [];
        $summaries = [];
        $total_summary_quantity = 0;
        $total_summary_volume = 0;
        foreach ($distinct_paper_mills as $distinct_paper_mill) {
            foreach ($distinct_material_types as $distinct_material_type) {
                $delivery = Delivery::where('location_id', $current_location)
                    ->where('paper_mill_id', $distinct_paper_mill->paper_mill_id)
                    ->where('material_type_id', $distinct_material_type->material_type_id)
                    ->where('status_id', 1)
                    ->whereBetween('encoded_at', [$date_from, $date_to])
                    ->get();

                $quantity = $delivery->pluck('quantity_bales')->sum();
                $volume = $delivery->pluck('corrected_weight')->sum();

                $summaries[] = [
                    'paper_mill' => $distinct_paper_mill->paperMill->name,
                    'material_type' => $distinct_material_type->materialType->name,
                    'quantity' => $quantity,
                    'volume' => $volume,
                ];

                $total_summary_quantity += $quantity;
                $total_summary_volume += $volume;
            }
        }
        // END For Total Summary ----------------------------------------------------------------------------

        $pdf = PDF::loadView('deliveries.print', compact('deliveries', 'date_from', 'date_to', 'date_title', 'summaries', 'total_summary_quantity', 'total_summary_volume'))
            ->setOptions([
                'chroot' => realpath(base_path()),
                'dpi' => 300,
                'defaultMediaType' => 'print',
                'isFontSubsettingEnabled' => 'true'
            ])
            ->setPaper('letter', 'landscape');

        return $pdf->stream();
    }

    public function summary()
    {
        return view('deliveries.summary');
    }

    public function printDeliveriesSummary(Request $request)
    {
        $current_location = session()->get('location_id');
        $material_type_id = $request->material_type_id;
        $date_from = $request->date_from;
        $date_to = $request->date_to;

        // Material Types
        $location = Location::find($current_location);

        if ($material_type_id == '') {
            $materials = $location->materialTypes;
        } else {
            $materials = $location->materialTypes()->where('id', $material_type_id)->get();
        }

        foreach ($materials as $material) {
            $material_type_ids[] = $material->id;
            $material_type_names[] = $material->name;
        }
        // End of Material Types

        if ($date_from == '' || $date_to == '') {
            $date_from = now()->format('Y-m-d');
            $date_to = now()->format('Y-m-d');
        }

        if ($date_from == $date_to) {
            $date_title = date('F j, Y', strtotime($date_from));
            $filename = 'Summary of Deliveries - ' . $date_title . '.xlsx';
        } else {
            $date_title = date('F j, Y', strtotime($date_from)) . ' to ' . date('F j, Y', strtotime($date_to));
            $filename = 'Summary of Deliveries from ' . $date_title . '.xlsx';
        }

        $deliveries = Delivery::whereBetween('encoded_at', [$date_from, $date_to])
            ->whereIn('material_type_id', $material_type_ids)
            ->where('location_id', $current_location)
            ->orderBy('encoded_at', 'asc')->get();

        return Excel::download(new DeliveriesSummaryExport($deliveries, $date_title, $material_type_ids, $material_type_names), $filename);
    }
}
