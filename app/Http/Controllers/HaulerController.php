<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Custom\CustomFPDF;
use App\Models\Hauler;
use App\Models\VehicleList;

class HaulerController extends Controller
{
    public function index()
    {
        return view('haulers.index');
    }

    public function edit($id)
    {
        $hid = $id;

        return view('haulers.edit', compact('hid'));
    }

    public function printHaulerForm()
    {
        $filename = 'HAULER FORM';

        define('FPDF_FONTPATH', public_path('/fonts'));

        // $fpdf = New CustomFPDF('L', 'mm', ['210', '165']);
        $fpdf = new CustomFPDF('P', 'mm', 'letter');

        // 1ST PAGE
        // $fpdf->AddPage('P', ['56', '87.7']);
        $fpdf->AddPage();
        $fpdf->AddFont('Roboto-Regular', 'R', 'Roboto-Regular.php');
        $fpdf->AddFont('Roboto-Bold', 'B', 'Roboto-Bold.php');
        $fpdf->SetMargins(0, 0, 0);
        $fpdf->SetAutoPageBreak(false);

        // PAGE TITLE
        $fpdf->SetTitle('Hauler Form');

        // HAULER FORM LAYOUT PAGE 1
        $fpdf->Image(public_path('images/Hauler Form Page1.jpg'), 0, 0, 216, 0);

        // 2ND PAGE
        $fpdf->AddPage();

        $fpdf->Image(public_path('images/Hauler Form Page2.jpg'), 0, 0, 216, 0);

        // OUTPUT
        $fpdf->Output($filename, 'I');
        exit;
    }

    public function printHaulerInfo($id)
    {
        $filename = 'HAULER INFORMATION';

        $hauler = Hauler::find($id);

        $company_name = ($hauler->name == '' || $hauler->name == NULL) ? ' ' : $hauler->name;
        $tin = ($hauler->tin == '' || $hauler->tin == NULL) ? ' ' : $hauler->tin;
        $vat = ($hauler->vat == '' || $hauler->vat == NULL) ? ' ' : $hauler->vat;
        $head_office_address = ($hauler->head_office_address == '' || $hauler->head_office_address == NULL) ? ' ' : $hauler->head_office_address;
        $head_office_tel_no = ($hauler->head_office_tel_no == '' || $hauler->head_office_tel_no == NULL) ? ' ' : $hauler->head_office_tel_no;
        $head_office_fax_no = ($hauler->head_office_fax_no == '' || $hauler->head_office_fax_no == NULL) ? ' ' : $hauler->head_office_fax_no;
        $head_office_email = ($hauler->head_office_email == '' || $hauler->head_office_email == NULL) ? ' ' : $hauler->head_office_email;
        $head_office_property = ($hauler->head_office_property == '' || $hauler->head_office_property == NULL) ? ' ' : $hauler->head_office_property;
        $plant_address = ($hauler->plant_address == '' || $hauler->plant_address == NULL) ? ' ' : $hauler->plant_address;
        $plant_tel_no = ($hauler->plant_tel_no == '' || $hauler->plant_tel_no == NULL) ? ' ' : $hauler->plant_tel_no;
        $plant_fax_no = ($hauler->plant_fax_no == '' || $hauler->plant_fax_no == NULL) ? ' ' : $hauler->plant_fax_no;
        $plant_email = ($hauler->plant_email == '' || $hauler->plant_email == NULL) ? ' ' : $hauler->plant_email;
        $plant_property = ($hauler->plant_property == '' || $hauler->plant_property == NULL) ? ' ' : $hauler->plant_property;
        $sec = ($hauler->sec == '' || $hauler->sec == NULL) ? ' ' : $hauler->sec;
        $dti = ($hauler->dti == '' || $hauler->dti == NULL) ? ' ' : $hauler->dti;
        $type_of_organization = ($hauler->type_of_organization == '' || $hauler->type_of_organization == NULL) ? ' ' : $hauler->type_of_organization;
        $line_of_business = ($hauler->line_of_business == '' || $hauler->line_of_business == NULL) ? ' ' : $hauler->line_of_business;
        $no_of_years = ($hauler->no_of_years == '' || $hauler->no_of_years == NULL) ? ' ' : $hauler->no_of_years;
        $documents = ($hauler->documents == '' || $hauler->documents == NULL || $hauler->documents == []) ? [] : json_decode($hauler->documents);

        $check = '3'; // Corresponding Key Cap for Check symbol of ZapfDingbats Font.

        define('FPDF_FONTPATH', public_path('/fonts'));

        // $fpdf = New CustomFPDF('L', 'mm', ['210', '165']);
        $fpdf = new CustomFPDF('P', 'mm', 'letter');

        // 1ST PAGE
        // $fpdf->AddPage('P', ['56', '87.7']);
        $fpdf->AddPage();
        $fpdf->AddFont('Roboto-Regular', 'R', 'Roboto-Regular.php');
        $fpdf->AddFont('Roboto-Bold', 'B', 'Roboto-Bold.php');
        $fpdf->AddFont('ZapfDingbats', '', 'ZapfDingbats.php');
        $fpdf->SetMargins(0, 0, 0);
        $fpdf->SetAutoPageBreak(false);

        // PAGE TITLE
        $fpdf->SetTitle('Hauler Information');

        // HAULER FORM LAYOUT PAGE 1
        $fpdf->Image(public_path('images/Hauler Form Page1.jpg'), 0, 0, 216, 0);

        // NAME OF COMPANY
        $fpdf->SetXY(14, 59);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(75, 5, $company_name, 0, 1, 'L');

        // TIN
        $fpdf->SetXY(92, 59);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(47, 5, $tin, 0, 1, 'L');

        // VAT
        if ($vat == 1) {
            $fpdf->SetXY(142, 54);
            $fpdf->SetFont('ZapfDingbats', '', 16);
            $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
        } elseif ($vat == 2) {
            $fpdf->SetXY(142, 62);
            $fpdf->SetFont('ZapfDingbats', '', 16);
            $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
        }

        // HEAD OFFICE ADDRESS
        $fpdf->SetXY(14, 79);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->MultiCell(75, 5, $head_office_address, 0, 'L', false);

        // HEAD OFFICE TEL NO.
        $fpdf->SetXY(119, 72.5);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(51, 5, $head_office_tel_no, 0, 1, 'L');

        // HEAD OFFICE FAX NO.
        $fpdf->SetXY(119, 83);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(51, 5, $head_office_fax_no, 0, 1, 'L');

        // HEAD OFFICE EMAIL ADDRESS
        $fpdf->SetXY(119, 94);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(51, 5, $head_office_email, 0, 1, 'L');

        // HEAD OFFICE PROPERTY
        if ($head_office_property == 1) {
            $fpdf->SetXY(177, 78);
            $fpdf->SetFont('ZapfDingbats', '', 16);
            $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
        } elseif ($head_office_property == 2) {
            $fpdf->SetXY(177, 89.7);
            $fpdf->SetFont('ZapfDingbats', '', 16);
            $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
        }

        // PLANT ADDRESS
        $fpdf->SetXY(14, 111);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->MultiCell(75, 5, $plant_address, 0, 'L', false);

        // PLANT TEL NO.
        $fpdf->SetXY(119, 104.5);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(51, 5, $plant_tel_no, 0, 1, 'L');

        // PLANT FAX NO.
        $fpdf->SetXY(119, 115);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(51, 5, $plant_fax_no, 0, 1, 'L');

        // PLANT EMAIL.
        $fpdf->SetXY(119, 126);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(51, 5, $plant_email, 0, 1, 'L');

        // PLANT OFFICE PROPERTY
        if ($plant_property == 1) {
            $fpdf->SetXY(177, 110);
            $fpdf->SetFont('ZapfDingbats', '', 16);
            $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
        } elseif ($plant_property == 2) {
            $fpdf->SetXY(177, 121.7);
            $fpdf->SetFont('ZapfDingbats', '', 16);
            $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
        }

        // PLANT BUSINESS LICENSE NO. SEC
        $fpdf->SetXY(25, 139.5);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(61, 5, $sec, 0, 1, 'L');

        // PLANT BUSINESS LICENSE NO. DTI
        $fpdf->SetXY(25, 145.5);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(61, 5, $dti, 0, 1, 'L');

        // TYPE OF ORGANIZATION
        if ($type_of_organization == 1) {
            $fpdf->SetXY(93, 143);
            $fpdf->SetFont('ZapfDingbats', '', 16);
            $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
        } elseif ($type_of_organization == 2) {
            $fpdf->SetXY(140, 143);
            $fpdf->SetFont('ZapfDingbats', '', 16);
            $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
        } elseif ($type_of_organization == 3) {
            $fpdf->SetXY(173, 143);
            $fpdf->SetFont('ZapfDingbats', '', 16);
            $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
        }

        // LINE OF BUSINESS
        $fpdf->SetXY(15, 158.5);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(73, 5, $line_of_business, 0, 1, 'L');

        // NO. OF YEARS IN BUSINESS
        $fpdf->SetXY(95, 158.5);
        $fpdf->SetTextColor(0, 0, 0);
        $fpdf->SetFont('Roboto-Regular', 'R', 11);
        $fpdf->CellFitScale(73, 5, $no_of_years, 0, 1, 'L');

        // DOCUMENTS NEEDED TO BE SUBMITTED
        if (count($documents) > 0) {
            foreach ($documents as $document) {
                switch ($document) {
                    case '1':
                        // No. 1
                        $fpdf->SetXY(58, 177);
                        $fpdf->SetFont('ZapfDingbats', '', 16);
                        $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
                        break;
                    case '2':
                        // No. 2
                        $fpdf->SetXY(58, 181);
                        $fpdf->SetFont('ZapfDingbats', '', 16);
                        $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
                        break;
                    case '3':
                        // No. 3
                        $fpdf->SetXY(58, 185);
                        $fpdf->SetFont('ZapfDingbats', '', 16);
                        $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
                        break;
                    case '4':
                        // No. 4
                        $fpdf->SetXY(58, 189.5);
                        $fpdf->SetFont('ZapfDingbats', '', 16);
                        $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
                        break;
                    case '5':
                        // No. 5
                        $fpdf->SetXY(58, 193.5);
                        $fpdf->SetFont('ZapfDingbats', '', 16);
                        $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
                        break;
                    case '6':
                        // No. 6
                        $fpdf->SetXY(58, 198);
                        $fpdf->SetFont('ZapfDingbats', '', 16);
                        $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
                        break;
                    case '7':
                        // No. 7
                        $fpdf->SetXY(58, 202);
                        $fpdf->SetFont('ZapfDingbats', '', 16);
                        $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
                        break;
                    case '8':
                        // No. 8
                        $fpdf->SetXY(58, 206);
                        $fpdf->SetFont('ZapfDingbats', '', 16);
                        $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
                        break;
                    case '9':
                        // No. 9
                        $fpdf->SetXY(58, 210);
                        $fpdf->SetFont('ZapfDingbats', '', 16);
                        $fpdf->CellFitScale(10, 5, $check, 0, 1, 'L');
                        break;
                    default:
                        '';
                }
            }
        }


        // 2ND PAGE
        $fpdf->AddPage();

        $fpdf->Image(public_path('images/Hauler Form Page2.jpg'), 0, 0, 216, 0);

        $vehicles = VehicleList::where('hauler_id', $hauler->id)->orderBy('created_at', 'asc')->get();

        if ($vehicles) {
            $y = 46;
            foreach ($vehicles as $index => $vehicle) {
                if ($index > 0) {
                    $y += 12.7; // This is additional Y coordinate for every row.
                }

                // PLATE NO.
                $fpdf->SetXY(19, $y);
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->SetFont('Roboto-Regular', 'R', 11);
                $fpdf->CellFitScale(38, 5, $vehicle->plate_no, 0, 1, 'C');

                // TYPE OF VEHICLE
                $fpdf->SetXY(64, $y);
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->SetFont('Roboto-Regular', 'R', 11);
                $fpdf->CellFitScale(50, 5, $vehicle->vehicleTypes->name, 0, 1, 'C');

                // TARE WEIGHT
                $fpdf->SetXY(121, $y);
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->SetFont('Roboto-Regular', 'R', 11);
                $fpdf->CellFitScale(44, 5, $vehicle->tare_weight, 0, 1, 'C');

                // CAPACITY
                $fpdf->SetXY(171.5, $y);
                $fpdf->SetTextColor(0, 0, 0);
                $fpdf->SetFont('Roboto-Regular', 'R', 11);
                $fpdf->CellFitScale(32, 5, $vehicle->capacity, 0, 1, 'C');
            }
        }

        // OUTPUT
        $fpdf->Output($filename, 'I');
        exit;
    }
}
