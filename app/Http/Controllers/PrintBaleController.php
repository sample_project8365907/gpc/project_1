<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Codedge\Fpdf\Fpdf\Fpdf;
use App\Http\Custom\CustomFPDF;
use App\Models\Production;
use Carbon\Carbon;

class PrintBaleController extends Controller
{

    protected $fpdf;

    public function normalize($word)
    {
        $word = str_replace("Ñ", "%D1", $word);
        $word = str_replace("ñ", "%F1", $word);

        return urldecode($word);
    }

    public function print(Request $request)
    {
        $production = Production::find($request->id);

        $weight = number_format($production->weight);

        $filename = 'Bale No. (' . $production->bale_no . ')';

        define('FPDF_FONTPATH', public_path('/fonts'));

        $this->fpdf = new CustomFPDF('L', 'mm', ['279.4', '215.9']);

        $this->fpdf->AddPage();
        $this->fpdf->AddFont('Roboto-Regular', 'R', 'Roboto-Regular.php');
        $this->fpdf->AddFont('Roboto-Bold', 'B', 'Roboto-Bold.php');
        $this->fpdf->SetMargins(0, 0, 0);
        $this->fpdf->SetAutoPageBreak(false);

        // Page Title
        $this->fpdf->SetTitle('Bale Number');

        // Baling Station
        $this->fpdf->SetXY(10, 10);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 24);
        $this->fpdf->CellFitScale(260, 15, strtoupper($production->location->name) . ' BALING STATION', 0, 1, 'L');

        // Date Produced
        $this->fpdf->SetXY(10, 25);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 24);
        $this->fpdf->CellFitScale(260, 15, 'Date Produced: ' . Carbon::parse($production->encoded_at)->format('F j, Y'), 0, 1, 'L');

        // Bale No.
        $current_location = session()->get('location_id');
        $date_produced = Carbon::parse($production->encoded_at)->format('mdy');
        $bale_no = str_replace($current_location . $date_produced, '', $production->bale_no);
        $this->fpdf->SetXY(10, 40);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 24);
        $this->fpdf->CellFitScale(260, 15, 'Bale No: ' . $bale_no, 0, 1, 'L');

        // Material Type
        $this->fpdf->SetXY(10, 55);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 24);
        $this->fpdf->CellFitScale(260, 15, 'Material Type: ' . $production->materialType->name, 0, 1, 'L');

        // Weight
        $this->fpdf->SetXY(10, 80);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 180);
        $this->fpdf->CellFitScale(260, 70, $weight, 0, 1, 'C');

        // Unit
        $this->fpdf->SetXY(10, 150);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 100);
        $this->fpdf->CellFitScale(260, 35, '(kg)', 0, 1, 'C');

        // Bale No. with current location and date produced
        $this->fpdf->SetXY(200, 190);
        $this->fpdf->SetTextColor(0, 0, 0);
        $this->fpdf->SetFont('Roboto-Bold', 'B', 40);
        $this->fpdf->CellFitScale(70, 20, $production->bale_no, 0, 1, 'C');

        // Output
        $this->fpdf->Output($filename, 'I');
        exit;
    }
}
