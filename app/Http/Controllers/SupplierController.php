<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Voucher;
use App\Models\Location;
use Barryvdh\DomPDF\Facade\Pdf;

class SupplierController extends Controller
{
    public function index()
    {
        return view('suppliers.index');
    }

    public function performanceReport()
    {
        return view('suppliers.performance-report');
    }

    public function form()
    {
        $current_location = session()->get('location_id');
        $location = Location::find($current_location);

        $baling_station = $location->name;

        $pdf = PDF::loadView('suppliers.form', compact('baling_station'))
            ->setOptions([
                'chroot' => realpath(base_path()),
                'dpi' => 300,
                'defaultMediaType' => 'print',
                'isFontSubsettingEnabled' => 'true'
            ])
            ->setPaper('letter', 'portrait');

        return $pdf->stream();
    }

    public function print(Request $request)
    {
        $search = $request->search;
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $material_type_filter = $request->material_type_filter;
        $view = $request->view;

        $current_location = session()->get('location_id');

        if ($date_from == $date_to) {
            $date_title = date('F j, Y', strtotime($date_from));
        } else {
            $date_title = date('F j, Y', strtotime($date_from)) . ' to ' . date('F j, Y', strtotime($date_to));
        }

        if ($view == '') {
            $table_cols = 7;
            $view_title = '(DETAILED)';
        } else {
            $table_cols = 5;
            $view_title = '(SUMMARY) - ' . $date_title;
        }

        if ($view == '') {
            // Detailed View
            $vouchers = Voucher::searchSupplier($search)
                ->where('location_id', $current_location)
                ->when($material_type_filter, function ($query, $material_type_filter) {
                    return $query->where('material_type_id', $material_type_filter);
                })
                ->whereBetween('encoded_at', [$date_from, $date_to])
                ->orderByRaw('CONVERT(amount, SIGNED) desc') // Convert amount to SIGNED (a signed 64-bit integer)
                ->get();
        } else {
            // Summary View
            $vouchers = Voucher::searchSupplier($search)
                ->selectRaw('*, sum(amount) as total_amount')
                ->where('location_id', $current_location)
                ->when($material_type_filter, function ($query, $material_type_filter) {
                    return $query->where('material_type_id', $material_type_filter);
                })
                ->whereBetween('encoded_at', [$date_from, $date_to])
                ->groupBy('supplier_id', 'material_type_id') // Enabled in config/database.php // In mysql array 'strict' => false
                ->orderByRaw('CONVERT(amount, SIGNED) desc') // Convert amount to SIGNED (a signed 64-bit integer)
                ->get();
        }

        $pdf = PDF::loadView('suppliers.print', compact('vouchers', 'search', 'date_from', 'date_to', 'material_type_filter', 'view', 'date_title', 'view_title', 'table_cols'))
            ->setOptions([
                'chroot' => realpath(base_path()),
                'dpi' => 300,
                'defaultMediaType' => 'print',
                'isFontSubsettingEnabled' => 'true'
            ])
            ->setPaper('letter', 'portrait');

        // Page number 
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->getCanvas();
        $canvas->page_script(function ($pageNumber, $pageCount, $canvas, $fontMetrics) {
            $text = "Page $pageNumber of $pageCount";
            $font = $fontMetrics->getFont('Nunito-400');
            $pageWidth = $canvas->get_width();
            $pageHeight = $canvas->get_height();
            $size = 6;
            $width = $fontMetrics->getTextWidth($text, $font, $size);
            $canvas->text($pageWidth - $width - 40, $pageHeight - 40, $text, $font, $size);
        });

        return $pdf->stream();
    }
}
