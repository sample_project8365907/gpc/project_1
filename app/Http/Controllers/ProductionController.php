<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Production;
use Barryvdh\DomPDF\Facade\Pdf;

class ProductionController extends Controller
{
    public function index()
    {
        return view('productions.index');
    }

    public function print(Request $request)
    {
        $date_from = $request->date_from_filter;
        $date_to = $request->date_to_filter;

        if($date_from == '' || $date_to == '') {
            $date_from = now()->format('Y-m-d');
            $date_to = now()->format('Y-m-d');
        }

        if($date_from == $date_to) {
            $date_title = date('F j, Y', strtotime($date_from));
        } else {
            $date_title = date('F j, Y', strtotime($date_from)).' to '.date('F j, Y', strtotime($date_to));
        }

        $current_location = session()->get('location_id');
        $productions = Production::where('location_id', $current_location)
                                    // ->where('status_filter', $request->status_filter)
                                    ->when($request->status_filter, function($query, $status) {
                                        return $query->where('status_id', $status);
                                    })
                                    ->when($request->material_type_filter, function($query, $material_type) {
                                        return $query->where('material_type_id', $material_type);
                                    })
                                    ->when($date_from, function($query, $date_from_filter) {
                                        return $query->whereDate('encoded_at', '>=', $date_from_filter);
                                    })
                                    ->when($date_to, function($query, $date_to_filter) {
                                        return $query->whereDate('encoded_at', '<=', $date_to_filter);
                                    })
                                    ->orderBy('encoded_at', 'asc')->get();
        $produced_count = 1;

        $prod_locc_qty = $productions->whereIn('material_type_id', [1,3])->count();
        $prod_locc_volume = $productions->whereIn('material_type_id', [1,3])->pluck('weight')->sum();
        $prod_mw_qty = $productions->where('material_type_id', 2)->count();
        $prod_mw_volume = $productions->where('material_type_id', 2)->pluck('weight')->sum();
        $prod_qty_total = $prod_locc_qty + $prod_mw_qty;
        $prod_volume_total = $prod_locc_volume + $prod_mw_volume;

        $pdf = PDF::loadView('productions.print', compact('productions', 'produced_count', 'prod_locc_qty', 'prod_locc_volume', 'prod_mw_qty', 'prod_mw_volume', 'prod_qty_total', 'prod_volume_total', 'date_title'))
                    ->setOptions([
                        'chroot' => realpath(base_path()),
                        'dpi' => 300,
                        'defaultMediaType' => 'print',
                        'isFontSubsettingEnabled' => 'true'
                    ])
                    ->setPaper('letter', 'portrait');

        return $pdf->stream();
    }
}
