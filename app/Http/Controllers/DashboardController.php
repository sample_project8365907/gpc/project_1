<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        if ((count(auth()->user()->permissions) == 0)) {
            return view('welcome');
        } else {
            return view('dashboard');
        }
    }
}
