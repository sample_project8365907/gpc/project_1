<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaperMillController extends Controller
{
    public function index()
    {
        return view('paper-mills.index');
    }

    public function edit($id)
    {
        $pm_id = $id;

        return view('paper-mills.edit', compact('pm_id'));
    }
}
