<?php

namespace App\Http\Livewire\MaterialTypes;

use Livewire\Component;
use App\Models\MaterialType;

class Delete extends Component
{
    public $mtid;
    public $name;

    public $isDeleteModalOpen = false;

    protected $listeners = [
        'open-delete-modal' => 'openDeleteModal',
    ];

    public function render()
    {
        return view('livewire.material-types.delete');
    }

    public function openDeleteModal($id)
    {
        $material_type = MaterialType::find($id);

        $this->mtid = $material_type->id;
        $this->name = $material_type->name;

        $this->isDeleteModalOpen = true;
    }

    public function closeDeleteModal()
    {
        $this->isDeleteModalOpen = false;
    }

    public function delete()
    {
        MaterialType::where('id', $this->mtid)->firstOrFail()->update(['status_id' => 2]);

        // Refresh vouchers list
        $this->emit('refresh-material-types');

        // Notification
        $status = 'success';
        $message = $this->name . ' was successfully deleted.';
        $this->emit('show-notif', $status, $message);

        $this->isDeleteModalOpen = false;
    }
}
