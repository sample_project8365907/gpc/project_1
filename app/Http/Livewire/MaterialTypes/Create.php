<?php

namespace App\Http\Livewire\MaterialTypes;

use Livewire\Component;
use App\Models\MaterialType;

class Create extends Component
{
    public $name;

    public function render()
    {
        return view('livewire.material-types.create');
    }

    public function store()
    {
        $validatedData = $this->validate([
            'name' => ['required', 'unique:material_types'],
        ]);

        MaterialType::create($validatedData);

        // Refresh vouchers list
        $this->emit('refresh-material-types');

        // Notification
        $status = 'success';
        $message = 'New Material Type successfully added.';
        $this->emit('show-notif', $status, $message);

        $this->reset(['name']);
    }
}
