<?php

namespace App\Http\Livewire\MaterialTypes;

use Livewire\Component;
use App\Models\MaterialType;

class Edit extends Component
{
    public $mtid;
    public $name;

    public $isEditModalOpen = false;

    protected $listeners = [
        'open-edit-modal' => 'openEditModal',
    ];

    public function render()
    {
        return view('livewire.material-types.edit');
    }

    public function openEditModal($id)
    {
        $material_type = MaterialType::find($id);

        $this->mtid = $material_type->id;
        $this->name = $material_type->name;

        $this->isEditModalOpen = true;
    }

    public function closeEditModal()
    {
        $this->isEditModalOpen = false;
    }

    public function update()
    {
        $validatedData = $this->validate([
            'name' => ['required', 'unique:material_types,name,' . $this->mtid . ',id'],
        ]);

        MaterialType::where('id', $this->mtid)->firstOrFail()->update($validatedData);

        // Refresh vouchers list
        $this->emit('refresh-material-types');

        // Notification
        $status = 'success';
        $message = 'Material Type successfully updated.';
        $this->emit('show-notif', $status, $message);

        $this->isEditModalOpen = false;

        $this->reset(['name']);
    }
}
