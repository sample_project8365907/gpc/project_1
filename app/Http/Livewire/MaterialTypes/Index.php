<?php

namespace App\Http\Livewire\MaterialTypes;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\MaterialType;

class Index extends Component
{
    use WithPagination;

    protected $listeners = [
        'refresh-material-types' => '$refresh',
    ];

    public function render()
    {
        $material_types = MaterialType::where('status_id', 1)->orderBy('id', 'asc')->paginate(20);

        return view('livewire.material-types.index', compact('material_types'));
    }
}
