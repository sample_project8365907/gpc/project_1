<?php

namespace App\Http\Livewire\VolunteerScraps;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\VolunteerScrap;
use App\Models\ScrapBatch;
use App\Models\ScrapCurrentBatch;

class Index extends Component
{
    use WithPagination;

    public $vid;
    public $volunteer_code;

    public $batch_id;
    public $search;

    protected $listeners = [
        'refresh-volunteer-scraps' => '$refresh',
    ];

    public function updatingBatchId()
    {
        $this->resetPage();
    }

    public function mount()
    {
        $current_batch_id = ScrapCurrentBatch::first('scrap_current_batch')->scrap_current_batch;

        $this->batch_id = $current_batch_id;
    }

    public function render()
    {
        $batch_id = $this->batch_id;

        $volunteer_scraps_data = VolunteerScrap::where('volunteer_id', $this->vid)
            ->when($batch_id, function ($query, $batch_id) {
                return $query->where('scrap_batch_id', $batch_id);
            })
            ->where('status_id', 1)
            ->orderBy('encoded_at', 'desc')
            ->orderBy('created_at', 'desc');

        $total_pet = 0;
        $total_sf = 0;
        $total_pet = VolunteerScrap::where('volunteer_id', $this->vid)->where('material_type_id', 1)->where('status_id', 1)->where('scrap_batch_id', $batch_id)->get(['weight'])->pluck('weight')->sum();
        $total_sf =  VolunteerScrap::where('volunteer_id', $this->vid)->where('material_type_id', 2)->where('status_id', 1)->where('scrap_batch_id', $batch_id)->get(['weight'])->pluck('weight')->sum();
        $total_weight = $total_pet + $total_sf;
        // $total_weight = $volunteer_scraps_data->pluck('weight')->sum();

        $total_coupons = floor($total_weight / 2);

        $volunteer_scraps = $volunteer_scraps_data->paginate(10);

        $batches = ScrapBatch::orderBy('created_at', 'asc')->get();

        return view('livewire.volunteer-scraps.index', compact('volunteer_scraps', 'batches', 'total_pet', 'total_sf', 'total_weight', 'total_coupons'));
    }
}
