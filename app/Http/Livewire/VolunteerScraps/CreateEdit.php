<?php

namespace App\Http\Livewire\VolunteerScraps;

use Livewire\Component;
use App\Models\VolunteerScrap;
use App\Models\Location;
use App\Models\MaterialType;
use App\Models\ScrapCurrentBatch;
use App\Models\ExcessScrap;

class CreateEdit extends Component
{
    public $vid;
    public $vsid;
    public $material_type_id;
    public $weight;
    public $batch_id;

    public $action;
    public $form_title;
    public $button_name;

    public $isCreateEditModalOpen = false;

    protected $listeners = [
        'open-create-modal' => 'openCreateModal',
        'open-edit-modal' => 'openEditModal',
    ];

    public function mount()
    {
        $current_batch_id = ScrapCurrentBatch::first('scrap_current_batch')->scrap_current_batch;

        $this->batch_id = $current_batch_id;
    }

    public function render()
    {
        $material_types = MaterialType::orderBy('name', 'asc')->get();

        return view('livewire.volunteer-scraps.create-edit', compact('material_types'));
    }

    public function openCreateModal()
    {
        $this->action = 'store';
        $this->form_title = 'Add';
        $this->button_name = 'Submit';

        $this->reset('material_type_id', 'weight');
        $this->resetValidation();

        $this->isCreateEditModalOpen = true;
    }

    public function store()
    {
        $validated_data = $this->validate([
            'material_type_id' => ['required'],
            'weight' => ['required', 'numeric'],
        ]);

        $validated_data['volunteer_id'] = $this->vid;
        $validated_data['scrap_batch_id'] = $this->batch_id;
        $validated_data['status_id'] = 1;
        $validated_data['encoded_at'] = now()->format('Y-m-d');

        $create = VolunteerScrap::create($validated_data);

        if ($create) {
            $this->computeExcessWeight();
        }

        $this->emit('refresh-volunteer-scraps');

        // Notification
        if ($create) {
            $status = 'success';
            $message = 'Scrap successfully added.';
        } else {
            $status = 'error';
            $message = 'Something went wrong.';
        }
        $this->emit('show-notif', $status, $message);

        $this->isCreateEditModalOpen = false;
    }

    public function openEditModal($id)
    {
        $this->action = 'update';
        $this->form_title = 'Edit';
        $this->button_name = 'Update';

        $volunteer_scrap = VolunteerScrap::find($id);

        $this->vsid = $volunteer_scrap->id;
        $this->material_type_id = $volunteer_scrap->material_type_id;
        $this->weight = $volunteer_scrap->weight;

        $this->isCreateEditModalOpen = true;
    }

    public function update()
    {
        $validated_data = $this->validate([
            'material_type_id' => ['required'],
            'weight' => ['required', 'numeric'],
        ]);

        $update = VolunteerScrap::where('id', $this->vsid)->update($validated_data);

        if ($update) {
            $this->computeExcessWeight();
        }

        $this->emit('refresh-volunteer-scraps');

        // Notification
        if ($update) {
            $status = 'success';
            $message = 'Scrap successfully updated.';
        } else {
            $status = 'error';
            $message = 'Something went wrong.';
        }
        $this->emit('show-notif', $status, $message);

        $this->isCreateEditModalOpen = false;
    }

    public function computeExcessWeight()
    {
        // Computation of excess scrap weight
        TODO: // Need to update this code in 2nd batch, separate the total weight with material types;
        $total_weight = VolunteerScrap::where('volunteer_id', $this->vid)->pluck('weight')->sum();

        $total_coupons = floor($total_weight / 2);

        // Get the excess weight
        $excess_weight = $total_weight - ($total_coupons * 2);

        // Solution for wrong output when subtracting floating point. Encounter the problem to this number (36.06 - 36).
        // When the decimals is greater than 10, it will use the number format function with 2 decimals.
        $decimal_count = (int) strpos(strrev($excess_weight), ".");
        if ($decimal_count > 10) {
            $excess_weight = number_format($excess_weight, 2);
        }

        // Check if the volunteer has a record already in excess_scrap table
        $volunteer_record = ExcessScrap::where('volunteer_id', $this->vid)->first();

        if ($volunteer_record) {
            ExcessScrap::where('volunteer_id', $this->vid)->update([
                // Need to add material type here in 2nd batch.
                'excess_weight' => $excess_weight,
            ]);
        } else {
            ExcessScrap::create([
                'volunteer_id' => $this->vid,
                'material_type_id' => 2, // This will need to change based on the material type in 2nd batch.
                'excess_weight' => $excess_weight,
                'scrap_current_batch' => $this->batch_id,
                'status_id' => 1,
            ]);
        }
    }

    public function closeCreateEditModal()
    {
        $this->isCreateEditModalOpen = false;
    }
}
