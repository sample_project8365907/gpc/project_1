<?php

namespace App\Http\Livewire\VolunteerScraps;

use Livewire\Component;
use App\Models\VolunteerScrap;

class Delete extends Component
{
    public $vsid;
    public $material_type_name;
    public $weight;

    public $isDeleteModalOpen = false;

    protected $listeners = [
        'open-delete-modal' => 'openDeleteModal',
    ];

    public function render()
    {
        return view('livewire.volunteer-scraps.delete');
    }

    public function openDeleteModal($id)
    {
        $volunteer_scrap = VolunteerScrap::find($id);

        $this->vsid = $volunteer_scrap->id;
        $this->material_type_name = $volunteer_scrap->materialType->name;
        $this->weight = $volunteer_scrap->weight;

        $this->isDeleteModalOpen = true;
    }

    public function delete()
    {
        $delete = VolunteerScrap::where('id', $this->vsid)->update(['status_id' => 2]);

        $this->emit('refresh-volunteer-scraps');

        // Notification
        if ($delete) {
            $status = 'success';
            $message = 'Scrap successfully deleted.';
        } else {
            $status = 'error';
            $message = 'Something went wrong.';
        }
        $this->emit('show-notif', $status, $message);

        $this->isDeleteModalOpen = false;
    }

    public function closeDeleteModal()
    {
        $this->isDeleteModalOpen = false;
    }
}
