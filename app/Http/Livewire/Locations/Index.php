<?php

namespace App\Http\Livewire\Locations;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Location;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $listeners = [
        'refresh-locations' => '$refresh',
    ];

    public function render()
    {   
        $locations = Location::search($this->search)->orderBy('created_at', 'desc')->paginate(10);

        return view('livewire.locations.index', compact('locations'));
    }
}
