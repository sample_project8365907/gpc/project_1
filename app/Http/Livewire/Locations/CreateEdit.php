<?php

namespace App\Http\Livewire\Locations;

use Livewire\Component;
use App\Models\Location;
use App\Models\MaterialType;

class CreateEdit extends Component
{
    public $lid;
    public $name;
    public $code;

    public $old_selected_material_types = [];
    public $selected_material_types = [];
    public $index = 0;

    public $action;
    public $form_title;
    public $button_name;

    public $isCreateEditModalOpen = false;

    protected $listeners = [
        'open-create-modal' => 'openCreateModal',
        'open-edit-modal' => 'openEditModal',
    ];

    public function render()
    {
        $material_types = MaterialType::where('status_id', 1)->get();

        return view('livewire.locations.create-edit', compact('material_types'));
    }

    public function openCreateModal()
    {
        $this->reset(['name', 'code', 'selected_material_types']);

        $this->action = 'store';
        $this->form_title = 'Add';
        $this->button_name = 'Submit';

        $this->isCreateEditModalOpen = true;
    }

    public function store()
    {
        $validatedData = $this->validate([
            'name' => ['required'],
            'code' => ['required'],
        ]);

        $location = Location::create($validatedData);

        if ($location) {
            // Save material types
            $location->materialTypes()->sync($this->selected_material_types);
        }

        // Refresh baling stations/locations list
        $this->emit('refresh-locations');

        $this->isCreateEditModalOpen = false;

        // Notification
        $status = 'success';
        $message = $this->name . ' successfully added.';
        $this->emit('show-notif', $status, $message);
    }

    public function openEditModal($id)
    {
        $this->action = 'update';
        $this->form_title = 'Edit';
        $this->button_name = 'Update';

        $location = Location::find($id);

        $this->lid = $location->id;
        $this->name = $location->name;
        $this->code = $location->code;
        $this->old_selected_material_types = array_map('strval', $location->materialTypes->pluck('id')->toArray());
        $this->selected_material_types = array_map('strval', $location->materialTypes->pluck('id')->toArray());

        $this->isCreateEditModalOpen = true;
    }

    public function update()
    {
        $validatedData = $this->validate([
            'name' => ['required'],
            'code' => ['required'],
        ]);

        $location = Location::where('id', $this->lid);

        // Update baling station/location info
        $update = $location->firstOrFail()->update($validatedData);

        // Save selected material types
        $sync = $location->first()->materialTypes()->sync($this->selected_material_types);

        // Log if theres a changes.
        if ($this->selected_material_types != $this->old_selected_material_types) {
            // Log the sync to related model.
            activity()
                ->performedOn($location->first())
                ->causedBy(auth()->user())
                ->withProperties(['attributes' => $this->selected_material_types, 'old' => $this->old_selected_material_types])
                ->log(':causer.name updated the material types of :subject.name');
        }

        // Refresh baling stations/locations list
        $this->emit('refresh-locations');

        $this->isCreateEditModalOpen = false;

        // Notification
        $status = 'success';
        $message = 'Baling Station successfully updated.';
        $this->emit('show-notif', $status, $message);
    }

    public function closeCreateEditModal()
    {
        $this->isCreateEditModalOpen = false;
    }
}
