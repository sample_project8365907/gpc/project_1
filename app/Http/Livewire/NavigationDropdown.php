<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Str;
use App\Models\Location;

class NavigationDropdown extends Component
{
    public $location;

    protected $listeners = [
        'refresh-navigation-dropdown' => '$refresh',
    ];

    public function render()
    {
        // Check if location session is set
        if (session()->get('location_id')) {
            $this->location = session()->get('location_id');
        } else {
            session()->put('location_id', Auth::user()->default_location);
            $this->location = Auth::user()->default_location;
        }

        // Set location name and code
        $location = Location::where('id', $this->location)->first();
        session()->put('location', $location->name);
        session()->put('location_code', $location->code);
        session()->put('location_program', $location->program);

        // Display all assigned location for user
        $user_id = Auth::id();
        $locations = Location::whereHas('users', function ($query) use ($user_id) {
            $query->where('id', $user_id);
        })->orderBy('name', 'asc')->get();

        $location_name = session()->get('location');

        return view('livewire.navigation-dropdown', compact('locations', 'location_name'));
    }

    public function updatedLocation()
    {
        // Update location ID when select input changed
        session()->put('location_id', $this->location);

        // Set location name and code
        $location = Location::where('id', $this->location)->first();
        session()->put('location', $location->name);
        session()->put('location_code', $location->code);
        session()->put('location_program', $location->program);

        return redirect('/');
    }
}
