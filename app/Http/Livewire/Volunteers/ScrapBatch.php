<?php

namespace App\Http\Livewire\Volunteers;

use Livewire\Component;
use App\Models\ScrapCurrentBatch;

class ScrapBatch extends Component
{
    public $scrap_current_batch;

    public $action = 'update';
    public $button_name = 'Update';

    public $isScrapBatchModalOpen = false;

    protected $listeners = [
        'open-scrap-batch-modal' => 'openScrapBatchModal',
    ];

    public function render()
    {
        return view('livewire.volunteers.scrap-batch');
    }

    public function openScrapBatchModal()
    {
        $this->isScrapBatchModalOpen = true;

        $scrap_current_batch = ScrapCurrentBatch::find(1);

        $this->scrap_current_batch = $scrap_current_batch->scrap_current_batch;
    }

    public function update()
    {
        $validated_data = $this->validate([
            'scrap_current_batch' => ['required', 'numeric', 'gt:0'],
        ]);

        $previous_batch_no = ScrapCurrentBatch::where('id', 1)->first()->scrap_current_batch;

        $update = ScrapCurrentBatch::where('id', 1)->update($validated_data);

        // Notification
        if ($update) {
            $status = 'success';
            $message = 'Scrap Batch successfully updated.';
        } else {
            $status = 'error';
            $message = 'Something went wrong.';
        }
        $this->emit('show-notif', $status, $message);

        return redirect('/volunteers');
    }

    public function closeScrapBatchModal()
    {
        $this->isScrapBatchModalOpen = false;
    }
}
