<?php

namespace App\Http\Livewire\Volunteers;

use Livewire\Component;
use App\Models\Volunteer;
use App\Models\VolunteerScrap;
use App\Models\Location;
use App\Models\ScrapCurrentBatch;

class AddScrap extends Component
{
    public $vid;
    public $code;
    public $name;
    public $material_type_id;
    public $weight;
    public $batch_id;

    public $action = 'store';
    public $form_title = 'Add';
    public $button_name = 'Submit';

    public $isAddScrapModalOpen = false;

    protected $listeners = [
        'open-add-scrap-modal' => 'openAddScrapModal',
    ];

    public function mount()
    {
        $current_batch_id = ScrapCurrentBatch::first('scrap_current_batch')->scrap_current_batch;
        $this->batch_id = $current_batch_id;
    }

    public function render()
    {
        $current_location = session()->get('location_id');

        $material_types = Location::where('id', $current_location)
            ->where('program', 2)->first()->materialTypes;

        return view('livewire.volunteers.add-scrap', compact('material_types'));
    }

    public function openAddScrapModal($code)
    {
        $this->reset(['material_type_id', 'weight']);

        $result = Volunteer::where('code', $code)->first();

        $this->vid = $result->id;
        $this->code = $result->code;
        $this->name = $result->name;

        $this->isAddScrapModalOpen = true;
    }

    public function store()
    {
        $validated_data = $this->validate([
            'material_type_id' => ['required'],
            'weight' => ['required', 'numeric'],
        ]);

        $validated_data['volunteer_id'] = $this->vid;
        $validated_data['scrap_batch_id'] = $this->batch_id;
        $validated_data['status_id'] = 1;
        $validated_data['encoded_at'] = now()->format('Y-m-d');

        $create = VolunteerScrap::create($validated_data);

        // Notification
        if ($create) {
            $status = 'success';
            $message = 'Scrap successfully added.';
        } else {
            $status = 'error';
            $message = 'Something went wrong.';
        }
        $this->emit('show-notif', $status, $message);

        $this->emitTo('volunteers.scan-qr', 'clear-fields');

        $this->isAddScrapModalOpen = false;
    }

    public function closeAddScrapModal()
    {
        $this->emitTo('volunteers.scan-qr', 'clear-fields');

        $this->isAddScrapModalOpen = false;
    }
}
