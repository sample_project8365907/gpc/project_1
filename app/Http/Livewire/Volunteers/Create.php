<?php

namespace App\Http\Livewire\Volunteers;

use Livewire\Component;
use App\Models\Volunteer;

class Create extends Component
{
    public $code;
    public $name;
    public $address;
    public $company_name;
    public $contact_no;
    public $location_id;
    public $encoded_at;

    public $action;
    public $form_title;
    public $button_name;

    public $isCreateEditModalOpen = false;

    protected $listeners = [
        'open-create-modal' => 'openCreateModal',
        'open-edit-modal' => 'openEditModal',
    ];

    public function render()
    {
        return view('livewire.volunteers.create');
    }

    public function openCreateModal()
    {
        $this->reset([
            'code',
            'name',
            'address',
            'company_name',
            'contact_no',
            'location_id',
            'encoded_at',
        ]);

        $this->action = 'store';
        $this->form_title = 'Add';
        $this->button_name = 'Submit';

        $this->encoded_at = now()->format('Y-m-d');

        $this->isCreateEditModalOpen = true;
    }

    public function store()
    {
        $validated_data = $this->validate([
            'name' => ['required'],
            'address' => ['nullable'],
            'company_name' => ['nullable'],
            'contact_no' => ['nullable'],
            'encoded_at' => ['required'],
        ]);

        // Code Format Example: 1061422001
        // 1 - Current Location, 06 - Month, 14 - Day, 22 - Year, 001 - auto increment
        $current_location = session()->get('location_id');
        $last_id = Volunteer::where('location_id', $current_location)
            ->whereDate('encoded_at', $this->encoded_at)
            ->latest()->first();
        $code_no = ($last_id) ? substr($last_id->code, -3) + 1 : 1;
        $code = $current_location . date('mdy', strtotime($this->encoded_at)) . str_pad($code_no, 3, 0, STR_PAD_LEFT);

        $validated_data['code'] = $code;
        $validated_data['name'] = strtoupper($this->name);
        $validated_data['address'] = strtoupper($this->address);
        $validated_data['company_name'] = strtoupper($this->company_name);
        $validated_data['location_id'] = $current_location;
        $validated_data['status_id'] = 1;

        Volunteer::create($validated_data);

        // Refresh volunteers list
        $this->emit('refresh-volunteers');

        // Notification
        $status = 'success';
        $message = 'New Volunteer successfully added.';
        $this->emit('show-notif', $status, $message);

        $this->isCreateEditModalOpen = false;
    }

    public function closeCreateEditModal()
    {
        $this->isCreateEditModalOpen = false;
    }
}
