<?php

namespace App\Http\Livewire\Volunteers;

use Livewire\Component;
use App\Models\Volunteer;

class Edit extends Component
{
    public $vid;
    public $code;
    public $name;
    public $address;
    public $company_name;
    public $contact_no;

    public function mount($vid)
    {
        $volunteer = Volunteer::find($vid);

        $this->vid = $volunteer->id;
        $this->code = $volunteer->code;
        $this->name = $volunteer->name;
        $this->address = $volunteer->address;
        $this->company_name = $volunteer->company_name;
        $this->contact_no = $volunteer->contact_no;
    }

    public function render()
    {
        return view('livewire.volunteers.edit');
    }

    public function update()
    {
        $validated_data = $this->validate([
            'name' => ['required'],
            'address' => ['nullable'],
            'company_name' => ['nullable'],
            'contact_no' => ['nullable'],
        ]);

        $update = Volunteer::where('id', $this->vid)->firstOrFail()->update($validated_data);

        // Notification
        if ($update) {
            $status = 'success';
            $message = 'Volunteer ' . $this->name . ' successfully updated.';
        } else {
            $status = 'error';
            $message = 'Something went wrong.';
        }

        $this->emit('show-notif', $status, $message);
    }

    public function download()
    {
        sleep(3);
    }
}
