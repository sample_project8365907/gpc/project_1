<?php

namespace App\Http\Livewire\Volunteers;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Volunteer;
use App\Models\VolunteerScrap;
use App\Models\ScrapCurrentBatch;
use App\Models\ExcessScrap;

class Index extends Component
{
    use WithPagination;

    public $search;
    public $load_scan_qr = false;

    public $batch_id;

    protected $listeners = [
        'refresh-volunteers' => '$refresh',
        'load-qr-scanner' => 'loadQrScanner',
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount()
    {
        $current_batch_id = ScrapCurrentBatch::first('scrap_current_batch')->scrap_current_batch;

        $this->batch_id = $current_batch_id;
    }

    public function render()
    {
        $batch_id = $this->batch_id;
        $current_location = session()->get('location_id');

        $volunteers_data = Volunteer::where('location_id', $current_location)
            ->where('status_id', 1)
            ->orderBy('encoded_at', 'desc')
            ->orderBy('created_at', 'desc');

        $total_pet = VolunteerScrap::whereIn('volunteer_id', $volunteers_data->get()->pluck('id'))
            ->where('material_type_id', 1)
            ->where('status_id', 1)
            ->where('scrap_batch_id', $batch_id)
            ->get(['weight'])
            ->pluck('weight')
            ->sum();
        $total_sf =  VolunteerScrap::whereIn('volunteer_id', $volunteers_data->get()->pluck('id'))
            ->where('material_type_id', 2)
            ->where('status_id', 1)
            ->where('scrap_batch_id', $batch_id)
            ->get(['weight'])
            ->pluck('weight')->sum();

        $total_weight = $total_pet + $total_sf;
        // $total_weight = VolunteerScrap::select('weight')->whereIn('volunteer_id', $volunteers_data->get()->pluck('id'))
        //     ->where('status_id', 1)
        //     ->where('scrap_batch_id', $this->batch_id)
        //     ->orderBy('encoded_at', 'desc')
        //     ->pluck('weight')
        //     ->sum();

        // Computation of total coupons, accumulated per volunteer and per batch
        $coupons = [];
        foreach ($volunteers_data->get() as $volunteer) {
            $store_coupon = 0;
            foreach ($volunteer->scraps()->where('scrap_batch_id', $batch_id)->where('status_id', 1)->get() as $scrap) {
                $store_coupon += $scrap->weight;
            }
            $coupons[] = floor($store_coupon / 2);
        }

        $total_coupons = array_sum($coupons);

        $volunteers_data_with_search = Volunteer::search($this->search)
            ->where('location_id', $current_location)
            ->where('status_id', 1)
            ->orderBy('encoded_at', 'desc')
            ->orderBy('created_at', 'desc');

        $volunteers = $volunteers_data_with_search->paginate(10);

        return view('livewire.volunteers.index', compact('volunteers', 'total_pet', 'total_sf', 'total_weight', 'total_coupons'));
    }

    public function loadScanQr()
    {
        $this->load_scan_qr = true;
        $this->emit('open-scan-qr-modal');
    }

    public function generateExcessWeight()
    {
        $current_location = session()->get('location_id');

        $volunteers = Volunteer::where('location_id', $current_location)
            ->where('status_id', 1)->get();

        if ($volunteers) {
            foreach ($volunteers as $volunteer) {
                // Computation of excess scrap weight
                TODO: // Need to update this code in 2nd batch, separate the total weight with material types;
                $total_weight = VolunteerScrap::where('volunteer_id', $volunteer->id)->pluck('weight')->sum();

                $total_coupons = floor($total_weight / 2);

                // Get the excess weight
                $excess_weight = $total_weight - ($total_coupons * 2);

                // Solution for wrong output when subtracting floating point to any numbers. (36.06 - 36)
                // When the decimals is greater than 10, it will use the number format function with 2 decimals.
                $decimal_count = (int) strpos(strrev($excess_weight), ".");
                if ($decimal_count > 10) {
                    $excess_weight = number_format($excess_weight, 2);
                }

                // Check if the volunteer has a record already in excess_scrap table
                $volunteer_record = ExcessScrap::where('volunteer_id', $volunteer->id)->first();

                if ($volunteer_record) {
                    ExcessScrap::where('volunteer_id', $volunteer->id)->update([
                        // Need to add material type here in 2nd batch.
                        'excess_weight' => $excess_weight,
                    ]);
                } else {
                    ExcessScrap::create([
                        'volunteer_id' => $volunteer->id,
                        'material_type_id' => 2, // This will need to change based on the material type in 2nd batch.
                        'excess_weight' => $excess_weight,
                        'scrap_current_batch' => $this->batch_id,
                        'status_id' => 1,
                    ]);
                }
            }

            // Notification
            $status = 'success';
            $message = 'Excess weight for batch no. ' . $this->batch_id . ' has been successfully generated.';
            $this->emit('show-notif', $status, $message);
        }
    }
}
