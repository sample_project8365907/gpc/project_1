<?php

namespace App\Http\Livewire\Volunteers;

use Livewire\Component;
use App\Models\Volunteer;

class ScanQr extends Component
{
    public $result;
    public $code;
    public $name;

    public $disable_button = true;

    protected $listeners = [
        'clear-fields' => 'clear',
    ];

    public function render()
    {
        return view('livewire.volunteers.scan-qr');
    }

    public function updatedResult()
    {
        $result = Volunteer::where('code', $this->result)->first();

        if ($result) {
            $code = $result->code;
            $name = $result->name;

            $this->disable_button = false;
        } else {
            $code = '';
            $name = '';

            $this->disable_button = true;
        }

        $this->code = $code;
        $this->name = $name;
    }

    public function clear()
    {
        $this->reset(['code', 'name', 'disable_button']);
    }
}
