<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\User;

class Index extends Component
{
    use WithPagination;

    protected $listeners = [
        'refresh-users' => '$refresh',
    ];

    public $search;

    public function render()
    {
        $users = User::search($this->search)->orderBy('created_at', 'desc')->paginate(10);
        
        return view('livewire.users.index', compact('users'));
    }
}
