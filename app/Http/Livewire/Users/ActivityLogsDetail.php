<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use App\Models\User;
use Spatie\Activitylog\Models\Activity;

class ActivityLogsDetail extends Component
{
    public $aid;
    public $properties;

    public $action;
    public $form_title;
    public $created_at;

    public $isActivityLogsDetailModalOpen = false;

    protected $listeners = [
        'open-activity-logs-detail-modal' => 'openActivityLogsDetailModal',
    ];

    public function render()
    {
        return view('livewire.users.activity-logs-detail');
    }

    public function openActivityLogsDetailModal($id)
    {
        $activity_log = Activity::find($id);
        $this->aid = $activity_log->id;
        $this->properties = $activity_log->properties;

        $subject = str_replace('App\Models\\', '', $activity_log->subject_type);
        $user = User::where('id', $activity_log->causer_id)->first('name')->name;

        // For custom description of Suppliers and Deliveries
        if (($subject == 'Supplier' || $subject == 'Delivery') && ($activity_log->description != 'created' && $activity_log->description != 'updated' && $activity_log->description != 'deleted')) {
            $this->form_title = $activity_log->description;
        } else {
            $this->form_title = $subject . ' ' . $activity_log->description . ' by ' . $user;
        }
        // $this->form_title = $subject . ' ' . $activity_log->description . ' by ' . $user;

        $this->created_at = date('F j, Y - h:i:s a', strtotime($activity_log->created_at));

        $this->isActivityLogsDetailModalOpen = true;
    }

    public function closeActivityLogsDetailModal()
    {
        $this->isActivityLogsDetailModalOpen = false;
    }
}
