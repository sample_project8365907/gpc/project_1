<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class Permissions extends Component
{
    public $uid;
    public $name;
    public $index = 0;

    public $old_selected_permissions = [];
    public $selected_permissions = [];

    public $isPermissionsModalOpen = false;

    protected $listeners = [
        'open-permissions-modal' => 'openPermissionsModal',
    ];

    public function mount()
    {
        $user = User::find($this->uid);

        $permission_ids = [];
        foreach ($user->permissions as $permission) {
            $permission_ids[] = $permission->id;
        }

        $this->old_selected_permissions = $permission_ids;
        $this->selected_permissions = $permission_ids;

        $this->name = $user->name;
    }

    public function render()
    {
        $dashboards = Permission::where('name', 'like', '%-dashboard%')->get();
        $users = Permission::where('name', 'like', '%-users%')->get();
        $material_types = Permission::where('name', 'like', '%-material-types%')->get();
        $locations = Permission::where('name', 'like', '%-locations%')->get();
        $truckscale_fee_vouchers = Permission::where('name', 'like', '%-truckscale-fee-vouchers%')->get();
        $database_histories = Permission::where('name', 'like', '%-database%')->get();
        $volunteers = Permission::where('name', 'like', '%-volunteers%')->get();

        return view('livewire.users.permissions', compact(
            'dashboards',
            'users',
            'material_types',
            'locations',
            'database_histories',
            'volunteers',
        ));
    }

    public function store()
    {
        $user = User::find($this->uid);

        $sync = $user->syncPermissions($this->selected_permissions);

        // Log if theres a changes.
        if ($this->selected_permissions != $this->old_selected_permissions) {
            // Log the sync to related model.
            activity()
                ->performedOn($user)
                ->causedBy(auth()->user())
                ->withProperties(['attributes' => $this->selected_permissions, 'old' => $this->old_selected_permissions])
                ->log(':causer.name updated the permission(s) of :subject.name account.');
        }

        $this->emit('refresh-sidebar');

        $status = 'success';
        $message = 'User permissions successfully updated.';
        $this->emit('show-notif', $status, $message);
    }
}
