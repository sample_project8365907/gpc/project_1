<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use Livewire\WithPagination;
use Spatie\Activitylog\Models\Activity;

class ActivityLogs extends Component
{
    use WithPagination;

    public $date_from_filter;
    public $date_to_filter;

    public function updatedDateFromFilter()
    {
        // Reset pagination when the date_from_filter is changed
        $this->resetPage();
    }

    public function updatedDateToFilter()
    {
        // Reset pagination when the date_to_filter is changed
        $this->resetPage();
    }

    public function render()
    {
        $activity_logs = Activity::when($this->date_from_filter, function ($query, $date_from_filter) {
            return $query->whereDate('created_at', '>=', $date_from_filter);
        })
            ->when($this->date_to_filter, function ($query, $date_to_filter) {
                return $query->whereDate('created_at', '<=', $date_to_filter);
            })->orderBy('created_at', 'desc')->paginate(15);

        return view('livewire.users.activity-logs', compact('activity_logs'));
    }

    public function clear()
    {
        return redirect('/maintenance/users/activity-logs');
    }
}
