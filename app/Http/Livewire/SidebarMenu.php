<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\Location;

class SidebarMenu extends Component
{
    public $location;
    public $selected = null;
    public $current_url;

    protected $listeners = [
        'refresh-sidebar' => '$refresh',
    ];

    public function mount()
    {
        $this->current_url = url()->current();

        if (
            request()->routeIs('inventories.index')
            || request()->routeIs('reimbursements.index')
            || request()->routeIs('cogs.index')
        ) {
            $this->selected = 1;
        }

        if (request()->routeIs('monitorings.index')) {
            $this->selected = 2;
        }

        if (
            request()->routeIs('beginning-inventories.index')
            || request()->routeIs('price-categories.*')
            || request()->routeIs('incentives.index')
            || request()->routeIs('cash-voucher-series.index')
            || request()->routeIs('cogs-beginning-inventories.index')
            || request()->routeIs('truckscale-fee-voucher-series.index')
            || request()->routeIs('material-types.index')
            || request()->routeIs('series.index')
            || request()->routeIs('paper-mills.index')
            || request()->routeIs('paper-mills.edit')
            || request()->routeIs('haulers.index')
            || request()->routeIs('haulers.edit')
            || request()->routeIs('trucking-rate-categories.*')
            || request()->routeIs('locations.index')
            || request()->routeIs('users.index')
            || request()->routeIs('users.permissions')
            || request()->routeIs('users.activity-logs')
        ) {
            $this->selected = 3;
        }

        if (
            request()->routeIs('suppliers.index')
            || request()->routeIs('suppliers.performance-report')
        ) {
            $this->selected = 4;
        }

        if (
            request()->routeIs('deliveries.index')
            || request()->routeIs('deliveries.summary')
        ) {
            $this->selected = 5;
        }
    }

    public function render()
    {
        // Check if location session is set
        if (session()->get('location_id')) {
            $this->location = session()->get('location_id');
        } else {
            session()->put('location_id', Auth::user()->default_location);
            $this->location = Auth::user()->default_location;
        }

        // Set location name and code
        $location = Location::where('id', $this->location)->first();
        session()->put('location', $location->name);
        session()->put('location_code', $location->code);
        session()->put('location_program', $location->program);

        // Display all assigned location for user
        $user_id = Auth::id();
        $locations = Location::whereHas('users', function ($query) use ($user_id) {
            $query->where('id', $user_id);
        })->orderBy('name', 'asc')->get();

        // Current location
        if ($location->program == 2) {
            $baling_station = session()->get('location');
        } else {
            $baling_station = session()->get('location') . ' Baling Station';
        }

        return view('livewire.sidebar-menu', compact('locations'));
    }

    public function updatedLocation()
    {
        // Update location ID when select input changed
        session()->put('location_id', $this->location);

        // Set location name and code
        $location = Location::where('id', $this->location)->first();
        session()->put('location', $location->name);
        session()->put('location_code', $location->code);
        session()->put('location_program', $location->program);

        if ($location->program == 2) {
            // Refresh and return to volunteers page.
            return redirect('/volunteers');
        } else {
            // Refresh and return to dashboard page.
            return redirect('/dashboard');
        }
    }
}
