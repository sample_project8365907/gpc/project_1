<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Volunteer;
use App\Models\VolunteerScrap;
use App\Models\ScrapCurrentBatch;
use App\Models\Location;

class Dashboard extends Component
{
    public $batch_id;

    public function mount()
    {
        $current_batch_id = ScrapCurrentBatch::first('scrap_current_batch')->scrap_current_batch;

        $this->batch_id = $current_batch_id;
    }

    public function render()
    {
        $batch_id = $this->batch_id;
        // $current_location = session()->get('location_id');

        $volunteers_data = Volunteer::where('status_id', 1)
            ->orderBy('encoded_at', 'desc')
            ->orderBy('created_at', 'desc');

        $total_pet = VolunteerScrap::whereIn('volunteer_id', $volunteers_data->get()->pluck('id'))
            ->where('material_type_id', 1)
            ->where('status_id', 1)
            ->where('scrap_batch_id', $batch_id)
            ->get(['weight'])
            ->pluck('weight')
            ->sum();
        $total_sf =  VolunteerScrap::whereIn('volunteer_id', $volunteers_data->get()->pluck('id'))
            ->where('material_type_id', 2)
            ->where('status_id', 1)
            ->where('scrap_batch_id', $batch_id)
            ->get(['weight'])
            ->pluck('weight')
            ->sum();

        $total_weight = $total_pet + $total_sf;

        // Computation of total coupons, accumulated per volunteer and per batch
        $coupons = [];
        foreach ($volunteers_data->get() as $volunteer) {
            $store_coupon = 0;
            foreach ($volunteer->scraps()->where('scrap_batch_id', $batch_id)->where('status_id', 1)->get() as $scrap) {
                $store_coupon += $scrap->weight;
            }
            $coupons[] = floor($store_coupon / 2);
        }

        $total_coupons = array_sum($coupons);

        // ----------------------------------------------------------------------------------------------------------- //

        $plants = Location::where('program', 2)->orderBy('id', 'asc')->get();
        $plant_infos = [];

        foreach ($plants as $plant) {
            $plant_id = $plant->id;
            $plant_name = $plant->name;

            $plant_infos[] = $this->loadPlants($plant_id, $plant_name);
        }

        return view('livewire.dashboard', compact('total_pet', 'total_sf', 'total_weight', 'total_coupons', 'plant_infos'));
    }

    public function loadPlants($plant_id, $plant_name)
    {
        $batch_id = $this->batch_id;

        $volunteers_data = Volunteer::where('location_id', $plant_id)
            ->where('status_id', 1)
            ->orderBy('encoded_at', 'desc')
            ->orderBy('created_at', 'desc');

        $total_pet = VolunteerScrap::whereIn('volunteer_id', $volunteers_data->get()->pluck('id'))
            ->where('material_type_id', 1)
            ->where('status_id', 1)
            ->where('scrap_batch_id', $batch_id)
            ->get(['weight'])
            ->pluck('weight')
            ->sum();
        $total_sf =  VolunteerScrap::whereIn('volunteer_id', $volunteers_data->get()->pluck('id'))
            ->where('material_type_id', 2)
            ->where('status_id', 1)
            ->where('scrap_batch_id', $batch_id)
            ->get(['weight'])
            ->pluck('weight')
            ->sum();

        $total_weight = $total_pet + $total_sf;

        // Computation of total coupons, accumulated per volunteer and per batch
        $coupons = [];
        foreach ($volunteers_data->get() as $volunteer) {
            $store_coupon = 0;
            foreach ($volunteer->scraps()->where('scrap_batch_id', $batch_id)->where('status_id', 1)->get() as $scrap) {
                $store_coupon += $scrap->weight;
            }
            $coupons[] = floor($store_coupon / 2);
        }

        $total_coupons = array_sum($coupons);

        return ['plant_name' => $plant_name, 'total_pet' => $total_pet, 'total_sf' => $total_sf, 'total_weight' => $total_weight, 'total_coupons' => $total_coupons];
    }

    public function downloadForm()
    {
        return response()->download('pdf/Volunteer\'s Registration Form Letter Size 2.pdf');
    }
}
