<?php

namespace App\Http\Livewire\Database;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.database.index');
    }
}
