<?php

namespace App\Exports;

use App\Models\DeliverySummary;
use App\Models\TruckingRate;
use App\Models\Location;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DeliveriesSummaryExport implements FromView, ShouldAutoSize
{
    private $deliveries;
    private $date_title;
    private $material_types;

    public function __construct($deliveries, $date_title, $material_type_ids, $material_type_names)
    {
        $this->deliveries = $deliveries;
        $this->date_title = $date_title;
        $this->material_type_ids = $material_type_ids;
        $this->material_type_names = $material_type_names;
    }

    public function view(): View
    {
        $deliveries = $this->deliveries;
        $date_title = $this->date_title;
        $material_type_ids = $this->material_type_ids;
        $material_type_names = $this->material_type_names;

        $deliver_to_plant_header1 = ['CUSTOMER\'S WT', 'MC', 'OT', 'PM', 'TOTAL DEDUCTION'];
        // $deliver_to_plant_header2 = array_column($material_types, 'name');
        $deliver_to_plant_header2 = $material_type_names;
        $deliver_to_plant_header3 = ['WT. DISC.'];

        $baling_station_header = ['DATE', 'ORIGIN', 'DESTINATION', 'ITEM', 'TRANSFER SLIP NO.', 'DOCUMENT NO.', 'QTY BALES', 'SUPPLIER\'S WT',];
        $deliver_to_plant_header = array_merge($deliver_to_plant_header1, $deliver_to_plant_header2, $deliver_to_plant_header3);
        $sales_header = ['SELLING PRICE', 'TOTAL RECEIVABLES',];
        $trucking_charges_header = ['TRIP TICKET', 'TRUCKING', 'PLATE NO.', 'RATE CATEGORY', 'TRUCKING RATE',];

        $header3_rows = [
            ['name' => 'BALING STATIONS', 'cols' => count($baling_station_header)],
            ['name' => 'DELIVER TO PLANT', 'cols' => count($deliver_to_plant_header)],
            ['name' => 'SALES', 'cols' => count($sales_header)],
            ['name' => 'TRUCKING CHARGES', 'cols' => count($trucking_charges_header)]
        ];
        $header4_rows = array_merge($baling_station_header, $deliver_to_plant_header, $sales_header, $trucking_charges_header);

        $headers_count = count($header4_rows);

        return view(
            'deliveries.export-excel',
            compact(
                'deliveries',
                'date_title',
                'header3_rows',
                'header4_rows',
                'headers_count',
                'material_type_ids',
                'material_type_names'
            )
        );
    }
}
