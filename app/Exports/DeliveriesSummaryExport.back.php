<?php

namespace App\Exports;

use App\Models\DeliverySummary;
use App\Models\TruckingRate;
use App\Models\Location;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class DeliveriesSummaryExport implements FromCollection, WithMapping, WithHeadings, WithStyles, WithColumnWidths, WithColumnFormatting
{
    private $deliveries;
    private $date_title;
    private $rows = 1;
    private $location_id;

    public function __construct($deliveries, $date_title)
    {
        $this->deliveries = $deliveries;
        $this->date_title = $date_title;

        $this->location_id = session()->get('location_id');
    }

    public function columnFormats(): array
    {
        return [
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'L' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'M' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'Q' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'R' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'S' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }

    public function columnWidths(): array
    {
        return [
            // FOR BALING STATIONS
            'A' => 15,
            'B' => 20,
            'C' => 35,
            'D' => 10,
            'E' => 20,
            'F' => 15,
            'G' => 15,
            'H' => 15,
            'I' => 20,
            // FOR DELIVER TO PLANT
            'J' => 15,
            'K' => 15,
            'L' => 15,
            'M' => 20,
            'N' => 15,
            'O' => 15,
            'P' => 15,
            'Q' => 15,
            // FOR SALES
            'R' => 30,
            'S' => 30,
            // FOR TRUCKING CHARGES
            'T' => 20,
            'U' => 30,
            'V' => 20,
            'W' => 35,
            'X' => 20,
        ];
    }

    public function headings(): array
    {
        $location = Location::where('id', $this->location_id)->first();

        // foreach ($material_types as $material_type) {
        //     $mats[] = $material_type->name;
        // }

        foreach ($location->materialTypes as $material_type) {
            $mats[] = $material_type->id;
        }

        // dd($mats);

        $row1 = ['BALING STATION', '', '', '', '', '', '', '', 'DELIVER TO PLANT', '', '', '', '', '', '', '', '', 'SALES', '', 'TRUCKING CHARGES'];

        $baling_station_header = ['DATE', 'ORIGIN', 'DESTINATION', 'ITEM', 'TRANSFER SLIP NO.', 'DOCUMENT NO.', 'QTY BALES', 'SUPPLIER\'S WT',];
        $deliver_to_plant_header = ['CUSTOMER\'S WT', 'MC', 'OT', 'PM', 'TOTAL DEDUCTION', 'LOCC', 'MW', 'TRIMMINGS', 'WT. DISC.',];
        $sales_header = ['SELLING PRICE', 'TOTAL RECEIVABLES',];
        $trucking_charges_header = ['TRIP TICKET', 'TRUCKING', 'PLATE NO.', 'RATE CATEGORY', 'TRUCKING RATE',];

        $row2 = array_merge($baling_station_header, $deliver_to_plant_header, $sales_header, $trucking_charges_header);

        return [$row1, $row2];
    }

    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'font' => ['bold' => true],
            'alignment' => ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
        ];

        $fontColor = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => 'ff0000']
            ],
        ];

        $header_style = [
            'font' => [
                'size' => 15,
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];

        $header_style2 = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];

        // FOR BALING STATION
        $sheet->mergeCells('A1:H1'); // Merge cell header 1
        $sheet->getStyle('A1:H1')->applyFromArray($header_style); // Style for Header 1
        $sheet->getStyle('A1:H' . ($this->deliveries->count() + 2)) // Add 2 rows in margin for headers
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN);
        $sheet->getStyle('A2:H2')->applyFromArray($header_style2); // Style for Header 2

        // FOR DELIVER TO PLANT
        $sheet->mergeCells('I1:Q1'); // Merge cell header 1
        $sheet->getStyle('I1:Q1')->applyFromArray($header_style); // Style for Header 1
        $sheet->getStyle('I1:Q' . ($this->deliveries->count() + 2)) // Add 2 rows in margin for headers
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN);
        $sheet->getStyle('I2:Q2')->applyFromArray($header_style2); // Style for Header 2

        // FOR SALES
        $sheet->mergeCells('R1:S1'); // Merge cell header 1
        $sheet->getStyle('R1:S1')->applyFromArray($header_style); // Style for Header 1
        $sheet->getStyle('R1:S' . ($this->deliveries->count() + 2)) // Add 2 rows in margin for headers
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN);
        $sheet->getStyle('R2:S2')->applyFromArray($header_style2); // Style for Header 2

        // FOR TRUCKING CHARGES
        $sheet->mergeCells('T1:X1'); // Merge cell header 1
        $sheet->getStyle('T1:X1')->applyFromArray($header_style); // Style for Header 1
        $sheet->getStyle('T1:X' . ($this->deliveries->count() + 2)) // Add 2 rows in margin for headers
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN);
        $sheet->getStyle('T2:X2')->applyFromArray($header_style2); // Style for Header 2

        // Hiding Column
        //$sheet->getColumnDimension('N')->setVisible(false);
    }

    public function map($delivery): array
    {
        // FOR BALING STATIONS AND DELIVER TO PLANT DATA
        $delivery_summary = DeliverySummary::where('delivery_id', $delivery->id)->first();

        if ($delivery_summary) {
            $summary_document_no = $delivery_summary->summary_document_no;

            $summary_net_weight = $delivery_summary->summary_net_weight;
            $summary_corrected_weight = $delivery_summary->summary_corrected_weight;
            $summary_mc = $delivery_summary->summary_mc;
            $summary_ot = $delivery_summary->summary_ot;
            $summary_pm = $delivery_summary->summary_pm;
            $summary_total_deduction = ($summary_net_weight - $summary_corrected_weight) - ($summary_mc + $summary_ot + $summary_pm);
            $summary_weight_dicrepancy = $delivery->corrected_weight - $summary_corrected_weight;
        } else {
            $summary_document_no = '';
        }

        // FOR TRUCKING CHARGES DATA
        if ($delivery->truckingRateCategory) {
            // Get the trucking rate based on the 
            // Hauler, Trucking Rate Category, Trucking Destination (Paper Mills) and the latest Effective Date
            $trucking_rate = TruckingRate::where('hauler_id', $delivery->hauler_id)
                ->where('trucking_rate_category_id', $delivery->truckingRateCategory->id)
                ->where('trucking_destination_id', $delivery->paperMill->id)
                ->where('status_id', 1)
                ->whereDate('effective_date', '<=', $delivery->encoded_at)
                ->orderBy('effective_date', 'desc')
                ->first();

            $trucking_name = $trucking_rate->destination->name;
            $trucking_rate = $trucking_rate->amount;
        } else {
            $trucking_name = '';
            $trucking_rate = 0;
        }

        $baling_station_data = [];
        $trucking_charges_data = [];

        $baling_station_data = [
            $delivery->encoded_at,
            $delivery->location->name,
            $delivery->paperMill->name,
            $delivery->materialType->name,
            $delivery->transfer_slip_no,
            $summary_document_no,
            $delivery->quantity_bales,
            $delivery->corrected_weight,
        ];

        $deliver_to_plant_data = [
            ($delivery_summary) ? $summary_corrected_weight : 0,
            ($delivery_summary) ? $summary_mc : 0,
            ($delivery_summary) ? $summary_ot : 0,
            ($delivery_summary) ? $summary_pm : 0,
            ($delivery_summary) ? $summary_total_deduction : 0,
            0, // LOCC
            0, // MW
            0, // TRIMMINGS
            ($delivery_summary) ? $summary_weight_dicrepancy : 0,
        ];

        $sales_data = [
            0, // Selling Price
            0, // Total Receiving
        ];

        $trucking_charges_data = [
            $delivery->trip_ticket,
            $delivery->hauler->name,
            $delivery->vehicle->plate_no,
            $trucking_name,
            $trucking_rate,
        ];

        $total = [
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '', // Total of Suppliers Wt.
            '', // Total of Customers Wt.
            '',
            '',
            '',
            '', // Total Deduction
            '', // LOCC
            '', // MW
            '', // TRIMMIMGS
            '', // Wt. Disc.
            '',
        ];

        return array_merge($baling_station_data, $deliver_to_plant_data, $sales_data, $trucking_charges_data);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->deliveries;
    }
}
