<?php

namespace App\Exports;

use App\Models\Voucher;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class VouchersExport implements FromCollection, WithMapping, WithHeadings, WithStyles, WithColumnWidths
{
    private $vouchers;
    private $rows = 1;
    private $total_summary_rows = 1;

    private $date_title;
    private $total_summary_locc;
    private $total_summary_mw;
    private $total_quantity;
    private $total_amount;

    public function __construct($vouchers, $date_title, $total_summary_locc, $total_summary_mw, $total_quantity, $total_amount)
    {
        $this->vouchers = $vouchers;
        $this->date_title = $date_title;
        $this->total_summary_locc = $total_summary_locc;
        $this->total_summary_mw = $total_summary_mw;
        $this->total_quantity = $total_quantity;
        $this->total_amount = $total_amount;
    }

    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'font' => ['bold' => true],
            'alignment' => ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
        ];

        $fontColor = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => 'ff0000']
            ],
        ];

        $header_style = [
            'font' => [
                'size' => 15,
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];

        $header_style2 = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];

        $sheet->mergeCells('A1:M1');
        $sheet->mergeCells('A2:M2');

        $sheet->getStyle('A1:M1')->applyFromArray($header_style);
        $sheet->getStyle('A2:M2')->applyFromArray($header_style);
        $sheet->getStyle('A3:M3')->applyFromArray($header_style2);

        $sheet->getStyle('A1:M' . ($this->rows + 3)) // Add 2 rows for headers
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN);

        $sheet->mergeCells('B' . ($this->rows + 7) . ':F' . ($this->rows + 7));
        $sheet->getStyle('B' . ($this->rows + 7) . ':F' . ($this->rows + 8))->applyFromArray($header_style2);
        $sheet->getStyle('B' . ($this->total_summary_rows + $this->rows + 8) . ':F' . ($this->total_summary_rows + $this->rows + 8))->applyFromArray($header_style2);

        $sheet->getStyle('B' . ($this->rows + 7) . ':F' . ($this->total_summary_rows + $this->rows + 8)) // Add 2 rows for headers
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN);

        // Add 2 rows for header and 1 row for summary row
        $last_row = $this->rows + 3;

        return [
            // Style for summary row
            'A' . $last_row . ':M' . $last_row => ['font' => ['bold' => true, 'color' => ['argb' => 'ff0000']]],
        ];
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'E' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'F' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_NUMBER,
            'I' => NumberFormat::FORMAT_NUMBER,
            'J' => NumberFormat::FORMAT_NUMBER,
            'K' => NumberFormat::FORMAT_NUMBER,
            'L' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'M' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 10,
            'B' => 10,
            'C' => 10,
            'D' => 15,
            'E' => 25,
            'F' => 15,
            'G' => 15,
            'H' => 10,
            'I' => 15,
            'J' => 15,
            'K' => 20,
            'L' => 15,
            'M' => 15,
        ];
    }

    public function headings(): array
    {
        $row1 = ['GREENKRAFT CORPORATION | ' . mb_strtoupper(session()->get('location')) . ' BALING STATION'];
        $row2 = ['DAILY RECEIVING | ' . $this->date_title];
        $row3 = [
            'DATE',
            'CV No.',
            'WS No.',
            'SUPPLIER',
            'MATERIAL TYPE',
            'GROSS WEIGHT',
            'TARE WEIGHT',
            'MC',
            'NET WEIGHT',
            'DEDUCTION',
            'CORRECTED WEIGHT',
            'UNIT PRICE',
            'AMOUNT',
        ];

        return [$row1, $row2, $row3];
    }

    public function prepareRows($vouchers)
    {
        $total_gross_weight = 0;
        $total_tare_weight = 0;
        $total_net_weight = 0;
        $total_deduction = 0;
        $total_corrected_weight = 0;
        $total_amount = 0;

        foreach ($vouchers as $voucher) {
            if ($voucher->status_id == 1) {
                $total_gross_weight += $voucher->gross_weight;
                $total_tare_weight += $voucher->tare_weight;
                $total_net_weight += $voucher->net_weight;
                $total_deduction += $voucher->deducted_weight;
                $total_corrected_weight += $voucher->corrected_weight;
                $total_amount += $voucher->amount;
            }

            $this->rows++;
        }

        // Append summary row
        $vouchers[] = [
            'is_summary' => true,
            'total_gross_weight' => $total_gross_weight,
            'total_tare_weight' => $total_tare_weight,
            'total_net_weight' => $total_net_weight,
            'total_deduction' => $total_deduction,
            'total_corrected_weight' => $total_corrected_weight,
            'total_amount' => $total_amount,
        ];

        return $vouchers;
    }

    public function map($voucher): array
    {
        // Differentiate Normal data row from prepareRows and Total Row (last row)
        if (isset($voucher['is_summary']) && $voucher['is_summary'] === true) {
            $total[] = [
                null,
                null,
                null,
                null,
                null,
                number_format($voucher['total_gross_weight']),
                number_format($voucher['total_tare_weight']),
                null,
                number_format($voucher['total_net_weight']),
                number_format($voucher['total_deduction']),
                number_format($voucher['total_corrected_weight']),
                null,
                $voucher['total_amount'],
            ];

            // Spaces
            array_push($total, [null, null, null, null, null, null], [null, null, null, null, null, null], [null, null, null, null, null, null]);

            // 1st Layer Header for Total Summary
            array_push($total, [null, 'TOTAL SUMMARY']);

            // 2nd Layer Header for Total Summary
            array_push($total, [null, 'ITEM', 'QUANTITY', 'UNIT PRICE', 'INCENTIVES', 'AMOUNT']);

            // Total Summary for LOCC
            foreach ($this->total_summary_locc as $summary_locc) {
                $this->total_summary_rows++;
                array_push(
                    $total,
                    [
                        null, 'LOCC',
                        number_format($summary_locc['quantity_locc']),
                        $summary_locc['unit_price_locc'],
                        $summary_locc['incentives_locc'],
                        number_format($summary_locc['amount_locc'], 2)
                    ]
                );
            }

            // Total Summary for MW
            foreach ($this->total_summary_mw as $summary_mw) {
                $this->total_summary_rows++;
                array_push(
                    $total,
                    [
                        null, 'MW',
                        number_format($summary_mw['quantity_mw']),
                        $summary_mw['unit_price_mw'],
                        $summary_mw['incentives_mw'],
                        number_format($summary_mw['amount_mw'], 2)
                    ]
                );
            }

            // Total for Quantity and Amount
            array_push($total, [null, null, number_format($this->total_quantity), null, null, number_format($this->total_amount, 2)]);

            return $total;
        } else {
            // Removed data when status is cancelled
            if ($voucher->status_id == 1) {
                $data[] = [
                    date('M-d-Y', strtotime($voucher->encoded_at)),
                    $voucher->gcv_no,
                    $voucher->ws_no,
                    $voucher->supplier_name,
                    $voucher->materialType->name,
                    number_format($voucher->gross_weight),
                    number_format($voucher->tare_weight),
                    $voucher->mc . '%',
                    number_format($voucher->net_weight),
                    $voucher->deducted_weight,
                    number_format($voucher->corrected_weight),
                    $voucher->unit_price,
                    $voucher->amount,
                ];
            } else {
                $data[] = [
                    date('M-d-Y', strtotime($voucher->encoded_at)),
                    $voucher->gcv_no,
                    $voucher->ws_no,
                    $voucher->supplier_name,
                    $voucher->material_type,
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                ];
            }
        }

        return $data;
    }

    public function collection()
    {
        return $this->vouchers;
    }
}
