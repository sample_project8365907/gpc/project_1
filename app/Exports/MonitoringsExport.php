<?php

namespace App\Exports;

use App\Models\Voucher;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class MonitoringsExport implements FromCollection, WithMapping, WithHeadings, WithStyles, WithColumnWidths, WithColumnFormatting
{
    private $accounting_reports;
    private $rows = 1;

    public function __construct($accounting_reports)
    {
        $this->accounting_reports = $accounting_reports;
    }

    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'font' => ['bold' => true],
            'alignment' => ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
        ];

        $fontColor = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => 'ff0000']
            ],
        ];

        $header_style = [
            'font' => [
                'size' => 15,
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];

        $header_style2 = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];

        $sheet->mergeCells('A1:Q1');
        $sheet->mergeCells('A2:Q2');

        $sheet->getStyle('A1:Q1')->applyFromArray($header_style);
        $sheet->getStyle('A2:Q2')->applyFromArray($header_style);
        $sheet->getStyle('A3:Q3')->applyFromArray($header_style2);

        $sheet->getStyle('A1:Q'.($this->rows+2)) // Plus two rows for headers
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(Border::BORDER_THIN);
    }

    public function columnFormats(): array
    {
        return [
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'N' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'O' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'Q' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }

    public function columnWidths(): array
    {

        return [
            'A' => 10,
            'B' => 10,
            'C' => 15,
            'D' => 25,
            'E' => 15,
            'F' => 15,
            'G' => 15,
            'H' => 15,
            'I' => 10,
            'J' => 10,
            'K' => 10,
            'L' => 5,
            'M' => 5,
            'N' => 15,
            'O' => 20,
            'P' => 15,
            'Q' => 10,
        ];
    }

    public function headings(): array
    {   
        $row1 = ['GREENKRAFT CORPORATION | '.strtoupper(session()->get('location')).' BALING STATION'];
        $row2 = ['ACCOUNTING REPORTS MONITORING | '.now()->format('F j, Y')];
        $row3 = [
            'DATE',
            'CV No.',
            'SUPPLIER',
            'DRIVER\'S NAME',
            'PLATE NUMBER',
            'MATERIAL TYPE',
            'TRUCK TYPE',
            'LOCATION',
            'GROSS',
            'TARE',
            'NET',
            'MC',
            'OT',
            'DEDUCTION',
            'CORRECTED WEIGHT',
            'UNIT PRICE',
            'TOTAL',
        ];

        return [$row1, $row2, $row3];
    }

    public function map($accounting_report): array
    {   
        $this->rows++;

        $data = [
            $accounting_report->created_at->format('M-d-y'),
            $accounting_report->gcv_no,
            $accounting_report->supplier_name,
            $accounting_report->driver_name,
            $accounting_report->plate_no,
            $accounting_report->materialType->name,
            $accounting_report->truck_type,
            $accounting_report->location->name,
            $accounting_report->gross_weight,
            $accounting_report->tare_weight,
            $accounting_report->net_weight,
            $accounting_report->mc,
            $accounting_report->ot,
            $accounting_report->deducted_weight,
            $accounting_report->corrected_weight,
            $accounting_report->unit_price,
            $accounting_report->amount,
        ];

        return $data;
    }

    public function collection()
    {
        return $this->accounting_reports;
    }
}
