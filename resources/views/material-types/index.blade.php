<x-app-layout>
    <x-slot name="header">
        {{-- Breadcrumbs --}}
        <div class="flex justify-start items-center text-gray-500 font-bold">
            <a href="{{ route('dashboard') }}"><x-icons.solid.home class="h-6 w-6"></x-icons.solid.home></a>
            <x-icons.outline.chevron-right class="h-5 w-5"></x-icons.outline.chevron-right>
            <a href="#" class="mx-2">Maintenance</a>
            <x-icons.outline.chevron-right class="h-5 w-5"></x-icons.outline.chevron-right>
            <div class="text-green-600 mx-2">Material Types</div>
        </div>
    </x-slot>

    <div class="mx-auto py-6 sm:px-6">
        @livewire('notification')
        <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
            <div class="col-span-12 sm:col-span-8 order-last sm:order-first">
                @can('edit-material-types')
                    @livewire('material-types.edit')
                @endcan
                @can('delete-material-types')
                    @livewire('material-types.delete')
                @endcan
                @livewire('material-types.index')
            </div>
            @can('create-material-types')
            <div class="col-span-12 sm:col-span-4">
                @livewire('material-types.create')
            </div>
            @endcan
        </div>
    </div>

</x-app-layout>