<x-app-layout>
    <x-slot name="header">
        {{-- Breadcrumbs --}}
        <div class="flex justify-start items-center text-gray-500 font-bold">
            <a href="{{ route('volunteers.index') }}" class="text-green-600 mx-2">Volunteers</a>
            <x-icons.outline.chevron-right class="h-5 w-5"></x-icons.outline.chevron-right>
            <a href="#" class="mx-2">{{ $volunteer_code }}</a>
        </div>
    </x-slot>
    
    @livewire('notification')
    <div class="mx-auto py-8 sm:px-6 lg:px-8 pb-10">
        @livewire('volunteers.edit', ['vid' => $vid])

        {{-- <div class="hidden sm:block" aria-hidden="true">
            <div class="py-5">
                <div class="border-t border-gray-200"></div>
            </div>
        </div> --}}

        @livewire('volunteer-scraps.create-edit', ['vid' => $vid])
        @livewire('volunteer-scraps.delete')
        @livewire('volunteer-scraps.index', ['vid' => $vid, 'volunteer_code' => $volunteer_code])
    </div>
</x-app-layout>