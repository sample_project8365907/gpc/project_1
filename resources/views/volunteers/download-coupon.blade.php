<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Download Coupons</title>

    {{-- Styles --}}
    {{-- <link rel="stylesheet" href="{{ public_path('css/table.css') }}" type="text/css" media="all"> --}}

    <style type="text/css" media="all">
        @font-face {
            font-family: 'Nunito-400';
            src: url({{ public_path('fonts/Nunito-Regular.ttf') }});
        }

        @font-face {
            font-family: 'Nunito-500';
            src: url({{ public_path('fonts/Nunito-Medium.ttf') }});
        }

        @font-face {
            font-family: 'Nunito-700';
            src: url({{ public_path('fonts/Nunito-Bold.ttf') }});
        }

        .font-medium {
            font-family: 'Nunito-500', sans-serif;
        }

        .font-bold {
            font-family: 'Nunito-700', sans-serif;
        }
    </style>
</head>
<body>
    <div style="display: flex; flex-direction: row; flex-wrap: wrap;">
        <table>
            @foreach($blocks as $block)
            <tr>
                @foreach($block as $record)
                    <td style="padding: 10px; text-align: center;">
                        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(150)->generate($record['volunteer_name'].' - '.$record['volunteer_code'])) !!} ">
                        {{-- C1 - the 1 in C1 is the batch_id --}}
                        <div class="text-center font-bold" style="font-size: 14px;">{{ $record['volunteer_code'] }}C{{ $record['batch_id'] }}{{ $record['coupon_number'] }}</div>
                    </td>
                @endforeach
            </tr>
            @endforeach
        </table>
    </div>
</body>
</html>