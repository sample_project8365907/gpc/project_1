<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-start items-center text-gray-500 font-bold">
            <a href="{{ route('volunteers.index') }}" class="text-green-600 mx-2">Volunteers</a>
            <x-icons.outline.chevron-right class="h-5 w-5"></x-icons.outline.chevron-right>
            <a href="#" class="mx-2">QR Scanner</a>
        </div>
    </x-slot>

    @livewire('notification')
    @livewire('volunteers.add-scrap')
    @livewire('volunteers.scan-qr')

</x-app-layout>