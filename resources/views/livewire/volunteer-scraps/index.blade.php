<div>
    <div class="mx-auto py-8 pb-10">
        <div class="bg-white overflow-hidden shadow-sm">
            <div class="flex justify-between items-center px-6 py-4 border-b font-bold">
                <div class="flex justify-start">
                    Scraps
                </div>
            </div>
            <div class="bg-white overflow-hidden shadow-sm">
                <div class="flex justify-between items-center px-6 py-4 border-b font-bold">
                    {{-- <div class="flex justify-start pr-4">
                        <x-input id="search" class="block mt-1 w-full" type="text" :name="__('search')" placeholder="Search" wire:model="search"/>
                    </div> --}}
                    <div>
                        <x-label :for="__('batch_id')" :value="__('Batch')" :errors="$errors"/>
                        <div class="relative">
                            <x-select wire:model="batch_id" wire:loading.attr="disabled" wire:target="store" id="batch_id" :name="__('batch_id')" class="block mt-1 min w-full" :errors="$errors" required>
                                {{-- <option value="" selected>All</option> --}}
                                @foreach($batches as $batch)
                                <option value="{{ $batch->id }}">{{ $batch->name }}</option>
                                @endforeach
                            </x-select>
                            <x-input-error for="batch_id"/>
                        </div>
                    </div>
                    <div>
                        <x-button-link href="{!! route('volunteer-scraps.print-coupon', ['id' => $vid, 'total_coupons' => $total_coupons, 'volunteer_code' => $volunteer_code]) !!}" target="_blank" class="mr-4">
                            Print Coupon
                        </x-button-link>

                        <x-button wire:click="$emitTo('volunteer-scraps.create-edit', 'open-create-modal')">
                            Add Scrap
                        </x-button>
                    </div>
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="mb-4">
                        <x-table>
                            <x-slot name="head">
                                <x-table-heading>
                                    PET WEIGHT
                                </x-table-heading>
                                <x-table-heading>
                                    SOFT PLASTIC WEIGHT
                                </x-table-heading>
                                <x-table-heading>
                                    TOTAL WEIGHT
                                </x-table-heading>
                                <x-table-heading>
                                    TOTAL COUPON(S)
                                </x-table-heading>
                            </x-slot>
                            <x-slot name="body">
                                <x-table-row>
                                    <x-table-cell>
                                        <div class="cursor-pointer">
                                            <div class="font-bold text-xl text-red-500">
                                                {{ $total_pet }}
                                            </div>
                                        </div>
                                    </x-table-cell>
                                    <x-table-cell>
                                        <div class="cursor-pointer">
                                            <div class="font-bold text-xl text-red-500">
                                                {{ $total_sf }}
                                            </div>
                                        </div>
                                    </x-table-cell>
                                    <x-table-cell>
                                        <div class="cursor-pointer">
                                            <div class="font-bold text-xl text-red-500">
                                                {{ $total_weight }}
                                            </div>
                                        </div>
                                    </x-table-cell>
                                    <x-table-cell>
                                        <div class="cursor-pointer">
                                            <div class="font-bold text-xl text-red-500">
                                                {{ $total_coupons }}
                                            </div>
                                        </div>
                                    </x-table-cell>
                                </x-table-row>
                            </x-slot>
                        </x-table>
                    </div>
                    <x-table>
                        <x-slot name="head">
                            <x-table-heading>
                                DATE
                            </x-table-heading>
                            <x-table-heading>
                                MATERIAL TYPE
                            </x-table-heading>
                            <x-table-heading>
                                WEIGHT (KG)
                            </x-table-heading>
                            <x-table-heading>
                                ACTION
                            </x-table-heading>
                        </x-slot>
                        <x-slot name="body">
                            @forelse($volunteer_scraps as $volunteer_scrap)
                            <x-table-row>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ date('F j, Y', strtotime($volunteer_scrap->encoded_at)) }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $volunteer_scrap->materialType->name }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $volunteer_scrap->weight }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <a wire:click="$emitTo('volunteer-scraps.create-edit', 'open-edit-modal', {{ $volunteer_scrap->id }})" 
                                        class="h-5 w-5 text-green-600 hover:text-green-500 font-bold cursor-pointer">
                                        Edit
                                    </a>
                                    -
                                    <a wire:click="$emitTo('volunteer-scraps.delete', 'open-delete-modal', {{ $volunteer_scrap->id }})" 
                                        class="text-green-600 hover:text-green-500 font-bold cursor-pointer">
                                        Delete
                                    </a>
                                </x-table-cell>
                            </x-table-row>
                            @empty
                            <x-table-row>
                                <x-table-cell class="text-center" colspan="12">
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            No Record Found.
                                        </div>
                                    </div>
                                </x-table-cell>
                            </x-table-row>
                            @endforelse
                        </x-slot>
                    </x-table>
                    <div class="pt-3">
                        {{ $volunteer_scraps->withQueryString()->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>