<div>
    <div class="mx-auto py-6 sm:px-6">
        <div class="bg-white overflow-hidden shadow-sm">
            <div class="flex justify-between items-center p-6 border-b font-bold">
                <div class="flex justify-start">
                    <div class="text-gray-500">{{ $name }}</div>
                </div>
            </div>
            <div class="p-6 bg-white border-b border-gray-200">
                <form wire:submit.prevent="store">
                    <div class="columns-1 md:columns-2 2xl:columns-4 gap-8 [column-fill:_balance] box-border mx-auto before:box-inherit after:box-inherit">
                        <div class="break-inside-avoid mb-6 rounded-lg">
                            <legend class="sr-only">Dashboard</legend>
                            <div class="text-base font-bold text-gray-900" aria-hidden="true">Dashboard</div>
                            <div class="mt-4 space-y-4 border-2 p-3 rounded">
                                @foreach($dashboards as $dashboard)
                                @php $index++ @endphp
                                <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                        <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $dashboard->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                                wire:model="selected_permissions"
                                                wire:loading.attr="disabled" 
                                                wire:target="store"
                                        />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $dashboard->name }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="break-inside-avoid mb-6 rounded-lg">
                            <legend class="sr-only">Users</legend>
                            <div class="text-base font-bold text-gray-900" aria-hidden="true">Users</div>
                            <div class="mt-4 space-y-4 border-2 p-3 rounded">
                                @foreach($users as $user)
                                @php $index++ @endphp
                                <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                        <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $user->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                                wire:model="selected_permissions"
                                                wire:loading.attr="disabled" 
                                                wire:target="store"
                                        />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $user->name }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="break-inside-avoid mb-6 rounded-lg">
                            <legend class="sr-only">Material Types</legend>
                            <div class="text-base font-bold text-gray-900" aria-hidden="true">Material Types</div>
                            <div class="mt-4 space-y-4 border-2 p-3 rounded">
                                @foreach($material_types as $material_type)
                                @php $index++ @endphp
                                <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                        <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $material_type->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                                wire:model="selected_permissions"
                                                wire:loading.attr="disabled" 
                                                wire:target="store"
                                        />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $material_type->name }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="break-inside-avoid mb-6 rounded-lg">
                            <legend class="sr-only">Locations</legend>
                            <div class="text-base font-bold text-gray-900" aria-hidden="true">Locations</div>
                            <div class="mt-4 space-y-4 border-2 p-3 rounded">
                                @foreach($locations as $location)
                                @php $index++ @endphp
                                <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                        <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $location->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                                wire:model="selected_permissions"
                                                wire:loading.attr="disabled" 
                                                wire:target="store"
                                        />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $location->name }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="break-inside-avoid mb-6 rounded-lg">
                            <legend class="sr-only">Volunteers</legend>
                            <div class="text-base font-bold text-gray-900" aria-hidden="true">Volunteers</div>
                            <div class="mt-4 space-y-4 border-2 p-3 rounded">
                                @foreach($volunteers as $volunteer)
                                @php $index++ @endphp
                                <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                        <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $volunteer->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                                wire:model="selected_permissions"
                                                wire:loading.attr="disabled" 
                                                wire:target="store"
                                        />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $volunteer->name }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        {{-- <div class="break-inside-avoid mb-6 rounded-lg">
                            <legend class="sr-only">Database</legend>
                            <div class="text-base font-bold text-gray-900" aria-hidden="true">Database</div>
                            <div class="mt-4 space-y-4 border-2 p-3 rounded">
                                @foreach($database_histories as $database_history)
                                @php $index++ @endphp
                                <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                        <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $database_history->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                                wire:model="selected_permissions"
                                                wire:loading.attr="disabled" 
                                                wire:target="store"
                                        />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $database_history->name }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div> --}}
                    </div>
                    <div class="flex justify-end pt-8">
                        <x-button class="my-1" wire:loading.attr="disabled" wire:target="store">
                            <div wire:loading wire:target="store">
                                <x-icons.loading class="w-4 mr-2"/>
                            </div>
                            <x-icons.solid.save wire:loading.remove wire:target="store" class="h-4 w-4 mr-2"/>
                            {{ __('Save') }}
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>