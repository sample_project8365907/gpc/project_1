<div>
    <x-form-modal maxWidth="xl" wire:model.defer="isActivityLogsDetailModalOpen" submit="{{ $action }}">
        <x-slot name="title">
            <div class="flex items-center justify-between">
                <div>
                    {{ $form_title }}
                    <div class="text-gray-400 text-xs ">
                        {{$created_at}}
                    </div>
                </div>
                <button wire:click="closeActivityLogsDetailModal()" type="button" class="text-gray-400 bg-transparent hover:text-green-700 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:text-green-700" data-modal-hide="defaultModal">
                    <x-icons.outline.x class="h-5 w-5"/>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
        </x-slot>
        <x-slot name="description"></x-slot>
        <x-slot name="content">
            <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                <div class="col-span-12 break-words">
                    <pre>{{ json_encode($properties, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) }}</pre>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-button type="button" wire:click="closeActivityLogsDetailModal()" wire:loading.attr="disabled" wire:target="store, update" class="ml-3 my-1">
                <x-icons.solid.x class="h-4 w-4 mr-2"/>
                {{ __('Close') }}
            </x-button>
        </x-slot>
    </x-form-modal>
</div>
