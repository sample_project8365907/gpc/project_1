<div>
    <div class="py-6 mx-auto sm:px-6">
        <div class="overflow-hidden bg-white shadow-sm">
            <div class="items-center justify-between px-6 py-4 font-bold border-b xl:flex">
                <div class="flex justify-start hidden gap-4 xl:block">
                    <x-input id="search" class="w-full" type="text" :name="__('search')" placeholder="Search" wire:model="search"/>
                </div>
                <div class="flex items-center gap-4 ml-auto">
                    <x-button-link href="{{ route('users.activity-logs') }}" class="mr-4" class="w-full xl:w-auto">
                        <x-icons.solid.document-arrow-down class="w-4 h-4 mr-2"/>
                        Activity Log
                    </x-button-link>
                    <x-button wire:click="$emitTo('users.create-edit', 'open-create-modal')" class="w-full xl:w-auto">
                        <x-icons.solid.user-plus class="w-4 h-4 mr-2"/>
                        Add User
                    </x-button>
                </div>
            </div>
            <div class="p-6 bg-white border-b border-gray-200">
                <div class="block xl:hidden">
                    <x-input id="search" class="block w-full mt-1 mb-6" type="text" :name="__('search')" placeholder="Search" wire:model="search"/>
                </div>
                <x-table>
                    <x-slot name="head">
                        <x-table-heading>
                            NAME
                        </x-table-heading>
                        <x-table-heading>
                            USERNAME
                        </x-table-heading>
                        <x-table-heading>
                            DEFAULT LOCATION
                        </x-table-heading>
                        <x-table-heading>
                            LOCATION(S)
                        </x-table-heading>
                        <x-table-heading>
                            STATUS
                        </x-table-heading>
                        <x-table-heading>
                            ACTIONS
                        </x-table-heading>
                    </x-slot>
                    <x-slot name="body">
                        @forelse($users as $user)
                        <x-table-row>
                            <x-table-cell>
                                <div class="cursor-pointer">
                                    <div class="font-medium text-gray-900">
                                        {{ $user->name }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                <div class="cursor-pointer">
                                    <div class="font-medium text-gray-900">
                                        {{ $user->username }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                <div class="cursor-pointer">
                                    <div class="font-medium text-gray-900">
                                        {{ $user->default->name }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                <div class="cursor-pointer">
                                    <div class="font-medium text-gray-900">
                                        {{ $user->locations->pluck('name')->implode(', ') }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                <div class="cursor-pointer">
                                    <div class="font-medium text-center text-white py-1 px-3 rounded-full 
                                        @if($user->status_id == 1) bg-green-400 @else bg-red-500 @endif"
                                    >
                                        {{ $user->status->name }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                <div class="flex items-center justify-center gap-3 cursor-pointer">
                                    <a wire:click="$emitTo('users.create-edit', 'open-edit-modal', {{ $user->id }})" class="font-bold text-green-600 cursor-pointer hover:text-green-500">
                                        Edit
                                    </a>
                                    <div>
                                        |
                                    </div>
                                    <a href="{{ route('users.permissions', $user->id) }}" class="font-bold text-green-600 cursor-pointer hover:text-green-500">
                                        Pemissions
                                    </a>
                                    <div>
                                        |
                                    </div>
                                    <a wire:click="$emitTo('users.change-password', 'open-change-password-modal', {{ $user->id }})" class="font-bold text-green-600 cursor-pointer hover:text-green-500">
                                        Change Pass
                                    </a>
                                </div>
                            </x-table-cell>
                        </x-table-row>
                        @empty
                        <x-table-row>
                            <x-table-cell class="text-center" colspan="12">
                                <div class="cursor-pointer">
                                    <div class="font-medium text-gray-900">
                                        No Record Found.
                                    </div>
                                </div>
                            </x-table-cell>
                        </x-table-row>
                        @endforelse
                    </x-slot>
                </x-table>
                <div class="pt-3">
                    {{ $users->withQueryString()->links() }}
                </div>
            </div>
        </div>
    </div>
</div>