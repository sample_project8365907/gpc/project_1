<div>
    <x-form-modal maxWidth="7xl" wire:model.defer="isPermissionsModalOpen" submit="store">
        <x-slot name="title">
            User Permissions
        </x-slot>
        <x-slot name="description"></x-slot>
        <x-slot name="content">
            <div class="">
                <div class="columns-1 sm:columns-3 gap-6 overscroll-y-contain">
                    <div class="mb-6">
                        <legend class="sr-only">Dashboard</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Dashboard</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($dashboards as $dashboard)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $dashboard->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $dashboard->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Users</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Users</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($users as $user)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $user->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $user->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Vouchers</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Vouchers</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($vouchers as $voucher)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $voucher->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $voucher->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Productions</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Productions</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($productions as $production)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $production->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $production->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Deliveries</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Deliveries</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($deliveries as $deliveries)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $deliveries->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $deliveries->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Reports</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Reports</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($reports as $report)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $report->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $report->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Accounting Reports</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Accounting Reports</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($accounting_reports as $accounting_report)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $accounting_report->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $accounting_report->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Beginning Inventory</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Beginning Inventory</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($beginning_inventories as $beginning_inventory)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $beginning_inventory->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $beginning_inventory->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Price Categories</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Price Categories</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($price_categories as $price_category)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $price_category->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $price_category->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Incentives</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Incentives</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($incentives as $incentive)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $incentive->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $incentive->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Cash Vouchers</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Cash Vouchers</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($cash_vouchers as $cash_voucher)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $cash_voucher->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $cash_voucher->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">TS/TT Series</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">TS/TT Series</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($series as $series)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $series->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $series->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-6">
                        <legend class="sr-only">Paper Mills</legend>
                        <div class="text-base font-bold text-gray-900" aria-hidden="true">Paper Mills</div>
                        <div class="mt-4 space-y-4 border-2 p-3 rounded">
                            @foreach($paper_mills as $paper_mill)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_permissions[{{ $index }}]" id="selected_permissions[{{ $index }}]" type="checkbox" value="{{ $paper_mill->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model="selected_permissions"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_permissions[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $paper_mill->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-button type="button" wire:click="closePermissionsModal()" wire:loading.attr="disabled" wire:target="store, update" class="ml-3 my-1">
                <x-icons.solid.x class="h-4 w-4 mr-2"/>
                {{ __('Cancel') }}
            </x-button>
            <x-button class="ml-3 my-1" wire:loading.attr="disabled" wire:target="store, update">
                <div wire:loading wire:target="store, update">
                    <x-icons.loading class="w-4 mr-2"/>
                </div>
                <x-icons.solid.save wire:loading.remove wire:target="store, update" class="h-4 w-4 mr-2"/>
                {{ __('Save') }}
            </x-button>
        </x-slot>
    </x-form-modal>
</div>
