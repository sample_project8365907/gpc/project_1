<div>
    {{-- A good traveler has no fixed plans and is not intent upon arriving. --}}
    <div class="mx-auto py-6 sm:px-6">
        <div class="bg-white overflow-hidden shadow-sm">
            <div class="flex justify-between items-center p-6 border-b font-bold">
                <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                    <div class="col-span-12 sm:col-span-2">
                        <div class="relative">
                            <x-label :for="__('date_from_filter')" :value="__('Date from')" />
                            <div class="relative">
                                <x-input-date-default wire:model="date_from_filter" wire:loading.attr="disabled" id="date_from_filter" class="block mt-1 w-full" type="text" :name="__('date_from_filter')" :value="old('date_from_filter')" :errors="$errors" required/>
                                <x-input-error for="date_from_filter"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-2">
                        <div class="relative">
                            <x-label :for="__('date_to_filter')" :value="__('Date to')" />
                            <div class="relative">
                                <x-input-date-default wire:model="date_to_filter" wire:loading.attr="disabled" id="date_to_filter" class="block mt-1 w-full" type="text" :name="__('date_to_filter')" :value="old('date_to_filter')" :errors="$errors" required/>
                                <x-input-error for="date_to_filter"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-1">
                        <x-button wire:click="clear" wire:loading.attr="disabled" wire:target="clear" class="mt-2 sm:mt-6 w-full">
                            <div wire:loading wire:target="clear">
                                <x-icons.loading class="w-4 mr-2"/>
                            </div>
                            <x-icons.solid.refresh wire:loading.remove wire:target="clear" class="h-4 w-4 mr-2"/>
                            Clear
                        </x-button>
                    </div>
                </div>
            </div>
            <div class="p-6 bg-white border-b border-gray-200">
                <x-table>
                    <x-slot name="head">
                        <x-table-heading>
                            LOG NAME
                        </x-table-heading>
                        <x-table-heading>
                            DESCRIPTION
                        </x-table-heading>
                        <x-table-heading>
                            SUBJECT
                        </x-table-heading>
                        <x-table-heading>
                            USER
                        </x-table-heading>
                        <x-table-heading>
                            DATE
                        </x-table-heading>
                        <x-table-heading>
                            ACTION
                        </x-table-heading>
                    </x-slot>
                    <x-slot name="body">
                        @forelse($activity_logs as $activity_log)
                        <x-table-row>
                            <x-table-cell>
                                <div>
                                    <div class="font-medium text-gray-900">
                                        {{ mb_strtoupper($activity_log->log_name) }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                <div>
                                    <div class="font-medium text-gray-900">
                                        {{ mb_strtoupper($activity_log->description) }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                <div>
                                    <div class="font-medium text-gray-900">
                                        {{ str_replace('APP\MODELS\\','',mb_strtoupper($activity_log->subject_type)) }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                <div>
                                    <div class="font-medium text-gray-900">
                                        {{ Auth::user()->where('id', $activity_log->causer_id)->first('name')->name }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                <div>
                                    <div class="font-medium text-gray-900">
                                        {{ date('F j, Y - h:i:s a', strtotime($activity_log->created_at)) }}
                                    </div>
                                </div>
                            </x-table-cell>
                            <x-table-cell>
                                @if($activity_log->description != 'logged in' && $activity_log->description != 'logged out')
                                <div class="flex justify-center items-center cursor-pointer gap-3">
                                    <a wire:click="$emitTo('users.activity-logs-detail', 'open-activity-logs-detail-modal', {{ $activity_log->id }})" class="text-green-600 hover:text-green-500 font-bold cursor-pointer">
                                        Details
                                    </a>
                                </div>
                                @endif
                            </x-table-cell>
                        </x-table-row>
                        @empty
                        <x-table-row>
                            <x-table-cell class="text-center" colspan="12">
                                <div class="cursor-pointer">
                                    <div class="font-medium text-gray-900">
                                        No Record Found.
                                    </div>
                                </div>
                            </x-table-cell>
                        </x-table-row>
                        @endforelse
                    </x-slot>
                </x-table>
                <div class="pt-3">
                    {{ $activity_logs->withQueryString()->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
