<div>
    <div
        x-show="isOpen()"
        x-cloak
        x-transition:enter="transition-all ease-in-out duration-500 sm:duration-700"
        x-transition:enter-start="-ml-72"
        x-transition:enter-end="sm:w-72"
        x-transition:leave="transition-all ease-in-out duration-500 sm:duration-700"
        x-transition:leave-start="sm:w-72"
        x-transition:leave-end="-ml-72"
        class="fixed xl:sticky top-0 inset-0 flex bg-white bg-opacity-75 h-screen z-20"
    >
        <form 
            form method="POST" action="{{ route('logout') }}"
            @click.away="handleAway()"
            class="flex flex-col text-white bg-gray-800 shadow w-72"
        >
            @csrf
            <div class="flex bg-gray-900 content-between">
                <div class="p-[22px] text-[25px] w-full">IES(PET)</div>
                <a
                    @click.prevent="handleClose()"
                    class="p-[22px] hover:bg-green-900 flex-1 flex items-center xl:hidden"
                    href="#"
                >
                    <svg
                        class="h-5 w-5"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M6 18L18 6M6 6l12 12"
                        />
                    </svg>
                </a>
            </div>
            <div x-data="{selected: @entangle('selected')}" class="overscroll-y-contain flex-grow md:block pb-20 md:pb-10 md:overflow-y-auto z-10 scrollbar-thin scrollbar-thumb-gray-900 scrollbar-track-gray-700 scrollbar-thumb-rounded-full scrollbar-track-rounded-full">
                @if(!Str::contains($current_url, 'maintenance'))
                <div class="flex items-center w-full px-5 py-4 text-black xl:hidden">
                    {{-- dont show location dropdown when the current page is on the maintenance. --}}
                    <x-select wire:model="location" wire:loading.attr="disabled" wire:target="store" id="location" :name="__('location')" class="block mt-1 w-full" :errors="$errors" required>
                        @foreach($locations as $location)
                            <option value="{{ $location->id }}">{{ $location->name }}</option>
                        @endforeach
                    </x-select>
                    <x-input-error for="location"/>
                </div>
                @endif

                @can('view-dashboard')
                    <x-sidebar-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
                        <x-icons.outline.home class="h-5 w-5 mr-3"/>
                        {{ __('Dashboard') }}
                    </x-sidebar-link>
                    <x-sidebar-link :href="route('volunteers.index')" :active="request()->routeIs('volunteers.index') || request()->routeIs('volunteers.edit')">
                        <x-icons.outline.users class="h-5 w-5 mr-3"/>
                        {{ __('Volunteers') }}
                    </x-sidebar-link>
                @endcan
                @can('view-locations')
                    <x-sidebar-link :href="route('locations.index')" :active="request()->routeIs('locations.index')">
                        <x-icons.outline.mop-pin class="h-5 w-5 mr-3"/>
                        {{ __('Locations') }}
                    </x-sidebar-link>
                @endcan
                @can('view-material-types')
                    <x-sidebar-link :href="route('material-types.index')" :active="request()->routeIs('material-types.index')">
                        <x-icons.outline.swatch class="h-5 w-5 mr-3"/>
                        {{ __('Material Types') }}
                    </x-sidebar-link>
                @endcan
                @can('view-users')
                    <x-sidebar-link :href="route('users.index')" :active="request()->routeIs('users.index') || request()->routeIs('users.permissions') || request()->routeIs('users.activity-logs')">
                        <x-icons.outline.users class="h-5 w-5 mr-3"/>
                        {{ __('Users') }}
                    </x-sidebar-link>
                @endcan    

                <x-sidebar-link href="#" onclick="event.preventDefault(); this.closest('form').submit();" :class="'xl:hidden'">
                    <x-icons.outline.logout class="h-5 w-5 mr-3"/>
                    {{ __('Log out') }}
                </x-sidebar-link>
            </div>
        </form>
    </div>
</div>

@push('scripts')
<script>
    function sidebar() {
        const breakpoint = 1280
        return {
            open: {
                above: true,
                below: false,
            },
            isAboveBreakpoint: window.innerWidth > breakpoint,
            handleResize() {
                this.isAboveBreakpoint = window.innerWidth > breakpoint
            },
            isOpen() {
                // console.log(this.isAboveBreakpoint)
                if (this.isAboveBreakpoint) {
                    return this.open.above
                }
                return this.open.below
            },
            handleOpen() {
                if (this.isAboveBreakpoint) {
                    this.open.above = true
                }
                this.open.below = true
                document.body.style.overflow = 'hidden'
            },
            handleClose() {
                if (this.isAboveBreakpoint) {
                    this.open.above = false
                }
                this.open.below = false
                document.body.style.overflow = 'auto'
            },
            handleAway() {
                if (!this.isAboveBreakpoint) {
                    this.open.below = false
                    document.body.style.overflow = 'auto'
                }
            },
        }
    }
</script>
@endpush