<div>
    <x-form-modal maxWidth="xl" wire:model.defer="isCreateEditModalOpen" submit="{{ $action }}">
        <x-slot name="title">
            {{ $form_title }} Volunteer
        </x-slot>
        <x-slot name="description"></x-slot>
        <x-slot name="content">
            <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                <div class="col-span-12">
                    <x-label :for="__('name')" :value="__('Name')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="name" class="block mt-1 w-full uppercase" type="text" :name="__('name')" :value="old('name')" :errors="$errors"
                                wire:model.defer="name"
                                wire:loading.attr="disabled"
                                wire:target='store, update'
                                required
                                autofocus
                        />
                        <x-input-error for="name"/>
                    </div>
                </div>
                <div class="col-span-12">
                    <x-label :for="__('address')" :value="__('Address')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="address" class="block mt-1 w-full uppercase" type="text" :name="__('address')" :value="old('address')" :errors="$errors"
                                wire:model.defer="address"
                                wire:loading.attr="disabled"
                                wire:target='store, update'
                        />
                        <x-input-error for="address"/>
                    </div>
                </div>
                <div class="col-span-12">
                    <x-label :for="__('company_name')" :value="__('Company Name')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="company_name" class="block mt-1 w-full uppercase" type="text" :name="__('company_name')" :value="old('company_name')" :errors="$errors"
                                wire:model.defer="company_name"
                                wire:loading.attr="disabled"
                                wire:target='store, update'
                        />
                        <x-input-error for="company_name"/>
                    </div>
                </div>
                <div class="col-span-12">
                    <x-label :for="__('contact_no')" :value="__('Contact No.')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="contact_no" class="block mt-1 w-full uppercase" type="text" :name="__('contact_no')" :value="old('contact_no')" :errors="$errors"
                                wire:model.defer="contact_no"
                                wire:loading.attr="disabled"
                                wire:target='store, update'
                        />
                        <x-input-error for="contact_no"/>
                    </div>
                </div>
                <div class="col-span-12">
                    <x-label :for="__('encoded_at')" :value="__('Date')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="encoded_at" class="block mt-1 w-full uppercase" type="date" :name="__('encoded_at')" :value="old('encoded_at')" :errors="$errors" 
                                wire:model.defer="encoded_at"
                                wire:loading.attr="disabled"
                                wire:target='store, update'
                        />
                        <x-input-error for="encoded_at"/>
                    </div>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-button type="button" wire:click="closeCreateEditModal()" wire:loading.attr="disabled" wire:target="store, update" class="ml-3 my-1">
                <x-icons.solid.x class="h-4 w-4 mr-2"/>
                {{ __('Cancel') }}
            </x-button>
            <x-button class="ml-3 my-1" wire:loading.attr="disabled" wire:target="store, update">
                <div wire:loading wire:target="store, update">
                    <x-icons.loading class="w-4 mr-2"/>
                </div>
                <x-icons.solid.paper-plane wire:loading.remove wire:target="store, update" class="h-4 w-4 mr-1 -mt-1 rotate-45"/>
                {{ $button_name }}
            </x-button>
        </x-slot>
    </x-form-modal>
</div>
