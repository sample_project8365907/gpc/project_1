<div class="overflow-hidden shadow-sm">
    <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
        <div class="col-span-12 lg:col-span-9 bg-white w-full p-10 order-2 lg:order-1">
            <form wire:submit.prevent="update" method="POST">
                <div class="grid grid-cols-1 lg:grid-cols-12 gap-6">
                    <div class="col-span-12 lg:col-span-4">
                        <x-label :for="__('code')" :value="__('Code')" :errors="$errors"/>
                        <div class="relative">
                            <x-input id="code" class="block mt-1 w-full" type="text" :name="__('code')" :value="old('code')" :errors="$errors" 
                                    wire:model.defer="code"
                                    wire:loading.attr="disabled"
                                    wire:target='update'
                                    disabled
                            />
                            <x-input-error for="code"/>
                        </div>
                    </div>
                    <div class="col-span-12 lg:col-span-4">
                        <x-label :for="__('name')" :value="__('Name')" :errors="$errors"/>
                        <div class="relative">
                            <x-input id="name" class="block mt-1 w-full" type="text" :name="__('name')" :value="old('name')" :errors="$errors" 
                                    wire:model.defer="name"
                                    wire:loading.attr="disabled"
                                    wire:target='update'
                                    required
                                    autofocus
                            />
                            <x-input-error for="name"/>
                        </div>
                    </div>
                    <div class="col-span-12 lg:col-span-4">
                        <x-label :for="__('address')" :value="__('Address')" :errors="$errors"/>
                        <div class="relative">
                            <x-input id="address" class="block mt-1 w-full" type="text" :name="__('address')" :value="old('address')" :errors="$errors" 
                                    wire:model.defer="address"
                                    wire:loading.attr="disabled"
                                    wire:target='update'
                            />
                            <x-input-error for="address"/>
                        </div>
                    </div>
                    <div class="col-span-12 lg:col-span-4">
                        <x-label :for="__('company_name')" :value="__('Company Name')" :errors="$errors"/>
                        <div class="relative">
                            <x-input id="company_name" class="block mt-1 w-full" type="text" :name="__('company_name')" :value="old('company_name')" :errors="$errors" 
                                    wire:model.defer="company_name"
                                    wire:loading.attr="disabled"
                                    wire:target='update'
                            />
                            <x-input-error for="company_name"/>
                        </div>
                    </div>
                    <div class="col-span-12 lg:col-span-4">
                        <x-label :for="__('contact_no')" :value="__('Contact No.')" :errors="$errors"/>
                        <div class="relative">
                            <x-input id="contact_no" class="block mt-1 w-full" type="text" :name="__('contact_no')" :value="old('contact_no')" :errors="$errors" 
                                    wire:model.defer="contact_no"
                                    wire:loading.attr="disabled"
                                    wire:target='update'
                            />
                            <x-input-error for="contact_no"/>
                        </div>
                    </div>

                    <hr class="col-span-12">

                    <div class="col-span-12">
                        <div class="flex justify-end items-center">
                            <x-button class="ml-3 my-1" wire:loading.attr="disabled">
                                <div wire:loading wire:target="update">
                                    <x-icons.loading class="w-4 mr-2"/>
                                </div>
                                <x-icons.solid.paper-plane wire:loading.remove wire:target="update" class="h-4 w-4 mr-1 -mt-1 rotate-45"/>
                                {{ __('Update') }}
                            </x-button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-span-12 lg:col-span-3 bg-white w-full p-10 order-1 lg:order-2">
            <a href="data:image/png;base64, {!! base64_encode(QrCode::format('png')->margin('1')->size(600)->generate($code)) !!}" target="_blank" class="flex justify-center items-center" download="{{ $code }}.png">
                {!! QrCode::size(200)->generate($code); !!}
            </a>
            <div class="pt-2 text-center text-gray-500">
                <small>[Click on the QR code to download it.]</small>
            </div>
        </div>
    </div>
</div>

