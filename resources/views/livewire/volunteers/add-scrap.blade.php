<div>
    <x-form-modal maxWidth="xl" wire:model.defer="isAddScrapModalOpen" submit="{{ $action }}">
        <x-slot name="title">
            Add Scrap
        </x-slot>
        <x-slot name="description"></x-slot>
        <x-slot name="content">
            <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                <div class="col-span-12">
                    <x-label :for="__('code')" :value="__('Code')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="code" class="block mt-1 w-full" type="text" :name="__('code')" :value="old('code')" :errors="$errors"
                                wire:model.defer="code"
                                wire:loading.attr="disabled"
                                wire:target='update'
                                disabled
                        />
                        <x-input-error for="code"/>
                    </div>
                </div>

                <div class="col-span-12">
                    <x-label :for="__('name')" :value="__('Name')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="name" class="block mt-1 w-full" type="text" :name="__('name')" :value="old('name')" :errors="$errors"
                                wire:model.defer="name"
                                wire:loading.attr="disabled"
                                wire:target='update'
                                disabled
                        />
                        <x-input-error for="name"/>
                    </div>
                </div>

                <div class="col-span-12">
                    <x-label :for="__('material_type_id')" :value="__('Material Type')" :errors="$errors"/>
                    <div class="relative">
                        <x-select wire:model="material_type_id" wire:loading.attr="disabled" wire:target="store, update, selectSupplier" id="material_type_id" :name="__('material_type_id')" class="block mt-1 w-full" :errors="$errors" autofocus required>
                            <option value="" selected>Select Material Type</option>
                            @foreach($material_types as $material_type)
                                <option value="{{ $material_type->id }}">{{ $material_type->name }}</option>
                            @endforeach
                        </x-select>
                        <x-input-error for="material_type_id"/>
                    </div>
                </div>

                <div class="col-span-12">
                    <x-label :for="__('weight')" :value="__('Weight')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="weight" class="block mt-1 w-full" type="text" :name="__('weight')" :value="old('weight')" :errors="$errors"
                                wire:model.defer="weight"
                                wire:loading.attr="disabled"
                                wire:target='update'
                                required
                        />
                        <x-input-error for="weight"/>
                    </div>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-button type="button" wire:click="closeAddScrapModal()" wire:loading.attr="disabled" wire:target="store, update" class="ml-3 my-1">
                <x-icons.solid.x class="h-4 w-4 mr-2"/>
                {{ __('Cancel') }}
            </x-button>
            <x-button class="ml-3 my-1" wire:loading.attr="disabled" wire:target="store, update">
                <div wire:loading wire:target="store, update">
                    <x-icons.loading class="w-4 mr-2"/>
                </div>
                <x-icons.solid.paper-plane wire:loading.remove wire:target="store, update" class="h-4 w-4 mr-1 -mt-1 rotate-45"/>
                {{ $button_name }}
            </x-button>
        </x-slot>
    </x-form-modal>
</div>
