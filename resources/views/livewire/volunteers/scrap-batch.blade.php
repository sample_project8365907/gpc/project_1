<div>
    <x-form-modal maxWidth="sm" wire:model.defer="isScrapBatchModalOpen" submit="{{ $action }}">
        <x-slot name="title">
            Scrap Batch
        </x-slot>
        <x-slot name="description"></x-slot>
        <x-slot name="content">
            <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                <div class="col-span-12">
                    <x-label :for="__('scrap_current_batch')" :value="__('Batch')" :errors="$errors"/>
                    <div class="relative">
                        <x-select id="scrap_current_batch" class="block mt-1 w-full" :name="__('scrap_current_batch')" :errors="$errors"
                                wire:model="scrap_current_batch" 
                                wire:loading.attr="disabled"
                                wire:target="store, update, selectSupplier" 
                                required
                                autofocus>
                            <option value="" selected>Select Batch</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </x-select>
                        <x-input-error for="scrap_current_batch"/>
                    </div>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-button type="button" wire:click="closeScrapBatchModal()" wire:loading.attr="disabled" wire:target="update" class="ml-3 my-1">
                <x-icons.solid.x class="h-4 w-4 mr-2"/>
                {{ __('Cancel') }}
            </x-button>
            <x-button class="ml-3 my-1" wire:loading.attr="disabled" wire:target="update">
                <div wire:loading wire:target="update">
                    <x-icons.loading class="w-4 mr-2"/>
                </div>
                <x-icons.solid.paper-plane wire:loading.remove wire:target="update" class="h-4 w-4 mr-1 -mt-1 rotate-45"/>
                {{ $button_name }}
            </x-button>
        </x-slot>
    </x-form-modal>
</div>