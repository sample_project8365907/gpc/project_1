<div>
    <div class="mx-auto py-8 sm:px-6 lg:px-8 pb-10">
        <div class="bg-white overflow-hidden shadow-sm">
            <div class="flex justify-between items-center px-6 py-4 border-b font-bold">
                <div class="flex justify-start">
                    QR Scanner
                </div>
            </div>
            <div class="bg-white overflow-hidden shadow-sm">
                <div class="flex justify-center items-center px-4 py-4 border-b font-bold">
                    <div>
                        <div wire:ignore id="video-container">
                            <video id="qr-video"></video>
                        </div>
                        <div id="cam-qr-result" class="py-4">
                            <div><span class="text-lg">Code:</span> {{ $code }}</div>
                            <div><span class="text-lg">Name:</span> {{ $name }}</div>
                        </div>
                        <div class="flex justify-center items-center">
                            <x-button wire:click="$emitTo('volunteers.add-scrap', 'open-add-scrap-modal', {{ $code }})" :disabled="$disable_button">Proceed</x-button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    document.addEventListener('livewire:load', function () {
        const video = document.getElementById('qr-video');
        const label = document.getElementById('cam-qr-result');

        function setResult(label, result) {
            console.log(result.data);
            @this.result = result.data;
            // label.textContent = result.data;
            // camQrResultTimestamp.textContent = new Date().toString();
            label.style.color = 'green';
            clearTimeout(label.highlightTimeout);
            label.highlightTimeout = setTimeout(() => label.style.color = 'inherit', 100);

            scanner.start();
        }

        const scanner = new QrScanner(video, result => setResult(label, result), {
            // onDecodeError: error => {
            //     camQrResult.textContent = error;
            //     camQrResult.style.color = 'inherit';
            // },
            highlightScanRegion: true,
            highlightCodeOutline: true,
        });

        scanner.start();

        // window.scanner = scanner;

        // document.getElementById('closeScanner').addEventListener('click', () => {
        //     scanner.stop();
        // })
    });
</script>
@endpush

