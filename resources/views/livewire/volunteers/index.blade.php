<div>
    <div class="mx-auto py-8 sm:px-6 lg:px-8 pb-10">
        <div class="bg-white overflow-hidden shadow-sm">
            <div class="bg-white overflow-hidden shadow-sm">
                <div class="xl:flex justify-between items-center px-6 py-4 border-b font-bold">
                    <div class="flex justify-start pr-4 hidden xl:block">
                        <x-input id="search" class="block mt-1 w-full" type="text" :name="__('search')" placeholder="Search" wire:model="search"/>
                    </div>
                    <div class="flex justify-stretch xl:justify-end items-center gap-4">
                        <x-button wire:click="generateExcessWeight" class="w-full xl:w-auto" wire:loading.attr="disabled" wire:target="generateExcessWeight">
                            <div wire:loading wire:target="generateExcessWeight">
                                <x-icons.loading class="w-4 mr-2"/>
                            </div>
                            <x-icons.solid.scale wire:loading.remove wire:target="generateExcessWeight" class="h-4 w-4 mr-2"/>
                            Generate Excess Weight
                        </x-button>
                        <x-button-link href="{{ route('volunteers.scan-qr') }}" class="w-full xl:w-auto">
                            Scan QR
                        </x-button-link>
                        <x-button wire:click="$emitTo('volunteers.create', 'open-create-modal')" class="w-full xl:w-auto">
                            Add Volunteer
                        </x-button>
                        <div>
                            <x-dropdown align="right" width="48">
                                <x-slot name="trigger">
                                    <button class="flex items-center text-sm font-medium text-gray-500 hover:text-green-800 focus:outline-none focus:text-green-800 transition duration-150 ease-in-out">
                                        <x-icons.solid.bars-3 class="h-5 w-5 cursor-pointer"/>
                                    </button>
                                </x-slot>
            
                                <x-slot name="content">
                                    <x-dropdown-link wire:click="$emitTo('volunteers.scrap-batch', 'open-scrap-batch-modal')" class="cursor-pointer" onclick="event.preventDefault();">
                                        {{ __('Scrap Batch') }}
                                    </x-dropdown-link>
                                </x-slot>
                            </x-dropdown>
                        </div>
                    </div>
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="block xl:hidden">
                        <x-input id="search" class="block mt-1 mb-6 w-full" type="text" :name="__('search')" placeholder="Search" wire:model="search"/>
                    </div>
                    <div class="mb-4">
                        <x-table class="">
                            <x-slot name="head">
                                <x-table-heading>
                                    PET WEIGHT
                                </x-table-heading>
                                <x-table-heading>
                                    SOFT PLASTIC WEIGHT
                                </x-table-heading>
                                <x-table-heading>
                                    TOTAL WEIGHT
                                </x-table-heading>
                                <x-table-heading>
                                    TOTAL COUPON(S)
                                </x-table-heading>
                            </x-slot>
                            <x-slot name="body">
                                <x-table-row>
                                    <x-table-cell>
                                        <div class="cursor-pointer">
                                            <div class="font-bold text-xl text-red-500">
                                                {{ $total_pet }}
                                            </div>
                                        </div>
                                    </x-table-cell>
                                    <x-table-cell>
                                        <div class="cursor-pointer">
                                            <div class="font-bold text-xl text-red-500">
                                                {{ $total_sf }}
                                            </div>
                                        </div>
                                    </x-table-cell>
                                    <x-table-cell>
                                        <div class="cursor-pointer">
                                            <div class="font-bold text-xl text-red-500">
                                                {{ $total_weight }}
                                            </div>
                                        </div>
                                    </x-table-cell>
                                    <x-table-cell>
                                        <div class="cursor-pointer">
                                            <div class="font-bold text-xl text-red-500">
                                                {{ $total_coupons }}
                                            </div>
                                        </div>
                                    </x-table-cell>
                                </x-table-row>
                            </x-slot>
                        </x-table>
                    </div>
                    <x-table>
                        <x-slot name="head">
                            <x-table-heading>
                                CODE
                            </x-table-heading>
                            <x-table-heading>
                                NAME
                            </x-table-heading>
                            <x-table-heading>
                                ADDRESS
                            </x-table-heading>
                            <x-table-heading>
                                COMPANY NAME
                            </x-table-heading>
                            <x-table-heading>
                                CONTACT NO.
                            </x-table-heading>
                            <x-table-heading>
                                Total Weight
                            </x-table-heading>
                            <x-table-heading>
                                Total Coupon
                            </x-table-heading>
                            <x-table-heading>
                                ACTION
                            </x-table-heading>
                        </x-slot>
                        <x-slot name="body">
                            @forelse($volunteers as $volunteer)
                            @php
                                $total_weight_each = $volunteer->scraps()->where('status_id', 1)->where('scrap_batch_id', $batch_id)->pluck('weight')->sum();
                                $total_coupon_each = floor($total_weight_each/2);
                            @endphp
                            <x-table-row>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $volunteer->code }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $volunteer->name }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $volunteer->address }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $volunteer->company_name }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $volunteer->contact_no }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $total_weight_each }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $total_coupon_each }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <a href="{{ route('volunteers.edit', $volunteer->id) }}" 
                                        class="h-5 w-5 text-green-600 hover:text-green-500 font-bold cursor-pointer">
                                        Edit
                                    </a>
                                </x-table-cell>
                            </x-table-row>
                            @empty
                            <x-table-row>
                                <x-table-cell class="text-center" colspan="12">
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            No Record Found.
                                        </div>
                                    </div>
                                </x-table-cell>
                            </x-table-row>
                            @endforelse
                        </x-slot>
                    </x-table>
                    <div class="pt-3">
                        {{ $volunteers->withQueryString()->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

