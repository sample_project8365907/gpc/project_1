<div>
    {{-- Stop trying to control. --}}
    <div class="mx-auto py-6 sm:px-6">
        <div class="bg-white overflow-hidden shadow-sm">
            <div class="flex justify-between items-center p-6 border-b font-bold">
                <div class="flex mr-auto items-center gap-4">
                    <x-button-link href="{{ route('database.export') }}">
                        <x-icons.solid.folder-arrow-down class="h-4 w-4 mr-2"/>
                        Export Database
                    </x-button-link>
                </div>
            </div>
        </div>
    </div>
</div>
