<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Welcome</title>

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
        <link rel="manifest" href="/icons/site.webmanifest">

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased" style="background-color: #f3f3f4;">
            <!-- Page Content -->
            <main class="min-h-screen flex flex-col justify-center items-center pt-6 pt-0 bg-gray-100 text-center">
                <article>
                    <div class="flex flex-col justify-center items-center">
                        <a href="/">
                            {{-- <x-application-logo class="w-20 h-20 fill-current text-gray-500" /> --}}
                            <img src="{{ asset('/images/greenkraft-logo.png') }}" class="h-20" alt="logo">
                        </a>
                    </div>
                    <br/>
                    <div class="text-5xl font-bold">Welcome to IES</div>
                    <br/>
                    <div>
                        <p class="text-2xl font-semibold">Please contact your system administrator to get access.</p>
                        <br/>
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <p class="text-lg font-semibold text-gray-400 hover:text-green-700 cursor-pointer">
                                <a :href="route('logout')" onclick="event.preventDefault();
                                this.closest('form').submit();"">Logout</a>
                            </p>
                        </form>
                    </div>
                </article>
            </main>
    </body>
</html>