<div>
    <div class="mx-auto py-6 sm:px-6">
        <div class="bg-white overflow-hidden shadow-sm">
            <div class="flex justify-between items-center p-6 border-b font-bold">
                <div class="flex justify-start">
                    Locations
                </div>
            </div>
            <div class="bg-white overflow-hidden shadow-sm">
                <div class="flex justify-between items-center px-6 py-4 border-b font-bold">
                    <div class="flex justify-start gap-4 hidden xl:block">
                        <x-input id="search" class="block mt-1 w-full" type="text" :name="__('search')" placeholder="Search" wire:model="search"/>
                    </div>
                    @can('create-baling-stations')
                        <div class="flex justify-end items-center gap-4">
                            <x-button wire:click="$emitTo('locations.create-edit', 'open-create-modal')" class="w-full">
                                <x-icons.solid.plus class="h-4 w-4 mr-2"/>
                                Add Location
                            </x-button>
                        </div>
                    @endcan
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="block xl:hidden">
                        <x-input id="search" class="block mt-1 mb-6 w-full" type="text" :name="__('search')" placeholder="Search" wire:model="search"/>
                    </div>
                    <x-table>
                        <x-slot name="head">
                            <x-table-heading>
                                NAME
                            </x-table-heading>
                            <x-table-heading>
                                CODE
                            </x-table-heading>
                            @can('edit-locations')
                            <x-table-heading>
                                ACTION
                            </x-table-heading>
                            @endcan
                        </x-slot>
                        <x-slot name="body">
                            @forelse($locations as $location)
                            <x-table-row>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $location->name }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            {{ $location->code }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                @can('edit-locations')
                                <x-table-cell>
                                    <a wire:click="$emitTo('locations.create-edit', 'open-edit-modal', {{ $location->id }})" 
                                        class="h-5 w-5 text-green-600 hover:text-green-500 font-bold cursor-pointer">
                                        Edit
                                    </a>
                                </x-table-cell>
                                @endcan
                            </x-table-row>
                            @empty
                            <x-table-row>
                                <x-table-cell class="text-center" colspan="12">
                                    <div class="cursor-pointer">
                                        <div class="font-medium text-gray-900">
                                            No Record Found.
                                        </div>
                                    </div>
                                </x-table-cell>
                            </x-table-row>
                            @endforelse
                        </x-slot>
                    </x-table>
                    <div class="pt-3">
                        {{ $locations->withQueryString()->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>