<div>
    <x-form-modal maxWidth="xl" wire:model.defer="isCreateEditModalOpen" submit="{{ $action }}">
        <x-slot name="title">
            {{ $form_title }} Location
        </x-slot>
        <x-slot name="description"></x-slot>
        <x-slot name="content">
            <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                <div class="col-span-12">
                    <x-label :for="__('name')" :value="__('Name')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="name" class="block mt-1 w-full" type="text" :name="__('name')" :value="old('name')" :errors="$errors"
                                wire:model.defer="name"
                                wire:loading.attr="disabled"
                                wire:target='update'
                                required
                                autofocus
                        />
                        <x-input-error for="name"/>
                    </div>
                </div>

                <div class="col-span-12">
                    <x-label :for="__('code')" :value="__('Code')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="code" class="block mt-1 w-full" type="text" :name="__('code')" :value="old('code')" :errors="$errors" 
                                wire:model.defer="code"
                                wire:loading.attr="disabled"
                                wire:target='store'
                                required
                        />
                        <x-input-error for="code"/>
                    </div>
                </div>

                <div class="col-span-12">
                    <div class="break-inside-avoid rounded-lg">
                        <legend class="sr-only">Material Types</legend>
                        <div class="block font-medium text-sm text-gray-700" aria-hidden="true">Material Types</div>
                        <div class="mt-1 space-y-4 border-2 p-3 rounded">
                            @foreach($material_types as $material_type)
                            @php $index++ @endphp
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input name="selected_material_types[{{ $index }}]" id="selected_material_types[{{ $index }}]" type="checkbox" value="{{ $material_type->id }}" class="focus:ring-green-700 h-4 w-4 text-green-800 border-gray-300 rounded"
                                            wire:model.defer="selected_material_types"
                                            wire:loading.attr="disabled" 
                                            wire:target="store"
                                    />
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="selected_material_types[{{ $index }}]" class="font-medium text-gray-700 hover:cursor-pointer">{{ $material_type->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-button type="button" wire:click="closeCreateEditModal()" wire:loading.attr="disabled" wire:target="store, update" class="ml-3 my-1">
                <x-icons.solid.x class="h-4 w-4 mr-2"/>
                {{ __('Cancel') }}
            </x-button>
            <x-button class="ml-3 my-1" wire:loading.attr="disabled" wire:target="store, update">
                <div wire:loading wire:target="store, update">
                    <x-icons.loading class="w-4 mr-2"/>
                </div>
                <x-icons.solid.paper-plane wire:loading.remove wire:target="store, update" class="h-4 w-4 mr-1 -mt-1 rotate-45"/>
                {{ $button_name }}
            </x-button>
        </x-slot>
    </x-form-modal>
</div>
