<div>
    <div class="mx-auto py-8 sm:px-6 lg:px-8 pb-10">
        <div class="bg-white overflow-hidden shadow-sm">
            <div class="flex justify-between items-center px-6 py-4 border-b font-bold">
                <div>
                    All Plants
                </div>
            </div>
            <div class="px-6 py-4 border-b font-bold">
                <div class="flex justify-stretch xl:justify-end items-center gap-4">
                    <x-button-link href="{{ route('volunteers.download-coupon', ['batch_id' => $batch_id]) }}" target="_blank" class="w-full xl:w-auto">
                        Download Coupons
                    </x-button-link>
                    <x-button wire:click="downloadForm" class="w-full xl:w-auto">
                        Download Form
                    </x-button>
                </div>
            </div>
            <div class="p-6 bg-white border-b border-gray-200">
                <div class="mb-4">
                    <x-table>
                        <x-slot name="head">
                            <x-table-heading>
                                PET WEIGHT
                            </x-table-heading>
                            <x-table-heading>
                                SOFT PLASTIC WEIGHT
                            </x-table-heading>
                            <x-table-heading>
                                TOTAL WEIGHT
                            </x-table-heading>
                            <x-table-heading>
                                TOTAL COUPON(S)
                            </x-table-heading>
                        </x-slot>
                        <x-slot name="body">
                            <x-table-row>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-bold text-xl text-red-500">
                                            {{ $total_pet }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-bold text-xl text-red-500">
                                            {{ $total_sf }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-bold text-xl text-red-500">
                                            {{ $total_weight }}
                                        </div>
                                    </div>
                                </x-table-cell>
                                <x-table-cell>
                                    <div class="cursor-pointer">
                                        <div class="font-bold text-xl text-red-500">
                                            {{ $total_coupons }}
                                        </div>
                                    </div>
                                </x-table-cell>
                            </x-table-row>
                        </x-slot>
                    </x-table>
                </div>
            </div>
        </div>
    </div>

    <div class="grid grid-cols-12">
        @foreach($plant_infos as $plant_info)
            <div class="col-span-12 lg:col-span-6">
                <div class="mx-auto sm:px-6 lg:px-8 pb-10">
                    <div class="bg-white overflow-hidden shadow-sm">
                        <div class="flex justify-start items-center px-6 py-4 border-b font-bold">
                            <div>
                                {{ $plant_info['plant_name'] }}
                            </div>
                        </div>
                        <div class="p-6 bg-white border-b border-gray-200">
                            <div class="mb-4">
                                <x-table>
                                    <x-slot name="head">
                                        <x-table-heading>
                                            PET WEIGHT
                                        </x-table-heading>
                                        <x-table-heading>
                                            SOFT PLASTIC WEIGHT
                                        </x-table-heading>
                                        <x-table-heading>
                                            TOTAL WEIGHT
                                        </x-table-heading>
                                        <x-table-heading>
                                            TOTAL COUPON(S)
                                        </x-table-heading>
                                    </x-slot>
                                    <x-slot name="body">
                                        <x-table-row>
                                            <x-table-cell>
                                                <div class="cursor-pointer">
                                                    <div class="font-bold text-xl text-red-500">
                                                        {{ $plant_info['total_pet'] }}
                                                    </div>
                                                </div>
                                            </x-table-cell>
                                            <x-table-cell>
                                                <div class="cursor-pointer">
                                                    <div class="font-bold text-xl text-red-500">
                                                        {{ $plant_info['total_sf']}}
                                                    </div>
                                                </div>
                                            </x-table-cell>
                                            <x-table-cell>
                                                <div class="cursor-pointer">
                                                    <div class="font-bold text-xl text-red-500">
                                                        {{ $plant_info['total_weight'] }}
                                                    </div>
                                                </div>
                                            </x-table-cell>
                                            <x-table-cell>
                                                <div class="cursor-pointer">
                                                    <div class="font-bold text-xl text-red-500">
                                                        {{ $plant_info['total_coupons'] }}
                                                    </div>
                                                </div>
                                            </x-table-cell>
                                        </x-table-row>
                                    </x-slot>
                                </x-table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

