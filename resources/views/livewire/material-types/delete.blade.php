<div>
    <x-form-modal maxWidth="xl" wire:model.defer="isDeleteModalOpen" submit="delete">
        <x-slot name="title">
            Delete {{ $name }}
        </x-slot>
        <x-slot name="description"></x-slot>
        <x-slot name="content">
            <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                <div class="col-span-12">
                    <span>Are you sure you want to delete this Material Type?</span>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-button type="button" wire:click="closeDeleteModal()" wire:loading.attr="disabled" wire:target="delete" class="ml-3 my-1">
                <x-icons.solid.x class="h-4 w-4 mr-2"/>
                {{ __('No') }}
            </x-button>
            <x-button class="ml-3 my-1" wire:loading.attr="disabled" wire:target="delete">
                <div wire:loading wire:target="delete">
                    <x-icons.loading class="w-4 mr-2"/>
                </div>
                <x-icons.solid.check wire:loading.remove wire:target="delete" class="h-4 w-4 mr-1"/>
                {{ __('Yes') }}
            </x-button>
        </x-slot>
    </x-form-modal>
</div>
