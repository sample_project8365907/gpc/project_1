<div>
    <div class="bg-white overflow-hidden shadow-sm">
        <div class="px-6 py-4 border-b font-bold">
            Add Material Type
        </div>
        <div class="p-6 bg-white border-b border-gray-200">
            <form wire:submit.prevent="store" method="POST">
                <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                    <div class="col-span-12">
                        <x-label :for="__('name')" :value="__('Name')" :errors="$errors"/>
                        <div class="relative">
                            <x-input id="name" class="block mt-1 w-full" type="text" :name="__('name')" :value="old('name')" :errors="$errors" 
                                    wire:model.defer="name"
                                    wire:loading.attr="disabled"
                                    wire:target='store'
                                    required
                                    autofocus
                            />
                            <x-input-error for="name"/>
                        </div>
                    </div>

                    <div class="col-span-12">
                        <div class="flex justify-end items-center">
                            <x-button class="ml-3 my-1" wire:loading.attr="disabled" wire:target="store">
                                <div wire:loading wire:target="store">
                                    <x-icons.loading class="w-4 mr-2"/>
                                </div>
                                <x-icons.solid.paper-plane wire:loading.remove wire:target="store" class="h-4 w-4 mr-1 -mt-1 rotate-45"/>
                                {{ __('Submit') }}
                            </x-button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
