<div>
    <x-form-modal maxWidth="xl" wire:model.defer="isEditModalOpen" submit="update">
        <x-slot name="title">
            Edit Material Type
        </x-slot>
        <x-slot name="description"></x-slot>
        <x-slot name="content">
            <div class="grid grid-cols-1 sm:grid-cols-12 gap-6">
                <div class="col-span-12">
                    <x-label :for="__('name')" :value="__('Name')" :errors="$errors"/>
                    <div class="relative">
                        <x-input id="name" class="block mt-1 w-full" type="text" :name="__('name')" :value="old('name')" :errors="$errors" 
                                wire:model.defer="name"
                                wire:loading.attr="disabled"
                                wire:target='store'
                                required
                                autofocus
                        />
                        <x-input-error for="name"/>
                    </div>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-button type="button" wire:click="closeEditModal()" wire:loading.attr="disabled" wire:target="update" class="ml-3 my-1">
                <x-icons.solid.x class="h-4 w-4 mr-2"/>
                {{ __('Cancel') }}
            </x-button>
            <x-button class="ml-3 my-1" wire:loading.attr="disabled" wire:target="update">
                <div wire:loading wire:target="update">
                    <x-icons.loading class="w-4 mr-2"/>
                </div>
                <x-icons.solid.paper-plane wire:loading.remove wire:target="update" class="h-4 w-4 mr-1 -mt-1 rotate-45"/>
                {{ __('Update') }}
            </x-button>
        </x-slot>
    </x-form-modal>
</div>
