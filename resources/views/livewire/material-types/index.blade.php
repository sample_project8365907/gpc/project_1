<div>
    <div class="bg-white overflow-hidden shadow-sm">
        {{-- <div class="flex justify-between items-center px-6 py-4 border-b font-bold">
            <div class="flex justify-start">
                <div class="relative">
                    <x-input id="search" class="block mt-1 w-full" type="text" :name="__('search')" placeholder="Search" wire:model="search"/>
                </div>
            </div>
            <div class="flex justify-end items-center gap-4">
                <x-button-link href="{{ route('productions.excel') }}" id="excel-production">
                    Excel
                </x-button-link>
                <x-button-link href="{{ route('productions.print') }}" target="_blank" id="print-production">
                    Print
                </x-button-link>
            </div>
        </div> --}}
        <div class="p-6 bg-white border-b border-gray-200">
            <x-table>
                <x-slot name="head">
                    <x-table-heading>
                        Material Type
                    </x-table-heading>
                    @canany(['edit-material-types', 'delete-material-types'])
                    <x-table-heading>
                        Action
                    </x-table-heading>
                    @endcanany
                </x-slot>
                <x-slot name="body">
                    @forelse($material_types as $material_type)
                    <x-table-row>
                        <x-table-cell>
                            <div class="cursor-pointer" >
                                <div class="font-medium text-gray-900">
                                    {{ $material_type->name }}
                                </div>
                            </div>
                        </x-table-cell>
                        
                        <x-table-cell class="flex justify-center items-center cursor-pointer gap-2">
                            @can('edit-material-types')
                            <a wire:click="$emitTo('material-types.edit', 'open-edit-modal', {{ $material_type->id }})" 
                                class="text-green-600 hover:text-green-500 font-bold cursor-pointer">
                                Edit
                            </a>
                            @endcan
                            @can(['edit-material-types', 'delete-material-types'])
                            <div>
                                |
                            </div>
                            @endcan
                            @can('delete-material-types')
                            <a wire:click="$emitTo('material-types.delete', 'open-delete-modal', {{ $material_type->id }})" 
                                class="text-green-600 hover:text-green-500 font-bold cursor-pointer">
                                Delete
                            </a>
                            @endcan
                        </x-table-cell>
                    </x-table-row>
                    @empty
                    <x-table-row>
                        <x-table-cell class="text-center" colspan="12">
                            <div class="cursor-pointer">
                                <div class="font-medium text-gray-900">
                                    No Record Found.
                                </div>
                            </div>
                        </x-table-cell>
                    </x-table-row>
                    @endforelse
                </x-slot>
            </x-table>
            <div class="pt-3">
                {{ $material_types->withQueryString()->links() }}
            </div>
        </div>
    </div>
</div>
