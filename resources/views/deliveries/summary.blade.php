<x-app-layout>
    <x-slot name="header">
        {{-- Breadcrumbs --}}
        <div class="flex justify-start items-center text-gray-500 font-bold">
            <a href="{{ route('dashboard') }}"><x-icons.solid.home class="h-6 w-6"></x-icons.solid.home></a>
            <x-icons.outline.chevron-right class="h-5 w-5"></x-icons.outline.chevron-right>
            <a href="#" class="mx-2">Deliveries</a>
            <x-icons.outline.chevron-right class="h-5 w-5"></x-icons.outline.chevron-right>
            <div class="text-green-600 mx-2">Summary</div>
        </div>
    </x-slot>

    @livewire('notification')
    @livewire('deliveries.view-summary')
    @livewire('deliveries.summary')

</x-app-layout>