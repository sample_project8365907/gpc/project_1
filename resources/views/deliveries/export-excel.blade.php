<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Deliveries</title>

    <style type="text/css"  media="all">
        @font-face {
            font-family: 'Nunito-400';
            src: url({{ public_path('fonts/Nunito-Regular.ttf') }});
        }

        @font-face {
            font-family: 'Nunito-500';
            src: url({{ public_path('fonts/Nunito-Medium.ttf') }});
        }

        @font-face {
            font-family: 'Nunito-700';
            src: url({{ public_path('fonts/Nunito-Bold.ttf') }});
        }

        body {
            font-family: 'Nunito-400', sans-serif;
        }

        .font-medium {
            font-family: 'Nunito-500', sans-serif;
        }

        .font-bold {
            font-family: 'Nunito-700', sans-serif;
        }
    </style>
</head>
<body>
    <table class="table-bordered table-collapse" style="font-size: 24px;">
        <thead>
            <tr>
                <th class="text-center vertical-center font-bold" colspan="{{ $headers_count }}" style="font-weight: bold; text-align: center; border: 2px solid #000; font-size: 18px; padding: 10px;">
                    GREENKRAFT CORPORATION | {{ strtoupper(session()->get('location')) }} BALING STATION
                </th>
            </tr>
            <tr>
                <th class="text-center vertical-center font-bold" colspan="{{ $headers_count }}" style="font-weight: bold; text-align: center; border: 2px solid #000; font-size: 16px; padding: 10px;">
                    SUMMARY OF DELIVERIES | {{ $date_title }}
                </th>
            </tr>
            <tr>
                @foreach($header3_rows as $header3_row)
                <th class="text-center vertical center" colspan="{{ $header3_row['cols'] }}" style="font-weight: bold; text-align:center; border: 2px solid #000; font-size: 14px; padding: 10px;">
                    {{ $header3_row['name'] }}
                </th>
                @endforeach
            </tr>
            <tr>
                @foreach($header4_rows as $header4_row)
                <th class="text-center vertical center" style="font-weight: bold; text-align:center; border: 2px solid #000;
                    @if($header4_row == 'MC' || $header4_row == 'OT' || $header4_row == 'PM')
                        width: 50px;
                    @endif
                ">
                    {{ $header4_row }}
                </th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @php
                $suppliers_weight_sum = 0;
                $summary_corrected_weight_sum = 0;
                $summary_weight_discrepancy_sum = 0;
            @endphp
            @forelse($deliveries as $key => $delivery)
            @php
                // FOR BALING STATIONS AND DELIVER TO PLANT DATA
                if ($delivery->summary) {
                    $summary_document_no = $delivery->summary->summary_document_no;

                    $summary_net_weight = $delivery->summary->summary_net_weight;
                    $summary_corrected_weight = $delivery->summary->summary_corrected_weight;
                    $summary_mc = $delivery->summary->summary_mc;
                    $summary_ot = $delivery->summary->summary_ot;
                    $summary_pm = $delivery->summary->summary_pm;
                    $summary_total_deduction = ($summary_net_weight - $summary_corrected_weight) - ($summary_mc + $summary_ot + $summary_pm);
                    $summary_weight_discrepancy = $delivery->corrected_weight - $summary_corrected_weight;
                } else {
                    $summary_document_no = '';

                    $summary_corrected_weight = 0;
                    $summary_mc = 0;
                    $summary_ot = 0;
                    $summary_pm = 0;
                    $summary_total_deduction = 0;
                    $summary_weight_discrepancy = 0;
                }
                
                // FOR TRUCKING CHARGES DATA
                if($delivery->truckingRateCategory) {
                    // Get the trucking rate based on the 
                    // Hauler, Trucking Rate Category, Trucking Destination (Paper Mills) and the latest Effective Date
                    $trucking_rate = $delivery->truckingRateCategory->truckingRate::where('hauler_id', $delivery->hauler_id)
                        ->where('trucking_rate_category_id', $delivery->truckingRateCategory->id)
                        ->where('trucking_destination_id', $delivery->paperMill->id)
                        ->where('status_id', 1)
                        ->whereDate('effective_date', '<=', $delivery->encoded_at)
                        ->orderBy('effective_date', 'desc')
                        ->first();

                    $trucking_name = $trucking_rate->destination->name;
                    $trucking_rate = $trucking_rate->amount;
                } else {
                    $trucking_name = '';
                    $trucking_rate = 0;
                }

                $suppliers_weight_sum += $delivery->corrected_weight;
                $summary_corrected_weight_sum += $summary_corrected_weight;
                $summary_weight_discrepancy_sum += $summary_weight_discrepancy;
                $total_test = [];
            @endphp
            <tr class="text-left">
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $delivery->encoded_at }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $delivery->location->name }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $delivery->paperMill->name }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $delivery->materialType->name }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $delivery->transfer_slip_no }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $summary_document_no }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $delivery->quantity_bales }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ number_format($delivery->corrected_weight, 2) }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ number_format($summary_corrected_weight, 2) }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $summary_mc }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $summary_ot }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $summary_pm }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ number_format($summary_total_deduction, 2) }}
                </td>
                @foreach($material_type_ids as $material_type_id)
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    @if($material_type_id == $delivery->material_type_id)
                    {{ number_format($delivery->corrected_weight, 2) }}
                    @endif
                </td>
                @endforeach
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ number_format($summary_weight_discrepancy, 2) }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    0
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    0
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $delivery->trip_ticket_no }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $delivery->hauler->name }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    @if($delivery->vehicle)
                        {{ $delivery->vehicle->plate_no }}
                    @endif
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $trucking_name }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;">
                    {{ $trucking_rate }}
                </td>
            </tr>
            @empty
            <tr>
                <td class="text-center" colspan="10">
                    no record found
                </td>
            </tr>
            @endforelse
            <tr>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="font-weight: bold; border: 2px solid #000; color: #FF0000;">
                    {{ number_format($suppliers_weight_sum, 2) }}
                </td>
                <td class="text-center vertical center" style="font-weight: bold; border: 2px solid #000; color: #FF0000;">
                    {{ number_format($summary_corrected_weight_sum, 2) }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                {{-- Material Types --}}
                @foreach($material_type_ids as $material_type_id)
                <td class="text-center vertical center" style="font-weight: bold; border: 2px solid #000; color: #FF0000;">
                    {{ number_format($deliveries->where('material_type_id', $material_type_id)->pluck('corrected_weight')->sum(), 2) }}
                </td>
                @endforeach
                {{-- End of Material Types --}}
                <td class="text-center vertical center" style="font-weight: bold; border: 2px solid #000; color: #FF0000;">
                    {{ number_format($summary_weight_discrepancy_sum, 2) }}
                </td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
                <td class="text-center vertical center" style="border: 2px solid #000;"></td>
            </tr>
        </tbody>
    </table>
</body>
</html>