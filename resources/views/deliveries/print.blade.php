<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Deliveries</title>

    {{-- Styles --}}
    <link rel="stylesheet" href="{{ public_path('css/table.css') }}" type="text/css" media="all">

    <style type="text/css"  media="all">
        @font-face {
            font-family: 'Nunito-400';
            src: url({{ public_path('fonts/Nunito-Regular.ttf') }});
        }

        @font-face {
            font-family: 'Nunito-500';
            src: url({{ public_path('fonts/Nunito-Medium.ttf') }});
        }

        @font-face {
            font-family: 'Nunito-700';
            src: url({{ public_path('fonts/Nunito-Bold.ttf') }});
        }

        body {
            font-family: 'Nunito-400', sans-serif;
        }

        .font-medium {
            font-family: 'Nunito-500', sans-serif;
        }

        .font-bold {
            font-family: 'Nunito-700', sans-serif;
        }
    </style>
</head>
<body>
    <div style="text-align: right; margin-bottom: 10px; font-size: 20px;">Date Generated: {{ now()->format('F j, Y') }}</div>
    <table class="table-bordered table-collapse" style="font-size: 24px;">
        <thead>
            <tr>
                <th class="border-1 text-center vertical-center font-bold" colspan="10" style="font-size: 30px; padding: 10px;">
                    GREENKRAFT CORPORATION | {{ strtoupper(session()->get('location')) }} BALING STATION
                </th>
            </tr>
            <tr>
                <th class="text-center vertical-center font-bold" colspan="10" style="font-size: 30px; padding: 10px;">
                    DELIVERIES | {{ $date_title }}
                </th>
            </tr>
            <tr class="border-1 text-left vertical-center font-bold">
                <td class="border-1 text-center vertical center">
                    DATE
                </td>
                <td class="border-1 text-center vertical center">
                    TS NO.
                </td>
                <td class="border-1 text-center vertical center">
                    TRIP TICKET NO.
                </td>
                <td class="border-1 text-center vertical center">
                    DELIVER TO
                </td>
                <td class="border-1 text-center vertical center">
                    QTY (BALES)
                </td>
                <td class="border-1 text-center vertical center">
                    QTY (KGS)
                </td>
                <td class="border-1 text-center vertical center">
                    MATERIAL TYPE
                </td>
                <td class="border-1 text-center vertical center">
                    HAULER
                </td>
                <td class="border-1 text-center vertical center">
                    TIME IN
                </td>
                <td class="border-1 text-center vertical center">
                    TIME OUT
                </td>
            </tr>
        </thead>
        <tbody>
            @forelse($deliveries as $key => $delivery)
            <tr class="text-left">
                <td class="border-1 text-center vertical center">
                    {{ date('M d, Y', strtotime($delivery->encoded_at)) }}
                </td>
                <td class="border-1 text-center vertical center">
                    {{ $delivery->transfer_slip_no }}
                </td>
                <td class="border-1 text-center vertical center">
                    {{ $delivery->trip_ticket_no }}
                </td>
                <td class="border-1 text-center vertical center">
                    {{ $delivery->paperMill->name }}
                </td>
                <td class="border-1 text-center vertical center">
                    {{ $delivery->quantity_bales }}
                </td>
                <td class="border-1 text-center vertical center">
                    {{ number_format($delivery->corrected_weight) }}
                </td>
                <td class="border-1 text-center vertical center">
                    {{ $delivery->materialType->name }}
                </td>
                <td class="border-1 text-center vertical center">
                    {{ $delivery->hauler->name }}
                </td>
                <td class="border-1 text-center vertical center">
                    @if($delivery->time_in == null)
                    --
                    @else
                    {{ date('h:i a', strtotime($delivery->time_in)) }}
                    @endif
                </td>
                <td class="border-1 text-center vertical center">
                    @if($delivery->time_out == null)
                    --
                    @else
                    {{ date('h:i a', strtotime($delivery->time_out)) }}
                    @endif
                </td>
            </tr>
            @empty
            <tr>
                <td class="border-1 text-center" colspan="10">
                    no record found
                </td>
            </tr>
            @endforelse
        </tbody>
    </table>

    <table class="table-bordered table-collapse" style="font-size: 24px; width: 45%; padding-top: 100px;">
        <thead>
            <tr>
                <th class="border-1 text-center vertical-center font-bold" colspan="4" style="font-size: 30px; padding: 10px;">
                    TOTAL SUMMARY
                </th>
            </tr>
            <tr class="border-1 text-left vertical-center font-bold">
                <th class="border-1 text-center vertical center">
                    PAPER MILL
                </th>
                <th class="border-1 text-center vertical center">
                    ITEM
                </th>
                <th class="border-1 text-center vertical center">
                    QUANTITY
                </th>
                <th class="border-1 text-center vertical center">
                    VOLUME
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($summaries as $index => $summary)
                @if($summary['quantity'] != 0)
                    <tr class="text-left">
                        <td class="border-1 text-left vertical center">
                            {{ $summary['paper_mill'] }}
                        </td>
                        <td class="border-1 text-center vertical center">
                            {{ $summary['material_type'] }}
                        </td>
                        <td class="border-1 text-center vertical center">
                            {{ number_format($summary['quantity']) }}
                        </td>
                        <td class="border-1 text-center vertical center">
                            {{ number_format($summary['volume']) }}
                        </td>
                    </tr>
                @endif
            @endforeach
            <tr>
                <td class="border-1 text-center vertical center font-bold">TOTAL</td>
                <td class="border-1 text-center vertical center font-bold"></td>
                <td class="border-1 text-center vertical center font-bold">{{ number_format($total_summary_quantity) }}</td>
                <td class="border-1 text-center vertical center font-bold">{{ number_format($total_summary_volume) }}</td>
            </tr>
        </tbody>
    </table>
</body>
</html>