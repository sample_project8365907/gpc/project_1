<x-app-layout>
    <x-slot name="header">
        {{-- Breadcrumbs --}}
        <div class="flex justify-start items-center text-gray-500 font-bold">
            <a href="{{ route('dashboard') }}"><x-icons.solid.home class="h-6 w-6 mx-2"></x-icons.solid.home></a>
            <x-icons.outline.chevron-right class="h-5 w-5"></x-icons.outline.chevron-right>
            <a href="#" class="mx-2">Maintenance</a>
            <x-icons.outline.chevron-right class="h-5 w-5"></x-icons.outline.chevron-right>
            <a href="{{ route('users.index') }}" class="mx-2">Users</a>
            <x-icons.outline.chevron-right class="h-5 w-5"></x-icons.outline.chevron-right>
            <div class="text-green-600 mx-2">Activity Logs</div>
        </div>
    </x-slot>

    @livewire('users.activity-logs')
    @livewire('users.activity-logs-detail')

</x-app-layout>