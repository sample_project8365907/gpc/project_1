<x-app-layout>
    <x-slot name="header">
        {{-- Breadcrumbs --}}
        <div class="flex justify-start items-center text-gray-500 font-bold">
            <a href="#" class="text-green-600">Dashboard</a>
        </div>
    </x-slot>

    @livewire('notification')
    @livewire('dashboard')
    
</x-app-layout>