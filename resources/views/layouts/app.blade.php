<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Ingress and Egress System') }}</title>

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
        <link rel="manifest" href="/icons/site.webmanifest" />

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="/css/flatpickr.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css" />
        <style>
            @media print {
                body { 
                    font-size: 8px;
                    line-height: 1.2;
                }
            }
            [x-cloak] {
                display: none !important;
            }
            
            .choices__input {
                /* Change the default style of tailwindcss to Choices.js in multiple select */
                --tw-ring-offset-color: #f9f9f9 !important;
                --tw-ring-color: #f9f9f9 !important;
                border-color: #f9f9f9 !important;
            }

            .checkbox-container {
                border: 2px solid #ccc; height: 200px; overflow-y: scroll;
            }

            /* Remove Arrow in number input */
            /* Chrome, Safari, Edge, Opera */
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            /* Firefox */
            input[type=number] {
                -moz-appearance: textfield;
            }

            .loader {
                border-top-color: #16a34a;
                -webkit-animation: spinner 1.5s linear infinite;
                animation: spinner 1.5s linear infinite;
            }
            
            @-webkit-keyframes spinner {
                0% {
                    -webkit-transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(360deg);
                }
            }
            
            @keyframes spinner {
                0% {
                    transform: rotate(0deg);
                }
                100% {
                    transform: rotate(360deg);
                }
            }
        </style>

        <!-- Alpine Plugins -->
        <script defer src="https://cdn.jsdelivr.net/npm/@alpinejs/collapse@3.x.x/dist/cdn.min.js"></script>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        @livewireStyles
        @stack('styles')
    </head>
    <body class="font-sans antialiased overscroll-none" style="background-color: #f3f3f4;">
        <div>
            <div x-data="sidebar()" @resize.window="handleResize()">
                <div class="xl:flex">
                    
                    @livewire('sidebar-menu')
    
                    <!-- Page Content -->
                    <main class="w-full overflow-x-hidden">
                        <!-- Loading -->
                        {{-- <x-loading></x-loading> --}}
    
                        {{-- Logout --}}
                        @livewire('navigation-dropdown')
                        
                        <!-- Page Heading -->
                        <header class="bg-white w-full">
                            <div class="mx-auto py-6 px-6">
                                {{ $header }}
                            </div>
                        </header>
    
                        {{-- Loader --}}
                        {{-- <div 
                            x-data="{ loading: false }" 
                            x-show="loading"
                            @loading.window="loading = $event.detail.loading" 
                        >
                            <div class="fixed top-0 left-0 right-0 bottom-0 w-full h-screen z-50 overflow-hidden bg-gray-700 opacity-75 flex flex-col items-center justify-center">
                                <div class="loader ease-linear rounded-full border-4 border-t-4 border-gray-200 h-12 w-12 mb-4"></div>
                                <h2 class="text-center text-white dark:text-fuchsia-600 text-xl font-semibold">Loading....</h2>
                            </div>
                        </div> --}}
    
                        {{ $slot }}
                    </main>
                </div>
            </div>
        </div>
        @livewireScripts
        <script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>
        <script src="{{ asset('js/qr-scanner.umd.min.js') }}"></script>
        @stack('scripts')
    </body>
</html>
