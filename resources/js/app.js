require("./bootstrap");

import Alpine from "alpinejs";

window.Alpine = Alpine;

Alpine.start();

import flatpckr from "flatpickr";
window.flatpckr = flatpckr;
