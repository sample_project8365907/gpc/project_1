<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddScrapBatchIdInVolunteerScrapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('volunteer_scraps', function (Blueprint $table) {
            $table->unsignedBigInteger('scrap_batch_id')->after('weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('volunteer_scraps', function (Blueprint $table) {
            $table->dropColumn('scrap_batch_id');
        });
    }
}
