<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ScrapCurrentBatch;

class ScrapCurrentBatchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ScrapCurrentBatch::truncate();

        ScrapCurrentBatch::create([
            'scrap_current_batch' => 1,
        ]);
    }
}
