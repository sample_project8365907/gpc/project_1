<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleAndPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        Role::truncate();

        Role::create([
            'name' => 'admin'
        ]);

        Role::create([
            'name' => 'accounting'
        ]);

        Role::create([
            'name' => 'baling'
        ]);

        Permission::truncate();

        Permission::create(['name' => 'view-dashboard']);

        Permission::create(['name' => 'view-users']);
        Permission::create(['name' => 'create-users']);
        Permission::create(['name' => 'edit-users']);
        Permission::create(['name' => 'edit-users-permissions']);
        Permission::create(['name' => 'edit-users-password']);
        Permission::create(['name' => 'delete-users']);

        Permission::create(['name' => 'view-locations']);
        Permission::create(['name' => 'create-locations']);
        Permission::create(['name' => 'edit-locations']);
        Permission::create(['name' => 'delete-locations']);

        Permission::create(['name' => 'view-material-types']);
        Permission::create(['name' => 'create-material-types']);
        Permission::create(['name' => 'edit-material-types']);
        Permission::create(['name' => 'delete-material-types']);

        Permission::create(['name' => 'view-volunteers']);
        Permission::create(['name' => 'create-volunteers']);
        Permission::create(['name' => 'edit-volunteers']);
        Permission::create(['name' => 'delete-volunteers']);

        Schema::enableForeignKeyConstraints();
    }
}
