<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Location;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        Location::truncate();

        Location::create([
            'name' => 'CCP-Plant',
            'code' => 'PCCP'
        ]);

        Location::create([
            'name' => 'SQ-Plant',
            'code' => 'PSQ'
        ]);

        Schema::enableForeignKeyConstraints();
    }
}
