<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use App\Models\User;
use App\Models\Location;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        User::truncate();

        $user = User::create([
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('greenkraft2022'),
            'status_id' => 1,
            'default_location' => 1,
        ]);

        // $role = Role::select('id')->where('name', 'admin')->first();
        // $user->roles()->attach($role);

        $location = Location::select('id')->where('name', 'CCP-Plant')->first();
        $user->locations()->attach($location);

        Schema::enableForeignKeyConstraints();
    }
}
