<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(RoleAndPermissionTableSeeder::class);
        $this->call(LocationTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(UserPermissionTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(MaterialTypeTableSeeder::class);
        $this->call(OtherStatusTableSeeder::class);
        $this->call(ScrapBatchTableSeeder::class);
        $this->call(ScrapCurrentBatchTableSeeder::class);
    }
}
