<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\MaterialType;

class MaterialTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        MaterialType::truncate();

        MaterialType::create([
            'name' => 'PET Bottle',
        ]);

        MaterialType::create([
            'name' => 'Soft Plastic',
        ]);

        Schema::enableForeignKeyConstraints();
    }
}
