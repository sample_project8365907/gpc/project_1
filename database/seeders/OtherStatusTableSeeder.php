<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OtherStatus;

class OtherStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OtherStatus::truncate();

        OtherStatus::create([
            'name' => 'Done',
        ]);

        OtherStatus::create([
            'name' => 'Cancelled',
        ]);

        OtherStatus::create([
            'name' => 'Submitted',
        ]);
    }
}
