<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ScrapBatch;

class ScrapBatchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ScrapBatch::truncate();

        ScrapBatch::create([
            'name' => 'Batch 1',
        ]);

        ScrapBatch::create([
            'name' => 'Batch 2',
        ]);

        ScrapBatch::create([
            'name' => 'Batch 3',
        ]);

        ScrapBatch::create([
            'name' => 'Batch 4',
        ]);

        ScrapBatch::create([
            'name' => 'Batch 5',
        ]);
    }
}
