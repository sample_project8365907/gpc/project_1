<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('id', 1)->first();

        $user->givePermissionTo('view-dashboard');

        $user->givePermissionTo('view-users');
        $user->givePermissionTo('create-users');
        $user->givePermissionTo('edit-users');
        $user->givePermissionTo('edit-users-permissions');
        $user->givePermissionTo('edit-users-password');
        $user->givePermissionTo('delete-users');

        $user->givePermissionTo('view-locations');
        $user->givePermissionTo('create-locations');
        $user->givePermissionTo('edit-locations');
        $user->givePermissionTo('delete-locations');

        $user->givePermissionTo('view-material-types');
        $user->givePermissionTo('create-material-types');
        $user->givePermissionTo('edit-material-types');
        $user->givePermissionTo('delete-material-types');

        $user->givePermissionTo('view-volunteers');
        $user->givePermissionTo('create-volunteers');
        $user->givePermissionTo('edit-volunteers');
        $user->givePermissionTo('delete-volunteers');
    }
}
