<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/locations', [LocationController::class, 'index'])->middleware('can:view-locations')->name('locations.index');

    Route::get('/material-types', [MaterialTypeController::class, 'index'])->middleware('can:view-material-types')->name('material-types.index');

    Route::get('/users', [UserController::class, 'index'])->middleware('can:view-users')->name('users.index');
    Route::get('/users/permissions/{id}', [UserController::class, 'permissions'])->middleware('can:view-users')->name('users.permissions');
    Route::get('/users/activity-logs', [UserController::class, 'activityLogs'])->middleware('can:view-users')->name('users.activity-logs');

    Route::get('/database', [DatabaseController::class, 'index'])->middleware('can:export-database')->name('database.index');
    Route::get('/database/export', [DatabaseController::class, 'export'])->middleware('can:export-database')->name('database.export');
    
    // For volunteers
    Route::get('/volunteers', [VolunteerController::class, 'index'])->middleware('can:view-volunteers')->name('volunteers.index');
    Route::get('/volunteers/edit/{id}', [VolunteerController::class, 'edit'])->middleware('can:view-volunteers')->name('volunteers.edit');
    Route::get('/volunteers/scan-qr', [VolunteerController::class, 'scanQr'])->middleware('can:view-volunteers')->name('volunteers.scan-qr');
    Route::get('/volunteers/download-coupons', [VolunteerController::class, 'downloadCoupon'])->middleware('can:view-volunteers')->name('volunteers.download-coupon');

    // For voluteer scraps
    Route::get('/volunteers/scraps/coupons', [VolunteerScrapController::class, 'printCoupon'])->middleware('can:view-volunteers')->name('volunteer-scraps.print-coupon');

    // For Admin only
    Route::get('phpmyinfo', function () {
        phpinfo();
    })->name('phpmyinfo');
});

require __DIR__ . '/auth.php';
